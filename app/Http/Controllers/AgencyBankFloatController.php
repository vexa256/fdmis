<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\AgencyBankFloat as floatData;
use App\banks as Banks;
use App\outlets;
use App\User;
use App\AgencyBankAccounts as BankAccounts;
use App\AgencyBankFloatLogs as BankLogs;
use App\bankfloats_logs as AgentBankLogs;
use App\Bankfloat as AgentfloatData;


class AgencyBankFloatController extends Controller
{



    public function AgencyBankFloatRequestForm()
			{
				$Banks = BankAccounts::where("AgencyID", Auth::user()->Agency_id)->get();

				$data = [

			    		'Title' => 'Agent Bank Float Request',
			    		'page' => 'Agency.AgencyBankFloatRequest',
			    		'Banks' => $Banks
			    		//'Notifications' => 'True',
			    		/**'message' => 'Hello',
			    		'not_type' => 'success'**/

    	               ];


    	                return view('accounts.admin', $data);

			}




	public function AgencyBankFloatRequest(request $request)

			{

		if (Auth::user()->Account_type == 'Agency') {
		


			$hasher    =  $request->input('Amount');
			$hasher2   =  Carbon::now();
			$hasher3   = $hasher2->toDateTimeString();
			$finalHash = $hasher3.'2333ddf22'.$hasher;



		 $request->validate(['Amount' => 'required|integer', 'BankID' => 'required', ]);

		 $BankID = $request->input('BankID');

		 $BankAccounts = BankAccounts::find($BankID);


		 if (floatData::where('BankAccount', $BankAccounts->BankAccount)->exists()) {

		$data = floatData::where('BankAccount', $BankAccounts->BankAccount)->first();
		 		
		    $data_id = $data->id;

		 	$CurrentRequestAmount = $data->request_amount;


		$MakeRequest = floatData::find($data_id);


				if ($MakeRequest->status == 'Approved') {
					
					$MakeRequest->request_amount = $request->input('Amount');


				} else {


					$MakeRequest->request_amount = $CurrentRequestAmount + $request->input('Amount');


				}



		
		$MakeRequest->status = "Pending Approval";

		$data_Unique = $MakeRequest->transactionID;

			if ($MakeRequest->save()) {



		$Float_log = new BankLogs;


			
		 $Float_log->AgencyID =  $BankAccounts->AgencyID;
		 $Float_log->AgencyName =  $BankAccounts->AgencyName;
		/// $Float_log->AgentName = Auth::user()->name;
		// $Float_log->OutletName =  $BankAccounts->OutletName;
		// $Float_log->OutletID =  $BankAccounts->OutletID;
		 $Float_log->BankName =  $BankAccounts->BankName;
		 $Float_log->BankID =  $BankAccounts->BankID;
		 $Float_log->AccountID =  $data_Unique;
		 $Float_log->BankAccount =  $BankAccounts->BankAccount;
		 $Float_log->status =  "Pending Approval";
		 $Float_log->request_amount =  $request->input('Amount');
		 $Float_log->transactionID = $data_Unique;



				


				 return redirect()->route('BankAgencyFloatRequestsHistory')->with('status', 'Float request to your HD Resources LTD made successfully and is pending approval');



			}/******inside if closure*************/

    
 } else {

 	$Float_log = new BankLogs;


      
		 $Float_log->AgencyID =  $BankAccounts->AgencyID;
		 $Float_log->AgencyName =  $BankAccounts->AgencyName;
		// $Float_log->AgentName = Auth::user()->name;
		 //$Float_log->OutletName =  $BankAccounts->OutletName;
		// $Float_log->OutletID =  $BankAccounts->OutletID;
		 $Float_log->BankName =  $BankAccounts->BankName;
		 $Float_log->BankID =  $BankAccounts->BankID;
		 $Float_log->AccountID =  $finalHash;
		 $Float_log->BankAccount =  $BankAccounts->BankAccount;
		 $Float_log->status =  "Pending Approval";
		 $Float_log->request_amount =  $request->input('Amount');
		 $Float_log->transactionID = $finalHash; 




 	$Float = new floatData;



      
		 $Float->AgencyID =  $BankAccounts->AgencyID;
		 $Float->AgencyName =  $BankAccounts->AgencyName;
		// $Float->AgentName = Auth::user()->name;
		/// $Float->OutletName =  $BankAccounts->OutletName;
		/// $Float->OutletID =  $BankAccounts->OutletID;
		 $Float->BankName =  $BankAccounts->BankName;
		 $Float->BankID =  $BankAccounts->BankID;
		 $Float->AccountID =  $finalHash;
		 $Float->BankAccount =  $BankAccounts->BankAccount;
		 $Float->status =  "Pending Approval";
		 $Float->request_amount =  $request->input('Amount');
		  $Float->transactionID = $finalHash; 
	

		 if ($Float->save() &&  $Float_log->save()) {
		 	

		 		 return redirect()->route('BankAgencyFloatRequestsHistory')->with('status', 'Float request to your HD Resources LTD made successfully and is pending approval');


		 }
		


 }/**********Else closure*****************/



}
		 
		
	}/***********function closure***************/





public function BankAgencyFloatRequestsHistory()
	{
	

		$Float = floatData::where("AgencyID", Auth::user()->Agency_id)->get();
		$Float_log = BankLogs::where("AgencyID", Auth::user()->Agency_id)->get();
		//$OutletName = $Float->OutletName;

       	$data = [

	       'Title' => 'Agent Bank Float Accounts',
	       'page' => 'Agency.ManageBankFloatRequests',
	       'Floats' => $Float,
	       'Float_logs' => $Float_log,
	      // 'OutletName' =>Auth::user()->Outlet_Name,
	       
    	  ];


	

  			return view('accounts.admin', $data);





	}




	public function Approve_Bank_Float($id)
	{



			$floatData = floatData::find($id);
			$data_Unique = $floatData->transactionID;
			$floatData->status =  "Approved";

			$oldFloat = $floatData->request_amount;
			$newFloat = $floatData->amount;
			$floatData->amount = $oldFloat + $newFloat;

			$BankLogs = BankLogs::where("transactionID", "=", $data_Unique)->first();
			$id = $BankLogs->id;

			$BankLogsSubmit = BankLogs::find($id);

			$BankLogsSubmit->status =  "Approved";

			if ($floatData->save() && $BankLogsSubmit->save()) {
				

				 return redirect()->route('BankAgencyFloatRequestsHistory')->with('status', 'Float request to from child Agent-User approved successfully');


			}
		


	}







	public function AgencyDeleteBankFloatRequestHistory($id)
	{
		
		$floatData = floatData::find($id);

				 if ($floatData->delete()) {


	 						 return redirect()->route('BankAgencyFloatRequestsHistory')->with('status', 'Float request deleted successfully');


						 }



	}


/*********************************************************************************/



public function AgencyApproveAgentFloatRequest()
  {
    $Float = AgentfloatData::where("AgencyID", Auth::user()->Agency_id)->get();
    $Float_log = AgentBankLogs::where("AgencyID", Auth::user()->Agency_id)->get();
    $floatData = floatData::where("AgencyID", Auth::user()->Agency_id)->get();
    //$OutletName = $Float->OutletName;

        $data = [

         'Title' => 'Float Requests',
         'page' => 'Agency.ApproveBankFloatRequests',
         'Floats' => $Float,
         'Banks' => $floatData,
         'Float_logs' => $Float_log,
         //'OutletName' =>Auth::user()->Outlet_Name,

        ];

        return view('accounts.admin', $data);
  }






     public function Agency_Approve_Bank_Float_request(request $request)
	{		
			$AgentFloatRequestID =  $request->input('AgentFloatRequestID'); /***ID OF AgentfloatData::find(id) */
			$AgencyFloatAccID =  $request->input('AgencyFloatAccID'); /***ID OF floatData::find(id) *****/

			$u =  AgentfloatData::find($AgentFloatRequestID);
			$b =  floatData::find($AgencyFloatAccID);

			define('AgentAmountRequested', $u->request_amount);
			define('AgencyFloatAvailable', $b->amount);
			


			define('AgencyBankID', $b->BankID);
			define('AgentBankID', $u->BankID);
			

			if (AgencyFloatAvailable < AgentAmountRequested) {


				return redirect()->route('Agency_Approve_Bank_Float')->with('status', 'Sorry you do not have enough float on this Bank account to complete transaction, Please make a float request');
				
			}elseif (AgencyBankID != AgentBankID) {
				
				return redirect()->route('Agency_Approve_Bank_Float')->with('status', 'Transaction failed, you are attempting to do an inter-Bank float transfer. This action is not supported. Please try again'.' '.AgencyBankID);


			}else{

			/*****issue with transactionID on BOTH agentfloatcontrollers*****/



			$floatData = AgentfloatData::find($AgentFloatRequestID);
			$data_Unique = $floatData->transactionID;
			$floatData->status =  "Approved";

			$oldFloat = $floatData->request_amount;
			$newFloat = $floatData->amount;
			$floatData->amount = $oldFloat + $newFloat;

			$BankLogs = AgentBankLogs::where("transactionID", "=", $data_Unique)->first();
			$id = $BankLogs->id;

			$BankLogsSubmit = AgentBankLogs::find($id);

			$BankLogsSubmit->status =  "Approved";

				/****deduct agency float******/
				$b->amount = AgencyFloatAvailable - AgentAmountRequested;
				$b->save();

				/****deduct agency float******/

			if ($floatData->save() && $BankLogsSubmit->save()) {
				

				 return redirect()->route('Agency_Approve_Bank_Float')->with('status', 'Float request  from child Agent-User approved successfully, Check Float Request Logs for more details');


			}
		


	}
	}































































}/************classss***********************************************/
