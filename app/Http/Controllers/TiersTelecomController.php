<?php
namespace App\Http\Controllers;

use App\telcom as Telecoms;
use App\TelecomTiers as Tiers;
use Illuminate\Http\Request;

class TiersTelecomController extends Controller
{
  /**
   * @param $id
   */
  public function ManageTelecomTiers($id)
  {

    $Telecoms = Telecoms::find($id);
    $TelecomID = $Telecoms->Telecom_ID;
    $Data = Tiers::where('TelecomID', '=', $TelecomID)->get();

    $data = [

      'Title'    => 'Manage Wallet Tiers',
      'page'     => 'tiers.ManageTelecomTiers',
      'TierData' => $Telecoms,
      'Tiers'    => $Data,

//'Notifications' => 'True',
      //'message' => 'Bank successfully created',
      'not_type' => 'success'

    ];

    return view('accounts.admin', $data);
  }

  /**
   * @param request $request
   */
  public function SubmitTelecomTiers(request $request)
  {

    $request->validate([
      'range_from'         => 'required',
      'range_to'           => 'required',
      'TelecomName'        => 'required',
      'TelecomID'          => 'required',
      'DepositCharge'      => 'required|integer',
      'WithdrawCharge'     => 'required|integer',
      'WithdrawCommission' => 'required|integer',
      'DepositCommission'  => 'required|integer'

    ]);

    $Tiers = new Tiers();
    $Tiers->range_from = $request->input('range_from');
    $Tiers->range_to = $request->input('range_to');
    $Tiers->TelecomName = $request->input('TelecomName');
    $Tiers->TelecomID = $request->input('TelecomID');
    $Tiers->DepositCharge = $request->input('DepositCharge');
    $Tiers->WithdrawCharge = $request->input('WithdrawCharge');
    $Tiers->WithdrawCommission = $request->input('WithdrawCommission');
    $Tiers->DepositCommission = $request->input('DepositCommission');

    if ($Tiers->save())
    {
      $data = $request->input('TelecomName');

      return redirect()->back()->with('status', 'Tier for telecom'.' '.$data.' '.'successfully created');
    }
  }

  /**
   * @param $id
   */
  public function DeleteTelecomTier($id)
  {

    $Tiers = Tiers::find($id);

    if ($Tiers->delete())
    {
      return redirect()->back()->with('status', 'Tier deleted successfully');
    }
  }
}
