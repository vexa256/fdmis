<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\outlets;
use App\telcom as Telecoms;
use App\Telecomfloat as floatData;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use App\TelecomTiers as Tiers;
use App\AgentPhoneNumbers as PhoneNumbers;
use App\telecomfloats_logs as TelecomLogs;
//use App\TelecomTiers as Tiers;


class TelecomDepositsController extends Controller
{

 /**Telecom Deposit Logic**/



		public function TelecomDepositLogic(request $request)
	     
	      {

	      	define("AuthorizedUserName", Auth::user()->name);
	      	define("AuthorizedAgencyName", Auth::user()->Agency_name);
	      	define("AuthorizedAgencyID", Auth::user()->Agency_id);
	      	define("AuthorizedAgentID", Auth::user()->Agent_id);
	      	define("AuthorizedAgentName", Auth::user()->name);
	      	define("AuthorizedOutletID", Auth::user()->Outlet_ID);
	      	define("AuthorizedOutletName", Auth::user()->Outlet_Name);
	      	


	      	$request->validate([
					        'TotalDeposit' => 'required|integer',
					        'RecipientNumber' => 'required|integer',
					        'SenderNumber' => 'required',
					        'TelecomCarrier' => 'required',
					      
					    ]);

	      	function generatePin( $number ) {
						    // Generate set of alpha characters
						    $alpha = array();
						    for ($u = 65; $u <= 90; $u++) {
						        // Uppercase Char
						        array_push($alpha, chr($u));
						    }
						    /***Timothy disable this for now***/


						    /**This whole system is dedicated to jesus christ my lord and is for the purpose of glorigying him and his church on earth. it will stay active and healthy and relevant until christ comes back****/

						    // Just in case you need lower case
						    // for ($l = 97; $l <= 122; $l++) {
						    //    // Lowercase Char
						    //    array_push($alpha, chr($l));
						    // }

						    // Get random alpha character
						    $rand_alpha_key = array_rand($alpha);
						    $rand_alpha = $alpha[$rand_alpha_key];

						    // Add the other missing integers
						    $rand = array($rand_alpha);
						    for ($c = 0; $c < $number - 1; $c++) {
						        array_push($rand, mt_rand(0, 9));
						        shuffle($rand);

						    }

						    return implode('', $rand);
						}


			  

			   define("TransactionID", generatePin(8));



	      	$TotalDeposit = $request->input("TotalDeposit");
	      	$RecipientNumber = $request->input('RecipientNumber');
	      	$SenderNumber = $request->input('SenderNumber');
	      	$TelecomID = $request->input('TelecomCarrier');

	      	define('TotalDepositUser', $request->input("TotalDeposit"));
	      	

	      	
	      
	      	$Float = 0;
	      	$Cashathand = $TotalDeposit;
	      	$DepositCharge = 0;
	      	$WithdrawCharge = 0;
	      	$WithdrawCommission = 0;
	      	//DepositCommission = 0;

	      	


	      	$PhoneNumberAccount = PhoneNumbers::find($TelecomID);

	      	$FloatAccountID = $PhoneNumberAccount->PhoneNumber;

	      	$FloatAccount =  floatData::where("PhoneNumber", "=", $FloatAccountID)->first();

	      		if (is_null($FloatAccount)) {
	      			

	      			 return redirect()->route('DepositsTelecom')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');
	      		}else {


	        	$FinalFloatAccount = floatData::find($FloatAccount->id);

	        }


	      	/****isolate Telecom ID*********/

	      	$id_z = $request->input('TelecomCarrier');

	      	$data_z = PhoneNumbers::find($id_z);

	      	$TelLID = $data_z->TelecomID;

	      	$Approved_telecom = $TelLID;




	      	/****isolate Telecom ID*********/




	      	define("SelectedTelecomID", $Approved_telecom);

			
			$Tiers = Tiers::where("TelecomID", "=", SelectedTelecomID);
	      	
			$Checker_init_func = floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->first();



	if (!floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->exists()) {



    return redirect()->route('DepositsTelecom')->with('status', ' Sorry, You are not authorized to execute this operation, Contact your system admin. Request for float to correct this issue');



		} elseif($Checker_init_func->amount < $TotalDeposit) {
			

			 return redirect()->route('DepositsTelecom')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');


		} else {



			$Tiers_Telecom = Tiers::where("TelecomID", "=", SelectedTelecomID)

			->where("range_from", "<=", TotalDepositUser)
			->where("range_to", ">=", TotalDepositUser)->first();

		
				if (!is_null($Tiers_Telecom)) {


					$DPC =  $Tiers_Telecom->DepositCommission;

			define('DepositCommission',$DPC);
			


				} elseif (is_null($Tiers_Telecom)) {
			
				return redirect()->route('DepositsTelecom')->with('status', ' Sorry, No Tiers assigned.');

			}

			





	  		 /**Determin agent float account by phone number***/

	  		// $Float = $FinalFloatAccount;

	  		 // $Float->


	  		//Send deposit Commission



	  		 	if (Commission::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)

	  		 		->where('OutletID', AuthorizedOutletID)

	  		 		->exists()) {

	     	       $Checker_for_DepositCommission = Commission::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)

	     	   	  ->where('OutletID', AuthorizedOutletID)

	     	   	  ->first();
	  		 		

	  		 		$CurrentDepositcommission = $Checker_for_DepositCommission->DepositCommission;


	  		 		$FinalDepositCommission_commit = $CurrentDepositcommission + DepositCommission;


	  		 		/**Trigger Deposit Commision Update only ***/



	  		 		$UniqueCommissionID = $Checker_for_DepositCommission->id;

	  		 		$UpdateDb = Commission::find($UniqueCommissionID);

	  		 		$UpdateDb->DepositCommission = $FinalDepositCommission_commit;

	  		 		$UpdateDb->save();




	  		 		/**Trigger Deposit Commision Update only ***/


	  		 	} else {


	  		 		/**new Deposit  commission entry***/

	  		 $DeositCommission = new Commission;

	  		 $DeositCommission->TelecomID = SelectedTelecomID;
	  		 $DeositCommission->TelecomName = $PhoneNumberAccount->TelecomName;
	  		 $DeositCommission->DepositCommission = DepositCommission;
	  		 $DeositCommission->AgencyID = $PhoneNumberAccount->AgencyID;
	  		 $DeositCommission->AgencyName = $PhoneNumberAccount->AgencyName;
	  		 $DeositCommission->AgentID	 =  AuthorizedAgentID;
	  		 $DeositCommission->AgentName = AuthorizedUserName;
	  		 $DeositCommission->OutletName = $PhoneNumberAccount->OutletName;
	  		 $DeositCommission->TransactionType = "Telecom Deposit";
	  		 $DeositCommission->OutletID = $PhoneNumberAccount->OutletID;
	  		 $DeositCommission->AccounID = $PhoneNumberAccount->OutletID;
	  		 $DeositCommission->PhoneNumber = $PhoneNumberAccount->PhoneNumber ;
	  		 $DeositCommission->save();
	  		


}/****deposit commission closure ***/


/***Cash at hand******/


/****ask about deducting charges from cash at hand***/


	  		 	if (Cashathand::where('TelecomPhoneNumber', $PhoneNumberAccount->PhoneNumber)

	  		 		->where('OutletID', AuthorizedOutletID)->exists()) {


	  		 	$IsolateCashAccount = 	Cashathand::where('TelecomPhoneNumber', $PhoneNumberAccount->PhoneNumber)

	  		 		->where('OutletID', AuthorizedOutletID)

	  		 		->first();


	  		 		$Cash_uiniqueID = $IsolateCashAccount->id;

	  		 		$CashAccountFinal = Cashathand::find($Cash_uiniqueID);
	  		 		$CurrentCashathand = $CashAccountFinal->amount;
	  		 		$CashAccountFinal->amount = $CurrentCashathand + $Cashathand;
	  		 		$CashAccountFinal->save();


	  		 	} else {

	  		 		$NewcashCashathand = new Cashathand;

	  		 		$NewcashCashathand->TelecomID = SelectedTelecomID;
	  		 		$NewcashCashathand->TelecomName = $PhoneNumberAccount->TelecomName;
	  		 		$NewcashCashathand->TelecomPhoneNumber = $PhoneNumberAccount->PhoneNumber ;
	  		 		$NewcashCashathand->TransactionPerson = $PhoneNumberAccount->AuthorizedUserName ;
	  		 		$NewcashCashathand->AgencyID = $PhoneNumberAccount->AgencyID ;
	  		 		$NewcashCashathand->AgencyName = $PhoneNumberAccount->AgencyName;
	  		 		$NewcashCashathand->AgentID = $PhoneNumberAccount->Agent_id ;
	  		 		$NewcashCashathand->AgentName = $PhoneNumberAccount->AuthorizedUserName;
	  		 		$NewcashCashathand->OutletName = $PhoneNumberAccount->OutletName ;
	  		 		$NewcashCashathand->OutletID = $PhoneNumberAccount->OutletID ;
	  		 		$NewcashCashathand->amount = $PhoneNumberAccount->Cashathand ;
	  		 		$NewcashCashathand->AccounID = $PhoneNumberAccount->OutletID ;
	  		 		$NewcashCashathand->save();



	  		 	}





/***Float Account***/ 
/****ask about deducting charges from float account***/



if (floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->exists()) {

	     	       $IsolateFloatAccount = floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)

	     	   	  ->where('OutletID', AuthorizedOutletID)

	     	   	  ->first();
	     	  

	     	       $FloatAccount_isolated = floatData::find($IsolateFloatAccount->id);

	     	       $CurrentFloatAmount =  $FloatAccount_isolated->amount;

	     	       $FloatAccount_isolated->amount = $CurrentFloatAmount - $TotalDeposit;

	     	       $FloatAccount_isolated->save();

	     	       $Float =   $FloatAccount_isolated->amount;


	  		 		}




	  			/**Create a transaction ***********/

						


	  			$TelecomTransactions = new TelecomTransactions;
	  			$TelecomTransactions->TransID =  TransactionID ;
	  			$TelecomTransactions->Customer_Name = '' ;

	  			if (is_null($RecipientNumber) || is_null($SenderNumber) ) {

	  				$TelecomTransactions->Transaction_Type = "Mono Deposit" ;

	  				
	  			}else {

	  					$TelecomTransactions->Transaction_Type = "Deposit" ;
	  			}
	  			
	  			$TelecomTransactions->Running_Balance = $Float;

	  			$TelecomTransactions->Deposit_Commission = DepositCommission;


	  			$TelecomTransactions->Customer_Name = $SenderNumber  ;
	  			$TelecomTransactions->TelecomName = $PhoneNumberAccount->TelecomName;  ;
	  			$TelecomTransactions->TelecomID = SelectedTelecomID;  
	  			$TelecomTransactions->AgentPhoneNumber = $PhoneNumberAccount->PhoneNumber;  
	  			$TelecomTransactions->AgencyID =  AuthorizedAgencyID;
	  			$TelecomTransactions->Reciever_Phone = $RecipientNumber;
	  			$TelecomTransactions->AgencyName = AuthorizedAgencyName ;
	  			$TelecomTransactions->AgentID = AuthorizedAgentID ;
	  			$TelecomTransactions->AgentName = AuthorizedUserName;
	  			$TelecomTransactions->OutletName = AuthorizedOutletName ;
	  			$TelecomTransactions->OutletID = AuthorizedOutletID  ;
	  			$TelecomTransactions->status ="Successfull";
	  			$TelecomTransactions->amount = $TotalDeposit;
	  			$TelecomTransactions->save();


	  			/**Create a transaction ***********/





             /*****log transaction***************/


$TelecomTransactionsLogs = new TelecomTransactionsLogs;
	  			$TelecomTransactionsLogs->TransID =  TransactionID ;
	  			$TelecomTransactionsLogs->Customer_Name = '' ;

	  			if (is_null($RecipientNumber) || is_null($SenderNumber) ) {

	  				$TelecomTransactionsLogs->Transaction_Type = "Mono Deposit" ;

	  				
	  			}else {

	  					$TelecomTransactionsLogs->Transaction_Type = "Deposit" ;
	  			}
	  			
	  			$TelecomTransactionsLogs->Running_Balance = $Float;

	  			$TelecomTransactionsLogs->Deposit_Commission = DepositCommission;

	  			$TelecomTransactionsLogs->TelecomName = $PhoneNumberAccount->TelecomName;  ;
	  			$TelecomTransactionsLogs->TelecomID = SelectedTelecomID;  
	  			$TelecomTransactionsLogs->AgentPhoneNumber = $PhoneNumberAccount->PhoneNumber;  
	  			$TelecomTransactionsLogs->Customer_Name = $SenderNumber  ;
	  			$TelecomTransactionsLogs->AgencyID =  AuthorizedAgencyID;
	  			$TelecomTransactionsLogs->Reciever_Phone = $RecipientNumber;
	  			$TelecomTransactionsLogs->AgencyName = AuthorizedAgencyName ;
	  			$TelecomTransactionsLogs->AgentID = AuthorizedAgentID ;
	  			$TelecomTransactionsLogs->AgentName =AuthorizedUserName;
	  			$TelecomTransactionsLogs->OutletName = AuthorizedOutletName ;
	  			$TelecomTransactionsLogs->OutletID = AuthorizedOutletID  ;
	  			$TelecomTransactionsLogs->status ="Successfull";
	  			$TelecomTransactionsLogs->amount = $TotalDeposit;
	  			$TelecomTransactionsLogs->save();




	  			  return redirect()->route('home')->with('status', 'Hello , Deposit Made successfully');




		  }/***Mother function Closure****/

















}/******check float account if exists and float amount****/


	









}/***class closure****/
