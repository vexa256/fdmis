<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat as BankData;
use App\BankCommission as Commission;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\BankCashAtHand as Cashathand;
use App\outlets;
//use App\banks as Banks;
use App\AgentBankAccounts as BankAccounts;
use App\User;
use App\BankTiers as Tiers;
use App\bankfloats_logs as BankLogs;

class BankDepositController extends Controller
{
   




		public function BankDepositLogic(request $request)
	     
	      {

	      	define("AuthorizedUserName", Auth::user()->name);
	      	define("AuthorizedAgencyName", Auth::user()->Agency_name);
	      	define("AuthorizedAgencyID", Auth::user()->Agency_id);
	      	define("AuthorizedAgentID", Auth::user()->Agent_id);
	      	define("AuthorizedAgentName", Auth::user()->name);
	      	define("AuthorizedOutletID", Auth::user()->Outlet_ID);
	      	define("AuthorizedOutletName", Auth::user()->Outlet_Name);
	      	


	      	$request->validate([
					        'TotalDeposit' => 'required|integer',
					        'RecipientAccount' => 'required',
					        'RecipientName' => 'required',
					        'SenderName' => 'required',
					        'BankCarrier' => 'required',
					      
					    ]);

	      	function generatePin( $number ) {
						    // Generate set of alpha characters
						    $alpha = array();
						    for ($u = 65; $u <= 90; $u++) {
						        // Uppercase Char
						        array_push($alpha, chr($u));
						    }
						    /***Timothy disable this for now***/


						    /**This whole system is dedicated to jesus christ my lord and is for the purpose of glorigying him and his church on earth. it will stay active and healthy and relevant until christ comes back****/

						    // Just in case you need lower case
						    // for ($l = 97; $l <= 122; $l++) {
						    //    // Lowercase Char
						    //    array_push($alpha, chr($l));
						    // }

						    // Get random alpha character
						    $rand_alpha_key = array_rand($alpha);
						    $rand_alpha = $alpha[$rand_alpha_key];

						    // Add the other missing integers
						    $rand = array($rand_alpha);
						    for ($c = 0; $c < $number - 1; $c++) {
						        array_push($rand, mt_rand(0, 9));
						        shuffle($rand);

						    }

						    return implode('', $rand);
						}


			  

			   define("TransactionID", generatePin(8));
			  


	      	$TotalDeposit = $request->input("TotalDeposit");
	      	$RecipientAccount = $request->input('RecipientAccount');
	      	$RecipientName = $request->input('RecipientName');
	      	$SenderName = $request->input('SenderName');
	      	$BankID = $request->input('BankCarrier');

	      	define('TotalDepositUser', $request->input("TotalDeposit"));
	      	

	      	
	      
	      	$Float = 0;
	      	$Cashathand = $TotalDeposit;
	      	$DepositCharge = 0;
	      	$WithdrawCharge = 0;
	      	$WithdrawCommission = 0;
	      	$SenderAccount = "";
	      	//DepositCommission = 0;

	      	


	      	$BankAccount = BankAccounts::find($BankID);

	      	$FloatAccountID = $BankAccount->BankAccount;

	      	$FloatAccount =  BankData::where("BankAccount", "=", $FloatAccountID)->first();

	      		if (is_null($FloatAccount)) {
	      			

	      			 return redirect()->route('DepositsBank')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');
	      		}else {


	        	$FinalFloatAccount = BankData::find($FloatAccount->id);

	        }


	      	/****isolate Bank ID*********/

	      	$id_z = $request->input('BankCarrier');

	      	$data_z = BankAccounts::find($id_z);

	      	$TelLID = $data_z->BankID;

	      	$Approved_Bank = $TelLID;




	      	/****isolate Bank ID*********/




	      	define("SelectedBankID", $Approved_Bank);
	      	define("SelectedBankName", $data_z->BankName);

			
			$Tiers = Tiers::where("BankID", "=", SelectedBankID);
	      	
			$Checker_init_func = BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->first();



	if (!BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->exists()) {



    return redirect()->route('DepositsBank')->with('status', ' Sorry, You are not authorized to execute this operation, Contact your system admin.');



		} elseif($Checker_init_func->amount < $TotalDeposit) {
			

			 return redirect()->route('DepositsBank')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');


		} else {



			$Tiers_Bank = Tiers::where("BankID", "=", SelectedBankID)

			->where("range_from", "<=", TotalDepositUser)
			->where("range_to", ">=", TotalDepositUser)->first();

		
				if (!is_null($Tiers_Bank)) {


					$DPC =  $Tiers_Bank->DepositCommission;

			        define('DepositCommission', $DPC);
			


				} elseif (is_null($Tiers_Bank)) {
			
				return redirect()->route('DepositsBank')->with('status', ' Sorry, No Tiers assigned.');

			}

			





	  		 /**Determin agent float account by phone number***/

	  		// $Float = $FinalFloatAccount;

	  		 // $Float->


	  		//Send deposit Commission



	  		 	if (Commission::where('BankAccount', $BankAccount->BankAccount)

	  		 		->where('OutletID', AuthorizedOutletID)

	  		 		->exists()) {

	     	       $Checker_for_DepositCommission = Commission::where('BankAccount', $BankAccount->BankAccount)

	     	   	  ->where('OutletID', AuthorizedOutletID)

	     	   	  ->first();
	  		 		

	  		 		$CurrentDepositcommission = $Checker_for_DepositCommission->DepositCommission;


	  		 		$FinalDepositCommission_commit = $CurrentDepositcommission + DepositCommission;


	  		 		/**Trigger Deposit Commision Update only ***/



	  		 		$UniqueCommissionID = $Checker_for_DepositCommission->id;

	  		 		$UpdateDb = Commission::find($UniqueCommissionID);

	  		 		$UpdateDb->DepositCommission = $FinalDepositCommission_commit;

	  		 		$UpdateDb->save();




	  		 		/**Trigger Deposit Commision Update only ***/


	  		 	} else {


	  		 		/**new Deposit  commission entry***/

	  		 $DeositCommission = new Commission;

	  		 $DeositCommission->BankID = SelectedBankID;
	  		 $DeositCommission->BankName = SelectedBankName;
	  		 $DeositCommission->DepositCommission = DepositCommission;
	  		 $DeositCommission->AgencyID = $BankAccount->AgencyID;
	  		 $DeositCommission->AgencyName = $BankAccount->AgencyName;
	  		 $DeositCommission->AgentID	 =  AuthorizedAgentID;
	  		 $DeositCommission->AgentName = AuthorizedUserName;
	  		 $DeositCommission->OutletName = $BankAccount->OutletName;
	  		 $DeositCommission->TransactionType = "Bank Deposit";
	  		 $DeositCommission->OutletID = $BankAccount->OutletID;
	  		 $DeositCommission->AccounID = $BankAccount->OutletID;
	  		 $DeositCommission->BankAccount = $BankAccount->BankAccount ;
	  		 $DeositCommission->save();
	  		


}/****deposit commission closure ***/


/***Cash at hand******/


/****ask about deducting charges from cash at hand***/


	  		 	if (Cashathand::where('BankAccountNumber', $BankAccount->BankAccount)

	  		 		->where('OutletID', AuthorizedOutletID)->exists()) {


	  		 	$IsolateCashAccount = 	Cashathand::where('BankAccountNumber', $BankAccount->BankAccount)

	  		 		->where('OutletID', AuthorizedOutletID)

	  		 		->first();


	  		 		$Cash_uiniqueID = $IsolateCashAccount->id;

	  		 		$CashAccountFinal = Cashathand::find($Cash_uiniqueID);
	  		 		$CurrentCashathand = $CashAccountFinal->amount;
	  		 		$CashAccountFinal->amount = $CurrentCashathand + $Cashathand;
	  		 		$CashAccountFinal->save();


	  		 	} else {

	  		 		$NewcashCashathand = new Cashathand;

	  		 		$NewcashCashathand->BankID = SelectedBankID;
	  		 		$NewcashCashathand->BankName = $BankAccount->BankName;
	  		 		$NewcashCashathand->BankAccountNumber = $BankAccount->BankAccount ;
	  		 		$NewcashCashathand->TransactionPerson = $BankAccount->AuthorizedUserName ;
	  		 		$NewcashCashathand->AgencyID = $BankAccount->AgencyID ;
	  		 		$NewcashCashathand->AgencyName = $BankAccount->AgencyName;
	  		 		$NewcashCashathand->AgentID = $BankAccount->Agent_id ;
	  		 		$NewcashCashathand->AgentName = $BankAccount->AuthorizedUserName;
	  		 		$NewcashCashathand->OutletName = $BankAccount->OutletName ;
	  		 		$NewcashCashathand->OutletID = $BankAccount->OutletID ;
	  		 		$NewcashCashathand->amount = $BankAccount->Cashathand ;
	  		 		$NewcashCashathand->AccounID = $BankAccount->OutletID ;
	  		 		$NewcashCashathand->save();



	  		 	}





/***Float Account***/ 
/****ask about deducting charges from float account***/



if (BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->exists()) {

	     	       $IsolateFloatAccount = BankData::where('BankAccount', $BankAccount->BankAccount)

	     	   	  ->where('OutletID', AuthorizedOutletID)

	     	   	  ->first();
	     	  

	     	       $FloatAccount_isolated = BankData::find($IsolateFloatAccount->id);

	     	       $CurrentFloatAmount =  $FloatAccount_isolated->amount;

	     	       $FloatAccount_isolated->amount = $CurrentFloatAmount - $TotalDeposit;

	     	       $FloatAccount_isolated->save();

	     	       $Float =   $FloatAccount_isolated->amount;


	  		 		}




	  			/**Create a transaction ***********/

						


	  			$BankTransactions = new BankTransactions;
	  			$BankTransactions->TransID =  TransactionID ;
	  			$BankTransactions->Customer_Name = '' ;

	  			if (is_null($RecipientAccount) || is_null($SenderAccount) ) {

	  				$BankTransactions->Transaction_Type = "Mono Deposit" ;

	  				
	  			}else {

	  					$BankTransactions->Transaction_Type = "Deposit" ;
	  			}
	  			
	  			$BankTransactions->Running_Balance = $Float;

	  			$BankTransactions->Deposit_Commission = DepositCommission;


	  			//$BankTransactions->Client_Phone = $SenderAccount  ;
	  			$BankTransactions->AgencyID =  AuthorizedAgencyID;
	  			$BankTransactions->Reciever_Acc = $RecipientAccount;
	  			$BankTransactions->Reciever_Name = $RecipientName;
	  			$BankTransactions->Sender_Name = $SenderName;
	  			$BankTransactions->AgencyName = AuthorizedAgencyName ;
	  			$BankTransactions->AgentID = AuthorizedAgentID ;
	  			$BankTransactions->AgentName = AuthorizedUserName;
	  			$BankTransactions->OutletName = AuthorizedOutletName ;
	  			$BankTransactions->OutletID = AuthorizedOutletID  ;
	  			$BankTransactions->status ="Successfull";
	  			$BankTransactions->amount = $TotalDeposit;
	  			$BankTransactions->BankID = $BankAccount->BankID;
	  			$BankTransactions->BankName = $BankAccount->BankName;
	  			$BankTransactions->AgentBankAcc = $BankAccount->BankAccount;
	  			$BankTransactions->save();


	  			/**Create a transaction ***********/





             /*****log transaction***************/


$BankTransactionsLogs = new BankTransactionsLogs;
	  			$BankTransactionsLogs->TransID =  TransactionID ;
	  			$BankTransactionsLogs->Customer_Name = '' ;

	  			if (is_null($RecipientAccount) || is_null($SenderAccount) ) {

	  				$BankTransactionsLogs->Transaction_Type = "Mono Deposit" ;

	  				
	  			}else {

	  					$BankTransactionsLogs->Transaction_Type = "Deposit" ;
	  			}
	  			
	  			$BankTransactionsLogs->Running_Balance = $Float;

	  			$BankTransactionsLogs->Deposit_Commission = DepositCommission;


	  			//$BankTransactionsLogs->Client_Phone = $SenderAccount  ;
	  			$BankTransactionsLogs->AgencyID =  AuthorizedAgencyID;
	  			//$BankTransactionsLogs->Reciever_Phone = $RecipientAccount;
	  			$BankTransactionsLogs->Reciever_Acc = $RecipientAccount;
	  			$BankTransactionsLogs->BankID = $BankAccount->BankID;
	  			$BankTransactionsLogs->BankName = $BankAccount->BankName;
	  			$BankTransactionsLogs->AgentBankAcc = $BankAccount->BankAccount;
	  			$BankTransactionsLogs->Reciever_Name = $RecipientName;
	  			$BankTransactionsLogs->Sender_Name = $SenderName;
	  			$BankTransactionsLogs->AgencyName = AuthorizedAgencyName ;
	  			$BankTransactionsLogs->AgentID = AuthorizedAgentID ;
	  			$BankTransactionsLogs->AgentName =AuthorizedUserName;
	  			$BankTransactionsLogs->OutletName = AuthorizedOutletName ;
	  			$BankTransactionsLogs->OutletID = AuthorizedOutletID  ;
	  			$BankTransactionsLogs->status ="Successfull";
	  			$BankTransactionsLogs->amount = $TotalDeposit;
	  			$BankTransactionsLogs->save();




	  			  return redirect()->route('home')->with('statuss', 'Hello , Deposit Made successfully');




		  }/***Mother function Closure****/

















}/******check float account if exists and float amount****/


	


















































   public function DepositsBank()
		{
			$Banks = BankAccounts::where("OutletID", "=", Auth::user()->Outlet_ID)->get();



			$data = [

			    		'Title' => 'Bank Deposit',
			    		'page' => 'banks.BankDeposit',
			    		'Banks' => $Banks,
			    		//'Notifications' => 'True',
			    		//'message' => 'Bank successfully created',
			    		'not_type' => 'success'

    	               ];


    	                return view('accounts.admin', $data);


		}


}/*************Final clousre*****************/
