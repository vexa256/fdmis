<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\outlets;
use App\telcom as Telecoms;
use App\Telecomfloat as floatData;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use App\TelecomTiers as Tiers;
use App\AgentPhoneNumbers as PhoneNumbers;
use App\telecomfloats_logs as TelecomLogs;

class AgencyTelecomCommissionController extends Controller
{

  						public function AgencyTelecomCommission()
  						{
  							$Commission = Commission::where("AgencyID", "=", Auth::user()->Agency_id)
  							->get();

  							$SumDeposit = Commission::where("AgencyID", "=", Auth::user()->Agency_id)->get()->sum('DepositCommission');

  							$SumWithdraw = Commission::where("AgencyID", "=", Auth::user()->Agency_id)->get()->sum('WithdrawCommission');


  							$Sum = $SumDeposit + $SumWithdraw;


  								$data = [

  					    		'Title' => 'Telecom Cash at Hand Accounts',
  					    		'page' => 'AgencyReports.TelecomCommissionReports',
  					    		'Telecoms' => $Commission,
  					    		'Total' => number_format($Sum) ,
  					    		//'Notifications' => 'True',
  					    		//'message' => 'Telecom successfully created',
  					    		'not_type' => 'success'

  		    	               ];


  		    	                return view('accounts.admin', $data);

  						}








}
