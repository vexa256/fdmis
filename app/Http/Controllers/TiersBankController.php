<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
//use App\banks as Banks;
//use App\Bankfloat;

use App\User;
use App\BankTiers as Tiers;

class TiersBankController extends Controller
{




        public function ManageBankTiers($id)
        {

            $Banks = Banks::find($id);
            $BankID = $Banks->Bank_ID;
            $Data = Tiers::where('BankID', '=',  $BankID)->get();


                    $data = [

                        'Title' => 'Manage Bank Tiers',
                        'page' => 'tiers.ManageBankTiers',
                        'TierData' => $Banks,
                        'Banks' => $Data,
                        //'Notifications' => 'True',
                        //'message' => 'Bank successfully created',
                        'not_type' => 'success'

                       ];


                        return view('accounts.admin', $data);


        }






        	public function SubmitBankTiers(request $request)
        	{
        

                     $request->validate([
                            'range_from' => 'required|integer',
                            'range_to' => 'required|integer',
                            'BankName' => 'required',
                            'BankID' => 'required',
                            'DepositCharge' => 'required|integer',
                            'WithdrawCharge' => 'required|integer',
                            'WithdrawCommission' => 'required|integer',
                            'DepositCommission' => 'required|integer'
                        
                        ]);


                $Tiers = new Tiers;
                $Tiers->range_from = $request->input('range_from');
                $Tiers->range_to = $request->input('range_to');
                $Tiers->BankName = $request->input('BankName');
                $Tiers->BankID = $request->input('BankID');
                    $Tiers->DepositCharge = $request->input('DepositCharge');
                    $Tiers->WithdrawCharge = $request->input('WithdrawCharge');
                    $Tiers->WithdrawCommission = $request->input('WithdrawCommission');
                    $Tiers->DepositCommission = $request->input('DepositCommission');
                


                if ($Tiers->save()) {

                    $data = $request->input('BankName');

                     return redirect()->back()->with('status', 'Tier for Bank'.' '.$data.' '.'successfully created');

                }



        	}




        	public function DeleteBankTier($id)
        	{

        			$Tiers = Tiers::find($id);

        			if ($Tiers->delete()) {
        				
        				 return redirect()->back()->with('status', 'Bank Tier deleted successfully');

        			}

        	}



}
