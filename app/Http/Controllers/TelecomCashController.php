<?php
namespace App\Http\Controllers;

use App\TelecomCashAtHand as Cashathand;
use App\User;
use Illuminate\Support\Facades\Auth;

class TelecomCashController extends Controller
{
  public function ManageTelecomCash()
  {

    $Cashathand = Cashathand::where("OutletID", "=", Auth::user()->Outlet_ID)
      ->get();

    $Sum = Cashathand::where("OutletID", "=", Auth::user()->Outlet_ID)
      ->get()->sum('amount');

    $data = [

      'Title'    => 'Wallet Cash at Hand Accounts',
      'page'     => 'Cash.ManageTelecomCash',
      'Telecoms' => $Cashathand,
      'Total'    => number_format($Sum),
      //'Notifications' => 'True',
      //'message' => 'Telecom successfully created',
      'not_type' => 'success'

    ];

    return view('accounts.admin', $data);

  }
}
