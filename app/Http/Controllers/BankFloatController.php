<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat as BankData;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
//use App\banks as Banks;
use App\AgentBankAccounts as BankAccounts;
use App\User;
use App\BankTiers as Tiers;
use App\bankfloats_logs as BankLogs;

class BankFloatController extends Controller
{
    


			public function BankFloatRequestForm()
			{
				$Banks = BankAccounts::where("OutletID", Auth::user()->Outlet_ID)->get();

				$data = [

			    		'Title' => 'Bank Float Request',
			    		'page' => 'float.BankFloatRequestForm',
			    		'Banks' => $Banks
			    		//'Notifications' => 'True',
			    		/**'message' => 'Hello',
			    		'not_type' => 'success'**/

    	               ];


    	                return view('accounts.admin', $data);

			}




public function BankAgentFloatRequest(request $request)

	{

		if (Auth::user()->Account_type == 'Agent_User') {
		


			$hasher    =  $request->input('Amount');
			$hasher2   =  Carbon::now();
			$hasher3   = $hasher2->toDateTimeString();
			$finalHash = $hasher3.'2333ddf22'.$hasher;



		 $request->validate(['Amount' => 'required|integer', 'BankID' => 'required', ]);

		 $BankID = $request->input('BankID');

		 $BankAccounts = BankAccounts::find($BankID);


		 if (BankData::where('BankAccount', $BankAccounts->BankAccount)->exists()) {

		$data = BankData::where('BankAccount', $BankAccounts->BankAccount)->first();
		 		
		    $data_id = $data->id;

		 	$CurrentRequestAmount = $data->request_amount;


		$MakeRequest = BankData::find($data_id);


				if ($MakeRequest->status == 'Approved') {
					
					$MakeRequest->request_amount = $request->input('Amount');


				} else {


					$MakeRequest->request_amount = $CurrentRequestAmount + $request->input('Amount');


				}



		
		$MakeRequest->status = "Pending Approval";

		$data_Unique = $MakeRequest->transactionID;

			if ($MakeRequest->save()) {



		$Float_log = new BankLogs;


			
		 $Float_log->AgencyID =  $BankAccounts->AgencyID;
		 $Float_log->AgencyName =  $BankAccounts->AgencyName;
		 $Float_log->AgentName = Auth::user()->name;
		 $Float_log->OutletName =  $BankAccounts->OutletName;
		 $Float_log->OutletID =  $BankAccounts->OutletID;
		 $Float_log->BankName =  $BankAccounts->BankName;
		 $Float_log->BankID =  $BankAccounts->BankID;
		 $Float_log->AccountID =  $data_Unique;
		 $Float_log->BankAccount =  $BankAccounts->BankAccount;
		 $Float_log->status =  "Pending Approval";
		 $Float_log->request_amount =  $request->input('Amount');
		 $Float_log->transactionID = $data_Unique;
		 $Float_log->save();



				


				 return redirect()->route('BankAgentFloatRequestsHistory')->with('status', 'Float request to your parent Agent made successfully and is pending approval');



			}/******inside if closure*************/

    
 } else {

 	$Float_log = new BankLogs;


      
		 $Float_log->AgencyID =  $BankAccounts->AgencyID;
		 $Float_log->AgencyName =  $BankAccounts->AgencyName;
		 $Float_log->AgentName = Auth::user()->name;
		 $Float_log->OutletName =  $BankAccounts->OutletName;
		 $Float_log->OutletID =  $BankAccounts->OutletID;
		 $Float_log->BankName =  $BankAccounts->BankName;
		 $Float_log->BankID =  $BankAccounts->BankID;
		 $Float_log->AccountID =  $finalHash;
		 $Float_log->BankAccount =  $BankAccounts->BankAccount;
		 $Float_log->status =  "Pending Approval";
		 $Float_log->request_amount =  $request->input('Amount');
		 $Float_log->transactionID = $finalHash; 




 	$Float = new BankData;



      
		 $Float->AgencyID =  $BankAccounts->AgencyID;
		 $Float->AgencyName =  $BankAccounts->AgencyName;
		 $Float->AgentName = Auth::user()->name;
		 $Float->OutletName =  $BankAccounts->OutletName;
		 $Float->OutletID =  $BankAccounts->OutletID;
		 $Float->BankName =  $BankAccounts->BankName;
		 $Float->BankID =  $BankAccounts->BankID;
		 $Float->AccountID =  $finalHash;
		 $Float->BankAccount =  $BankAccounts->BankAccount;
		 $Float->status =  "Pending Approval";
		 $Float->request_amount =  $request->input('Amount');
		  $Float->transactionID = $finalHash; 
	

		 if ($Float->save() &&  $Float_log->save()) {
		 	

		 		 return redirect()->route('BankAgentFloatRequestsHistory')->with('status', 'Float request to your parent Agent made successfully and is pending approval');


		 }
		


 }/**********Else closure*****************/



}
		 
		
	}/***********function closure***************/



public function BankAgentFloatRequestsHistory()
	{
		

		$Float = BankData::where("OutletID", Auth::user()->Outlet_ID)->get();
		$Float_log = BankLogs::where("OutletID", Auth::user()->Outlet_ID)->get();
		//$OutletName = $Float->OutletName;

       	$data = [

	       'Title' => 'Bank Float Request History ',
	       'page' => 'float.ManageBankFloatRequest',
	       'Floats' => $Float,
	       'Float_logs' => $Float_log,
	       'OutletName' =>Auth::user()->Outlet_Name,
	       
    	  ];


	

  			return view('accounts.admin', $data);





	}




	public function Approve_Bank_Float($id)
	{



			$BankData = BankData::find($id);
			$data_Unique = $BankData->transactionID;
			$BankData->status =  "Approved";

			$oldFloat = $BankData->request_amount;
			$newFloat = $BankData->amount;
			$BankData->amount = $oldFloat + $newFloat;

			$BankLogs = BankLogs::where("transactionID", "=", $data_Unique)->first();
			$id = $BankLogs->id;

			$BankLogsSubmit = BankLogs::find($id);

			$BankLogsSubmit->status =  "Approved";

			if ($BankData->save() && $BankLogsSubmit->save()) {
				

				 return redirect()->route('BankAgentFloatRequestsHistory')->with('status', 'Float request to from child Agent-User approved successfully');


			}
		


	}







	public function DeleteBankFloatRequestHistory($id)
	{
		
		$BankData = BankData::find($id);

				 if ($BankData->delete()) {


	 						 return redirect()->route('BankAgentFloatRequestsHistory')->with('status', 'Float request deleted successfully');


						 }



	}




}
