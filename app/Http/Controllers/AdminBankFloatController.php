<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\AgencyBankFloat as floatData;
use App\banks as Banks;
use App\outlets;
use App\User;
use App\AgencyBankAccounts as BankAccounts;
use App\AgencyBankFloatLogs as BankLogs;
use App\bankfloats_logs as AgentBankLogs;
use App\Bankfloat as AgentfloatData;

class AdminBankFloatController extends Controller
{




    
    public function AdminBankAgencyFloat()
	{
	

		$Float = floatData::all();
		$Float_log = BankLogs::all();
		//$OutletName = $Float->OutletName;

       	$data = [

	       'Title' => 'Hello Admin, Manage Agent Float Requests',
	       'page' => 'Admin.BankFloat',
	       'Floats' => $Float,
	       'Float_logs' => $Float_log,
	      // 'OutletName' =>Auth::user()->Outlet_Name,
	       
    	  ];


	

  			return view('accounts.admin', $data);

	}





		public function AdminApproveAgencyBankFloat($id)
		{
			$floatData = floatData::find($id);
			$data_Unique = $floatData->transactionID;

			if (Auth::user()->User_role == "Master") {
				

				if ($floatData->status == "Approved") {

						 return redirect()->route('AdminBankAgencyFloat')->with('status', 'Float request already approved by admin');
					
				} else {

					
			$floatData->status =  "Approved";

			$oldFloat = $floatData->request_amount;
			$newFloat = $floatData->amount;
			$floatData->amount = $oldFloat + $newFloat;

			$BankLogs = BankLogs::where("transactionID", "=", $data_Unique)->first();
			$id = $BankLogs->id;

			$BankLogsSubmit = BankLogs::find($id);

			$BankLogsSubmit->status =  "Approved";

			if ($floatData->save() && $BankLogsSubmit->save()) {
				

				 return redirect()->route('AdminBankAgencyFloat')->with('status', 'Float request to from Agent approved successfully');


			}



				}
			
			
			}/****Auth*******/
		}










} /***********************Classs************************/
