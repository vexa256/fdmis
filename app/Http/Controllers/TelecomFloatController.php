<?php
namespace App\Http\Controllers;

use App\AgentPhoneNumbers as PhoneNumbers;
use App\Telecomfloat as floatData;
use App\telecomfloats_logs as TelecomLogs;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TelecomFloatController extends Controller
{
  public function TelecomFloatRequest()
  {

    $Telecoms = PhoneNumbers::where("OutletID", Auth::user()->Outlet_ID)->get();

    $data = [

      'Title'    => 'Wallet Float Request',
      'page'     => 'float.FloatRequest',
      'Telecoms' => $Telecoms

//'Notifications' => 'True',
      /**'message' => 'Hello',
    'not_type' => 'success'**/

    ];

    return view('accounts.admin', $data);
  }

  /**
   * @param request $request
   */
  public function TelecomAgentFloatRequest(request $request)
  {

    if (Auth::user()->Account_type == 'Agent_User')
    {
      $hasher = $request->input('Amount');
      $hasher2 = Carbon::now();
      $hasher3 = $hasher2->toDateTimeString();
      $finalHash = Hash::make($hasher3.'2333ddf22'.$hasher);

      define('finalHash', $finalHash);

      $request->validate(['Amount' => 'required', 'TelecomID' => 'required']);

      $TelecomID = $request->input('TelecomID');

      $PhoneNumbers = PhoneNumbers::find($TelecomID);

      if (floatData::where('PhoneNumber', $PhoneNumbers->PhoneNumber)->exists())
      {
        $data = floatData::where('PhoneNumber', $PhoneNumbers->PhoneNumber)->first();

        $data_id = $data->id;

        $CurrentRequestAmount = $data->request_amount;

        $MakeRequest = floatData::find($data_id);

        if ($MakeRequest->status == 'Approved')
        {
          $MakeRequest->request_amount = $request->input('Amount');
        }
        else
        {

          $MakeRequest->request_amount = $CurrentRequestAmount + $request->input('Amount');
        }

        $MakeRequest->status = "Pending Approval";

        $data_Unique = $MakeRequest->transactionID;

        if ($MakeRequest->save())
        {
          $Float_log = new TelecomLogs();

          $Float_log->AgencyID = $PhoneNumbers->AgencyID;
          $Float_log->AgencyName = $PhoneNumbers->AgencyName;
          $Float_log->AgentName = Auth::user()->name;
          $Float_log->OutletName = $PhoneNumbers->OutletName;
          $Float_log->OutletID = $PhoneNumbers->OutletID;
          $Float_log->TelecomName = $PhoneNumbers->TelecomName;
          $Float_log->TelecomID = $PhoneNumbers->TelecomID;
          $Float_log->AccountID = $data_Unique;
          $Float_log->PhoneNumber = $PhoneNumbers->PhoneNumber;
          $Float_log->status = $MakeRequest->status;
          $Float_log->request_amount = $request->input('Amount');
          $Float_log->transactionID = $data_Unique;
          $Float_log->save();

          return redirect()->route('AgentFloatRequestsHistory')->with('status', 'Float request to your parent Agent made successfully and is pending approval');
        }
/******inside if closure*************/
      }
      else
      {

        $Float_log = new TelecomLogs();

        $Float_log->AgencyID = $PhoneNumbers->AgencyID;
        $Float_log->AgencyName = $PhoneNumbers->AgencyName;
        $Float_log->AgentName = Auth::user()->name;
        $Float_log->OutletName = $PhoneNumbers->OutletName;
        $Float_log->OutletID = $PhoneNumbers->OutletID;
        $Float_log->TelecomName = $PhoneNumbers->TelecomName;
        $Float_log->TelecomID = $PhoneNumbers->TelecomID;
        $Float_log->AccountID = finalHash;
        $Float_log->PhoneNumber = $PhoneNumbers->PhoneNumber;
        $Float_log->status = "Pending Approval";
        $Float_log->request_amount = $request->input('Amount');
        $Float_log->transactionID = finalHash;

        $Float = new floatData();

        $Float->AgencyID = $PhoneNumbers->AgencyID;
        $Float->AgencyName = $PhoneNumbers->AgencyName;
        $Float->AgentName = Auth::user()->name;
        $Float->OutletName = $PhoneNumbers->OutletName;
        $Float->OutletID = $PhoneNumbers->OutletID;
        $Float->TelecomName = $PhoneNumbers->TelecomName;
        $Float->TelecomID = $PhoneNumbers->TelecomID;
        $Float->AccountID = finalHash;
        $Float->PhoneNumber = $PhoneNumbers->PhoneNumber;
        $Float->status = "Pending Approval";
        $Float->request_amount = $request->input('Amount');
        $Float->transactionID = finalHash;

        if ($Float->save() && $Float_log->save())
        {
          return redirect()->route('AgentFloatRequestsHistory')->with('status', 'Float request to your parent Agent made successfully and is pending approval');
        }
      }
/**********Else closure*****************/
    }
  }

/***********function closure***************/

  public function AgentFloatRequestsHistory()
  {

    $Float = floatData::where("OutletID", Auth::user()->Outlet_ID)->get();
    $Float_log = TelecomLogs::where("OutletID", Auth::user()->Outlet_ID)->get();
    //$OutletName = $Float->OutletName;

    $data = [

      'Title'      => 'Float Requests',
      'page'       => 'float.ManageTelecomFloatRequest',
      'Floats'     => $Float,
      'Float_logs' => $Float_log,
      'OutletName' => Auth::user()->Outlet_Name

    ];

    return view('accounts.admin', $data);
  }

  /**
   * @param $id
   */
  public function approveTelecomFloat($id)
  {

    $floatData = floatData::find($id);
    $data_Unique = $floatData->transactionID;
    $floatData->status = "Approved";

    $oldFloat = $floatData->request_amount;
    $newFloat = $floatData->amount;
    $floatData->amount = $oldFloat + $newFloat;

    $TelecomLogs = TelecomLogs::where("transactionID", "=", $data_Unique)->first();
    $id = $TelecomLogs->id;

    $TelecomLogsSubmit = TelecomLogs::find($id);

    $TelecomLogsSubmit->status = "Approved";

    if ($floatData->save() && $TelecomLogsSubmit->save())
    {
      return redirect()->route('AgentFloatRequestsHistory')->with('status', 'Float request to from child Agent-User approved successfully');
    }
  }

  /**
   * @param $id
   */
  public function DeleteFloatRequestHistory($id)
  {

    $floatData = floatData::find($id);

    if ($floatData->delete())
    {
      return redirect()->route('AgentFloatRequestsHistory')->with('status', 'Float request deleted successfully');
    }
  }
}
/***********classs closure*******************/
