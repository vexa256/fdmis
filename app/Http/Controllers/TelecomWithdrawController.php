<?php
namespace App\Http\Controllers;

use App\AgentPhoneNumbers as PhoneNumbers;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\Telecomfloat as floatData;
use App\TelecomTiers as Tiers;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

//use App\TelecomTiers as Tiers;

class TelecomWithdrawController extends Controller
{
  public function TelecomWithdrawForm()
  {

    $Telecoms = PhoneNumbers::where("OutletID", "=", Auth::user()->Outlet_ID)->get();

    $data = [

      'Title'    => 'Wallet Withdraw Form',
      'page'     => 'telecom.TelecomWithdraw',
      'Telecoms' => $Telecoms,

//'Notifications' => 'True',
      //'message' => 'Bank successfully created',
      'not_type' => 'success'

    ];

    return view('accounts.admin', $data);
  }

  /***Withdraw Logic****************/

  /**
   * @param request $request
   */
  public function TelecomWithdrawLogic(request $request)
  {

    define("AuthorizedUserName", Auth::user()->name);
    define("AuthorizedAgencyName", Auth::user()->Agency_name);
    define("AuthorizedAgencyID", Auth::user()->Agency_id);
    define("AuthorizedAgentID", Auth::user()->Agent_id);
    define("AuthorizedAgentName", Auth::user()->name);
    define("AuthorizedOutletID", Auth::user()->Outlet_ID);
    define("AuthorizedOutletName", Auth::user()->Outlet_Name);

    $request->validate([
      'ClientPhone'    => 'required',
      'TotalWithdraw'  => 'required|integer',
      'TelecomCarrier' => 'required'

    ]);

    $TotalWithdraw = $request->input("TotalWithdraw");
    $RecipientNumber = $request->input('ClientPhone');
    $TelecomID = $request->input('TelecomCarrier');

    $Float = 0;
    $Cashathand = $TotalWithdraw;
    $WithdrawCharge = 0;
    $WithdrawCharge = 0;
    $WithdrawCommission = 0;
    //WithdrawCommission = 0;

    define('TotalWithdrawUser', $request->input("TotalWithdraw"));

    function generatePin($number)
    {
      // Generate set of alpha characters
      $alpha = array();

      for ($u = 65; $u <= 90; $u++)
      {
        // Uppercase Char
        array_push($alpha, chr($u));
      }

/***Timothy disable this for now***/

/**This whole system is dedicated to jesus christ my lord and is for the purpose of glorigying him and his church on earth. it will stay active and healthy and relevant until christ comes back****/

// Just in case you need lower case

// for ($l = 97; $l <= 122; $l++) {

//    // Lowercase Char

//    array_push($alpha, chr($l));

// }

      // Get random alpha character
      $rand_alpha_key = array_rand($alpha);
      $rand_alpha = $alpha[$rand_alpha_key];

      // Add the other missing integers
      $rand = array($rand_alpha);

      for ($c = 0; $c < $number - 1; $c++)
      {
        array_push($rand, mt_rand(0, 9));
        shuffle($rand);
      }

      return implode('', $rand);
    }

    define("TransactionID", generatePin(8));

    $PhoneNumberAccount = PhoneNumbers::find($TelecomID);

    $FloatAccountID = $PhoneNumberAccount->PhoneNumber;

    $FloatAccount = floatData::where("PhoneNumber", "=", $FloatAccountID)->first();

    if (is_null($FloatAccount))
    {
      return redirect()->route('TelecomWithdrawForm')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');
    }
    else
    {

      $FinalFloatAccount = floatData::find($FloatAccount->id);
    }

    /****isolate Telecom ID*********/

    $id_z = $request->input('TelecomCarrier');

    $data_z = PhoneNumbers::find($id_z);

    $TelLID = $data_z->TelecomID;

    $Approved_telecom = $TelLID;

    /****isolate Telecom ID*********/

    define("SelectedTelecomID", $Approved_telecom);

    $Tiers = Tiers::where("TelecomID", "=", SelectedTelecomID);

    $Checker_init_func = floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->first();

    if (!floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->exists())
    {
      return redirect()->route('TelecomWithdrawForm')->with('status', ' Sorry, You are not authorized to execute this operation, No Float Account Detected, Please create one by requesting for float');

/*****repeated to prevent Adapter code edits******/
    }
    elseif (!floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->exists())
    {

      return redirect()->route('TelecomWithdrawForm')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');
    }
    else
    {

      $Tiers_Telecom = Tiers::where("TelecomID", "=", SelectedTelecomID)

        ->where("range_from", "<=", TotalWithdrawUser)
        ->where("range_to", ">=", TotalWithdrawUser)->first();

      if (!is_null($Tiers_Telecom))
      {
        $DPC = $Tiers_Telecom->WithdrawCommission;

        define('WithdrawCommission', $DPC);
      }
      elseif (is_null($Tiers_Telecom))
      {

        return redirect()->route('TelecomWithdrawForm')->with('status', ' Sorry, No Tiers assigned.');
      }

      if (Commission::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)

        ->where('OutletID', AuthorizedOutletID)

        ->exists())
      {
        $Checker_for_WithdrawCommission = Commission::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)

          ->where('OutletID', AuthorizedOutletID)

          ->first();

        $CurrentWithdrawCommission = $Checker_for_WithdrawCommission->WithdrawCommission;

        $FinalWithdrawCommission_commit = $CurrentWithdrawCommission + WithdrawCommission;

        /**Trigger Withdraw Commision Update only ***/

        $UniqueCommissionID = $Checker_for_WithdrawCommission->id;

        $UpdateDb = Commission::find($UniqueCommissionID);

        $UpdateDb->WithdrawCommission = $FinalWithdrawCommission_commit;

        $UpdateDb->save();

        /**Trigger Withdraw Commision Update only ***/
      }
      else
      {

        /**new Withdraw  commission entry***/

        $DeositCommission = new Commission();

        $DeositCommission->TelecomID = SelectedTelecomID;
        $DeositCommission->TelecomName = $PhoneNumberAccount->TelecomName;
        $DeositCommission->WithdrawCommission = WithdrawCommission;
        $DeositCommission->AgencyID = $PhoneNumberAccount->AgencyID;
        $DeositCommission->AgencyName = $PhoneNumberAccount->AgencyName;
        $DeositCommission->AgentID = AuthorizedAgentID;
        $DeositCommission->AgentName = AuthorizedUserName;
        $DeositCommission->OutletName = $PhoneNumberAccount->OutletName;
        $DeositCommission->TransactionType = "Telecom Withdraw";
        $DeositCommission->OutletID = $PhoneNumberAccount->OutletID;
        $DeositCommission->AccounID = $PhoneNumberAccount->OutletID;
        $DeositCommission->PhoneNumber = $PhoneNumberAccount->PhoneNumber;
        $DeositCommission->save();
      }

/****Withdraw commission closure ***/

/****ask about if cash at hand is not enough****/

/****ask about deducting charges from cash at hand***/

      if (Cashathand::where('TelecomPhoneNumber', $PhoneNumberAccount->PhoneNumber)

        ->where('OutletID', AuthorizedOutletID)->exists())
      {
        $IsolateCashAccount = Cashathand::where('TelecomPhoneNumber', $PhoneNumberAccount->PhoneNumber)

          ->where('OutletID', AuthorizedOutletID)

          ->first();

        $Cash_uiniqueID = $IsolateCashAccount->id;

        $CashAccountFinal = Cashathand::find($Cash_uiniqueID);
        $CurrentCashathand = $CashAccountFinal->amount;
        $CashAccountFinal->amount = $CurrentCashathand - $Cashathand;
        $CashAccountFinal->save();
      }
      else
      {

        $NewcashCashathand = new Cashathand();

        $NewcashCashathand->TelecomID = SelectedTelecomID;
        $NewcashCashathand->TelecomName = $PhoneNumberAccount->TelecomName;
        $NewcashCashathand->TelecomPhoneNumber = $PhoneNumberAccount->PhoneNumber;
        $NewcashCashathand->TransactionPerson = $PhoneNumberAccount->AuthorizedUserName;
        $NewcashCashathand->AgencyID = $PhoneNumberAccount->AgencyID;
        $NewcashCashathand->AgencyName = $PhoneNumberAccount->AgencyName;
        $NewcashCashathand->AgentID = $PhoneNumberAccount->Agent_id;
        $NewcashCashathand->AgentName = $PhoneNumberAccount->AuthorizedUserName;
        $NewcashCashathand->OutletName = $PhoneNumberAccount->OutletName;
        $NewcashCashathand->OutletID = $PhoneNumberAccount->OutletID;
        $NewcashCashathand->amount = $PhoneNumberAccount->Cashathand;
        $NewcashCashathand->AccounID = $PhoneNumberAccount->OutletID;
        $NewcashCashathand->save();
      }

/***Float Account***/

/****ask about deducting charges from float account***/

      if (floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)->where('OutletID', AuthorizedOutletID)->exists())
      {
        $IsolateFloatAccount = floatData::where('PhoneNumber', $PhoneNumberAccount->PhoneNumber)

          ->where('OutletID', AuthorizedOutletID)

          ->first();

        $FloatAccount_isolated = floatData::find($IsolateFloatAccount->id);

        $CurrentFloatAmount = $FloatAccount_isolated->amount;

        $FloatAccount_isolated->amount = $CurrentFloatAmount + $TotalWithdraw;

        $FloatAccount_isolated->save();

        $Float = $FloatAccount_isolated->amount;
      }

      /**Create a transaction ***********/

      $TelecomTransactions = new TelecomTransactions();
      $TelecomTransactions->TransID = TransactionID;
      $TelecomTransactions->Customer_Name = '';

      $TelecomTransactions->Transaction_Type = "Withdraw";

      $TelecomTransactions->Running_Balance = $Float;

      $TelecomTransactions->Withdraw_Commission = WithdrawCommission;

      $TelecomTransactions->TelecomName = $PhoneNumberAccount->TelecomName;
      $TelecomTransactions->TelecomID = SelectedTelecomID;
      $TelecomTransactions->AgentPhoneNumber = $PhoneNumberAccount->PhoneNumber;
      $TelecomTransactions->Client_Phone = $RecipientNumber;
      $TelecomTransactions->AgencyID = AuthorizedAgencyID;
      $TelecomTransactions->Reciever_Phone = $RecipientNumber;
      $TelecomTransactions->AgencyName = AuthorizedAgencyName;
      $TelecomTransactions->AgentID = AuthorizedAgentID;
      $TelecomTransactions->AgentName = AuthorizedUserName;
      $TelecomTransactions->OutletName = AuthorizedOutletName;
      $TelecomTransactions->OutletID = AuthorizedOutletID;
      $TelecomTransactions->status = "Successfull";
      $TelecomTransactions->amount = $TotalWithdraw;
      $TelecomTransactions->save();

/**Create a transaction ***********/

      /*****log transaction***************/

      $TelecomTransactionsLogs = new TelecomTransactionsLogs();
      $TelecomTransactionsLogs->TransID = TransactionID;
      $TelecomTransactionsLogs->Customer_Name = '';

      $TelecomTransactionsLogs->Transaction_Type = "Withdraw";

      $TelecomTransactionsLogs->Running_Balance = $Float;

      $TelecomTransactionsLogs->Withdraw_Commission = WithdrawCommission;

      $TelecomTransactionsLogs->TelecomName = $PhoneNumberAccount->TelecomName;
      $TelecomTransactionsLogs->TelecomID = SelectedTelecomID;
      $TelecomTransactionsLogs->AgentPhoneNumber = $PhoneNumberAccount->PhoneNumber;
      $TelecomTransactionsLogs->Client_Phone = $RecipientNumber;
      $TelecomTransactionsLogs->AgencyID = AuthorizedAgencyID;
      $TelecomTransactionsLogs->Reciever_Phone = $RecipientNumber;
      $TelecomTransactionsLogs->AgencyName = AuthorizedAgencyName;
      $TelecomTransactionsLogs->AgentID = AuthorizedAgentID;
      $TelecomTransactionsLogs->AgentName = AuthorizedUserName;
      $TelecomTransactionsLogs->OutletName = AuthorizedOutletName;
      $TelecomTransactionsLogs->OutletID = AuthorizedOutletID;
      $TelecomTransactionsLogs->status = "Successfull";
      $TelecomTransactionsLogs->amount = $TotalWithdraw;
      $TelecomTransactionsLogs->save();

      return redirect()->route('home')->with('status', 'Hello , Withdraw Made successfully');
    }
/****if***/
  }

/****Mother FUNCTION***********/
}
/**********CLASS****/
