<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
use App\telcom as telecom;
use App\Telecomfloat;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
  




			public function AdminCreateAgencyAccount()
			{
					 $data = [

					                'Title' => 'Create Agent  Account',
					                'page' => 'Admin.CreateAgency',
					                'Desc' => 'Agent Account Setup Form',
					                'User_role' => 'Agency',


             					  ];




                    return view('accounts.admin', $data);





			}



				public function AddAgencyAccount(request $request)
			{

			 	$request->validate([
				         'email'  => 'required|max:255|email',
				         'password' => 'required|confirmed',
				         'name' => 'required',
				    ]);


					$User = User::where("email", "=", $request->input('email'))->first();

					define('User_idHash', Hash::make($request->input('email')));
					

					if (is_null($User)) {
					

					$Users = new User;
					//$Users->agent_phone = $request->input('phone');
					$Users->name = $request->input('name');
					$Users->password = Hash::make($request->input('password'));
					$Users->email = $request->input('email');
					$Users->Account_type = $request->input('User_role');
					$Users->User_id = User_idHash;
					$Users->Agency_id = User_idHash;
					$Users->Agency_name = $request->input('name');
					//$Users->Agent_name = $request->input('name');
					$Users->User_role = $request->input('User_role');
					//$Users->agency_phone = Auth::user()->agency_phone;
					$Users->Username = $request->input('email');

				if ($Users->save())

						{

							if ($request->input('User_role') == 'Master') {

								 return redirect()->route('ManageAdmins')->with('status', 'Admin Account Created Successfully');
								
							}else {



				 return redirect()->route('ManageAgency')->with('status', 'Agent Account Created Successfully');


							}




						}



					}/************Auth***********/else {

						 return redirect()->route('AdminCreateAgencyAccount')->with('status', 'Email Already in use. Please Try Again');

					}/*********else**********/



			}





	public function ManageAgency()
	{
			$Users = User::where("User_role", "=", "Agency")->get();


			 $data = [

					                'Title' => 'Account Management',
					                'page' => 'Admin.ManageAgencyAcc',
					                'Agents' => $Users,
					                'Desc' => 'Manage all Agent Accounts',


             					  ];

                      return view('accounts.admin', $data);

	}






	public function ManageAdmins()
	{
			$Users = User::where("User_role", "=", "Master")->get();


			 $data = [

					                'Title' => 'Master Account Management',
					                'page' => 'Admin.ManageAgencyAcc',
					                'Agents' => $Users,
					                'Desc' => 'Manage all Admin Accounts',


             	 ];

                      return view('accounts.admin', $data);

	}




	public function DeleteAgency($id)
	{
		
			$Users = User::find($id);

			if ($Users->delete()) {


				 return redirect()->back()->with('status', ' Account Deleted Successfully');




			}
	}






			public function AdminCreateAccount()
			{
					 $data = [

					                'Title' => 'Create Admin  Account',
					                'page' => 'Admin.CreateAgency',
					                'Desc' => 'Admin Account Setup',
					                'User_role' => 'Master',


             					  ];




                    return view('accounts.admin', $data);





			}




			public function AdminStats()
			{
				
				$AgentUsers = User::where("User_role", "=", "Agent_User")->count();

				$Agency = User::where("User_role", "=", "Agency")->count();


				$Admins = User::where("User_role", "=", "Master")->count();

				$telecoms = telecom::all()->count();


				$outlets = outlets::all()->count();


				$banks = banks::all()->count();



				 $data = [

					                'Title' => 'Administrator Mini Reports',
					                'page' => 'Admin.Stats',
					                'AgentUsers' => $AgentUsers ,
					                'Agency' => $Agency ,
					                'Admins' => $Admins ,
					                'telecoms' => $telecoms ,
					                'outlets' => $outlets ,
					                'banks' => $banks ,
					                

             					  ];




                    return view('accounts.admin', $data);











			}


}/******************CLASS***********/
