<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\outlets;
use App\telcom as Telecoms;
use App\AgencyTelecomFloat as floatData;
use App\User;
use App\AgencyTelecomFloatLogs as TelecomLogs;
use App\AgencyBankFloatLogs;
use App\TelecomTiers as Tiers;
use App\AgencyTelecomNumbers as PhoneNumbers;
//use App\telcom as Telecoms;
use App\Telecomfloat as AgentfloatData;
use App\telecomfloats_logs as AgentTelecomLogs;

class AgencyTelecomFloatController extends Controller
{



   			public function AgencyTelecomFloatRequest() {


		     	$Telecoms = PhoneNumbers::where("AgencyID", Auth::user()->Agency_id)->get();

				$data = [

			    		'Title' => 'Agent Telecom Float Request',
			    		'page' => 'Agency.AgencyFloatRequest',
			    		'Telecoms' => $Telecoms
			    		//'Notifications' => 'True',
			    		/**'message' => 'Hello',
			    		'not_type' => 'success'**/

    	               ];


    	                return view('accounts.admin', $data);



		}






public function TelecomAgencyFloatRequest(request $request)

	{

		if (Auth::user()->Account_type == 'Agency') {



			$hasher    =  $request->input('Amount');
			$hasher2   =  Carbon::now();
			$hasher3   = $hasher2->toDateTimeString();
			$finalHash = $hasher3.'2333ddf22'.$hasher;



		 $request->validate(['Amount' => 'required', 'TelecomID' => 'required', ]);

		 $TelecomID = $request->input('TelecomID');

		 $PhoneNumbers = PhoneNumbers::find($TelecomID);


		 if (floatData::where('PhoneNumber', $PhoneNumbers->PhoneNumber)->exists()) {

		 	$data = floatData::where('PhoneNumber', $PhoneNumbers->PhoneNumber)->first();

		    $data_id = $data->id;

		 	$CurrentRequestAmount = $data->request_amount;


		$MakeRequest = floatData::find($data_id);


				if ($MakeRequest->status == 'Approved') {

					$MakeRequest->request_amount = $request->input('Amount');


				} else {


					$MakeRequest->request_amount = $CurrentRequestAmount + $request->input('Amount');


				}




		$MakeRequest->status = "Pending Approval";

		$data_Unique = $MakeRequest->transactionID;

			if ($MakeRequest->save()) {



		$Float_log = new TelecomLogs;



		 $Float_log->AgencyID =  $PhoneNumbers->AgencyID;
		 $Float_log->AgencyName =  $PhoneNumbers->AgencyName;
		 //$Float_log->AgentName = Auth::user()->name;
		// $Float_log->OutletName =  $PhoneNumbers->OutletName;
		 //$Float_log->OutletID =  $PhoneNumbers->OutletID;
		 $Float_log->TelecomName =  $PhoneNumbers->TelecomName;
		 $Float_log->TelecomID =  $PhoneNumbers->TelecomID;
		 $Float_log->AccountID =  $data_Unique;
		 $Float_log->PhoneNumber =  $PhoneNumbers->PhoneNumber;
		 $Float_log->status =  "Pending Approval";
		 $Float_log->request_amount =  $request->input('Amount');
		 $Float_log->transactionID = $data_Unique;






				 return redirect()->route('AgencyFloatRequestsHistory')->with('status', 'Float request to HD Resources LTD made successfully and is pending approval');



			}/******inside if closure*************/


 } else {

 	$Float_log = new TelecomLogs;



		 $Float_log->AgencyID =  $PhoneNumbers->AgencyID;
		 $Float_log->AgencyName =  $PhoneNumbers->AgencyName;
		 //$Float_log->AgentName = Auth::user()->name;
		 //$Float_log->OutletName =  $PhoneNumbers->OutletName;
		 //$Float_log->OutletID =  $PhoneNumbers->OutletID;
		 $Float_log->TelecomName =  $PhoneNumbers->TelecomName;
		 $Float_log->TelecomID =  $PhoneNumbers->TelecomID;
		 $Float_log->AccountID =  $finalHash;
		 $Float_log->PhoneNumber =  $PhoneNumbers->PhoneNumber;
		 $Float_log->status =  "Pending Approval";
		 $Float_log->request_amount =  $request->input('Amount');
		 $Float_log->transactionID = $finalHash;




 	$Float = new floatData;




		 $Float->AgencyID =  $PhoneNumbers->AgencyID;
		 $Float->AgencyName =  $PhoneNumbers->AgencyName;
		 //$Float->AgentName = Auth::user()->name;
		// $Float->OutletName =  $PhoneNumbers->OutletName;
		// $Float->OutletID =  $PhoneNumbers->OutletID;
		 $Float->TelecomName =  $PhoneNumbers->TelecomName;
		 $Float->TelecomID =  $PhoneNumbers->TelecomID;
		 $Float->AccountID =  $finalHash;
		 $Float->PhoneNumber =  $PhoneNumbers->PhoneNumber;
		 $Float->status =  "Pending Approval";
		 $Float->request_amount =  $request->input('Amount');
		  $Float->transactionID = $finalHash;


		 if ($Float->save() &&  $Float_log->save()) {


		 		 return redirect()->route('AgencyFloatRequestsHistory')->with('status', 'Float request to HD Resources LTD made successfully and is pending approval');


		 }



 }/**********Else closure*****************/



}


	}/***********function closure***************/




		public function AgencyFloatRequestsHistory()
	{


		$Float = floatData::where("AgencyID", Auth::user()->Agency_id)->get();
		$Float_log = TelecomLogs::where("AgencyID", Auth::user()->Agency_id)->get();
		//$OutletName = $Float->OutletName;

       	$data = [

	       'Title' => 'Float Requests',
	       'page' => 'Agency.TelecomFloatRequestHistory',
	       'Floats' => $Float,
	       'Float_logs' => $Float_log,
	      // 'OutletName' =>Auth::user()->Outlet_Name,

    	  ];




  			return view('accounts.admin', $data);





	}











  public function AgencyApproveAgentFloatRequest()
  {
    $Float = AgentfloatData::where("AgencyID", Auth::user()->Agency_id)->get();
    $Float_log = AgentTelecomLogs::where("AgencyID", Auth::user()->Agency_id)->get();
    $floatData = floatData::where("AgencyID", Auth::user()->Agency_id)->get();
    //$OutletName = $Float->OutletName;

        $data = [

         'Title' => 'Float Requests',
         'page' => 'Agency.ApproveTelecomFloat',
         'Floats' => $Float,
         'Telecoms' => $floatData,
         'Float_logs' => $Float_log,
         //'OutletName' =>Auth::user()->Outlet_Name,

        ];

        return view('accounts.admin', $data);
  }





     public function Agency_Approve_Telecom_Float(request $request)
	{		
			$AgentFloatRequestID =  $request->input('AgentFloatRequestID'); /***ID OF AgentfloatData::find(id) */
			$AgencyFloatAccID =  $request->input('AgencyFloatAccID'); /***ID OF floatData::find(id) *****/

			$u =  AgentfloatData::find($AgentFloatRequestID);
			$b =  floatData::find($AgencyFloatAccID);

			define('AgentAmountRequested', $u->request_amount);
			define('AgencyFloatAvailable', $b->amount);
			


			define('AgencyTelecomID', $b->TelecomID);
			define('AgentTelecomID', $u->TelecomID);
			

			if (AgencyFloatAvailable < AgentAmountRequested) {


				return redirect()->route('AgencyApproveAgentFloatRequest')->with('status', 'Sorry you do not have enough float on this telecom account to complete transaction, Please make a float request');
				
			}elseif (AgencyTelecomID != AgentTelecomID) {
				
				return redirect()->route('AgencyApproveAgentFloatRequest')->with('status', 'Transaction failed, you are attempting to do an inter-telecom float transfer. This action is not supported. Please try again');


			}else{

			/*****issue with transactionID on BOTH agentfloatcontrollers*****/



			$floatData = AgentfloatData::find($AgentFloatRequestID);
			$data_Unique = $floatData->transactionID;
			$floatData->status =  "Approved";

			$oldFloat = $floatData->request_amount;
			$newFloat = $floatData->amount;
			$floatData->amount = $oldFloat + $newFloat;

			$TelecomLogs = AgentTelecomLogs::where("transactionID", "=", $data_Unique)->first();
			$id = $TelecomLogs->id;

			$TelecomLogsSubmit = AgentTelecomLogs::find($id);

			$TelecomLogsSubmit->status =  "Approved";

				/****deduct agency float******/
				$b->amount = AgencyFloatAvailable - AgentAmountRequested;
				$b->save();

				/****deduct agency float******/

			if ($floatData->save() && $TelecomLogsSubmit->save()) {
				

				 return redirect()->route('AgencyApproveAgentFloatRequest')->with('status', 'Float request  from child Agent-User approved successfully, Check Float Request Logs for more details');


			}
		


	}
	}






}
