<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks as Banks;
//use App\BankTransactions;
//use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
use App\Bank;
//use App\Bankfloat;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\User;
use App\AgentBankAccounts as BankAccounts;

class BankController extends Controller
{
    



				public function BankSetupAccountForm()
				{
					



					      $data = [

					                'Title' => 'Bank Setup',
					                'page' => 'banks.BankSetup',
					             
                

             					  ];


					

                    return view('accounts.admin', $data);





				}





				public function SubmitBank(request $request)
				{
					

					$Banks = new Banks;
					$Banks->BankName = $request->input('BankName');
					$Banks->BranchName = $request->input('BranchName');
				    $hasher =  $request->input('BankName');
			        $hasher2 =  Carbon::now();
			        $hasher3 = $hasher2->toDateTimeString();
			        $finalHash= $hasher3.'chaAGENCYBANK'.$hasher;
			        $Banks->Bank_ID = Hash::make($finalHash);

			        $Banks->save();


			     return redirect()->route('ListBanks')->with('status', 'Banks Added Successfully');


				}




				public function ListBanks()
				{
					
					$Banks = Banks::all();


					$data = [

			    		'Title' => 'Bank Setup',
			    		'page' => 'banks.AllBanks',
			    		'Banks' => $Banks,
			    		//'Notifications' => 'True',
			    		//'message' => 'Bank successfully created',
			    		'not_type' => 'success'

    	               ];


    	                return view('accounts.admin', $data);




				}



				public function AssignBankAccounts()
				{
					


								$Banks = Banks::all();

				$BankAccounts = BankAccounts::all();

                $AgencyID = Auth::user()->Agency_id;

				$AgencyName = Auth::user()->Agency_name;
				

				$Users = User::where('Account_type', '=', 'Agent_User')->where('Agency_id', '=', $AgencyID)->get();

				$Outlets = outlets::where('AgencyID', '=', $AgencyID)->where('AgencyName', '=', $AgencyName)->get();

				          $data = [

					                'Title' => 'Manage Agent User Phone Numbers ',
					                'page' => 'banks.AssignBankAccounts',
					                'Agents' => $BankAccounts,
					                'Outlets' => $Outlets,
					                'Banks' => $Banks,
             					  ];


					

                    return view('accounts.admin', $data);







				}



		public function SubmitBankAccount(request $request)
	         

	         {


			         $request->validate([
					        'OutletAccountNumber' => 'required',
					        'BankID' => 'required',
					        'OutletID' => 'required'
					        
					    ]);


					$number = $request->input('OutletAccountNumber');

					//$checker = BankAccounts::where("PhoneNumber", "=", $id)->get();


					if (BankAccounts::where('BankAccount', '=', $number)->exists()) {
					

					 return redirect()->route('AssignBankAccounts')->with('status', 'Error Duplicate Merchant bank  account number, Please try again');



				} else {


		
			$BankAccounts = new BankAccounts;
			$BankID = $request->input('BankID');
			$OutletID = $request->input('OutletID');
			$Banks = Banks::find($BankID);
			$outlets = outlets::find($OutletID);

			$BankAccounts->AgencyID = Auth::user()->Agency_id;
			$BankAccounts->AgencyName = Auth::user()->Agency_name;
			$BankAccounts->OutletName = $outlets->outlet_name;
			$BankAccounts->OutletID = $outlets->outlet_id;

			$BankAccounts->BankName = $Banks->BankName;
			$BankAccounts->BranchName = $Banks->BranchName;
			$BankAccounts->BankID = $Banks->Bank_ID;
			$BankAccounts->BankAccount = $request->input('OutletAccountNumber');

			

			if ($BankAccounts->save()) {
			

			         return redirect()->route('AssignBankAccounts')->with('status', 'Merchant bank account number attached to outlet succesfully');

			}

}





	}

	/***Function End********/




	public function DeleteOutletAccountNumber($id)
	{
		

		$BankAccounts = BankAccounts::find($id);



		if ($BankAccounts->delete()) {
			

			         return redirect()->route('AssignBankAccounts')->with('status', 'Merchant bank account number deleted succesfully');

			}


	}













/************Class closure******************/
}
