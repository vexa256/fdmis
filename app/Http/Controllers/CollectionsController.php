<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Collections;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\AgencyBankAccounts as BankAcc;
use App\AgencyTelecomNumbers as TelecomNo;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\outlets;
use App\telcom as Telecoms;
use App\Telecomfloat as floatData;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use App\TelecomTiers as Tiers;
use App\AgentPhoneNumbers as PhoneNumbers;
use App\telecomfloats_logs as TelecomLogs;
use App\Bankfloat as BankData;
//use App\BankCommission as Commission;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
//use App\BankCashAtHand as Cashathand;
use App\AgentBankAccounts as BankAccounts;
//use App\User;
//use App\BankTiers as Tiers;
use App\bankfloats_logs as BankLogs;
use App\Traits\ImageUpload;
use App\Image;
use Illuminate\Support\Facades\Storage;

class CollectionsController extends Controller
{
    
use ImageUpload;

		public function ManageCollectors()
		{	
		    	$Collectors = User::where('User_role', '=', 'Collector')
		    						->where('Account_type', '=', 'Collector')->get();

                $Agents = User::where('User_role', '=', 'Agent_User')
                                    ->where('Account_type', '=', 'Agent_User')->get();

                $Banks = Banks::all();
                $Telecoms = Telecoms::all();                   
                 

			           $data = [
                            
	                            'Title' => 'Manage Collectors',
	                            'page' => 'collections.ManageCollectors',
	                            'Collectors' => $Collectors,
                                'Agents' => $Agents,
                                'Banks' => $Banks,
                                'Telecoms' => $Telecoms,
	                            'Total' => '' ,
                                'ManageCollectors' => 'True' ,
	                            //'Notifications' => 'True',
	                            //'message' => 'Bank successfully created',
	                            'not_type' => 'success'

                              ];


                            return view('accounts.admin', $data);


		}




	public function CreateCollectorsAccount(request $request)
		{


         $request->validate([
          
        'NationalIDScan'    =>  'required|image|mimes:jpeg,png,jpg,gif|max:6048',
        'ProfileImageScan'   =>  'required|image|mimes:jpeg,png,jpg,gif|max:6048',
        'email' => "required|unique:users,email",
        'CollectorAddress' => 'required',
        'phone' => 'required',
        'name' => 'required',
        'password' => 'confirmed',

        ]);


			$hasher    =  $request->input('email');
			$hasher2   =  Carbon::now();
			$hasher3   = $hasher2->toDateTimeString();
			$finalHash = $hasher3.'2333ddf22'.$hasher;


        /****method created in upload trait******/

        $data = new User;

        $data->ProfileImageScan = $request->file('ProfileImageScan');
        $data->NationalIDScan = $request->file('NationalIDScan');
        if($data->ProfileImageScan && $data->NationalIDScan){
           try {
            $filePathNationalIDScan = $this->UserImageUpload($data->NationalIDScan); //Passing $data->NationalIDScan as parameter to  created method
            $filePathProfileImageScan = $this->UserImageUpload($data->ProfileImageScan); //Passing $data->ProfileImageScan as parameter to  created method
            $data->NationalIDScan = $filePathNationalIDScan;
            $data->ProfileImageScan = $filePathProfileImageScan;
            $data->email = $request->input('email');
            $data->name = $request->input('name');
            $data->password = $request->input('password');
            $data->CollectorsPhone = $request->input('phone');
            $data->CollectorAddress = $request->input('CollectorAddress');
            $data->User_id = Hash::make($finalHash);
            $data->CollectorsID = Hash::make('3535342'.$finalHash);
            $data->Account_type = 'Collector';
            $data->User_role = 'Collector';

            $data->save();
           return redirect()->route('ManageCollectors')->with('status', 'Collectors Account Created Successfully');


       } catch (Exception $e) {
          
       }
     }
  }


  public function UpdateCollectorsAccount(request $request)
  {

     $request->validate([
          
       
        'email' => "required",
        'CollectorAddress' => 'required',
        'phone' => 'required',
        'name' => 'required',
        'password' => 'confirmed',

        ]);

        $data = User::find($request->input('id'));

            if ( $request->file('ProfileImageScan') != null) {
                
                 $ProfileImageScan = public_path().$data->ProfileImageScan;

                  Storage::delete($ProfileImageScan);

                 $data->ProfileImageScan = $request->file('ProfileImageScan');

                  $filePathProfileImageScan = $this->UserImageUpload($data->ProfileImageScan); //Passing $data->ProfileImageScan as parameter to  created method
                  $data->NationalIDScan = $filePathNationalIDScan;

            }


             if ($request->file('NationalIDScan') != null ) {
                
                 $NationalIDScan =  public_path().$data->NationalIDScan;

                  Storage::delete($NationalIDScan);

                 $data->NationalIDScan = $request->file('NationalIDScan');

                 $filePathNationalIDScan = $this->UserImageUpload($data->NationalIDScan);

                 $data->NationalIDScan = $filePathNationalIDScan;
                
            }



            $data->email = $request->input('email');
            $data->name = $request->input('name');
            $data->password = $request->input('password');
            $data->CollectorsPhone = $request->input('phone');
            $data->CollectorAddress = $request->input('CollectorAddress');


            $data->save();
            return redirect()->route('ManageCollectors')->with('status', 'Collectors Account Updated Successfully');




 }





            public function DeleteCollectorsAccount($id)
            {

                  $data = User::find($id);

                  $ProfileImageScan = public_path().$data->ProfileImageScan;

                  Storage::delete($ProfileImageScan);

                 // $data->ProfileImageScan = $request->file('ProfileImageScan');



                  $NationalIDScan =  public_path().$data->NationalIDScan;

                  Storage::delete($NationalIDScan);

                 // $data->NationalIDScan = $request->file('NationalIDScan');



                 if( $data->delete() ) {


                  return redirect()->route('ManageCollectors')->with('status', 'Collectors Account Deleted Successfully');


                 }

                
            }








            public function AssignCollectingAgents(request $request)
            {
                $request->validate([
                 
                'AgentID' => 'required',
                'CollectingAgentID' => 'required',
                'TelecomNumber' => 'required',
                'BankAccountNumber' => 'required',
                'TriggerAmount' => 'required|integer',
              
                ]);

                $Telecoms = $request->input('Telecom');
                $Banks =   $request->input('Bank');
                $AgentID =  User::find($request->input('AgentID'));

            if ($AgentID->Outlet_ID == null || $AgentID->Outlet_ID =='') {

               return redirect()->route('ManageCollectors')->with('status', 'Error, agent user not assigned an outlet, collector not assigned');

             }else if ($Banks !='' || $Telecoms !='') {


               

                $CollectingAgentID =  User::find($request->input('CollectingAgentID'));

                $Collectors = $CollectingAgentID;

                $Agents = $AgentID;

                $TriggerAmount = $request->input('TriggerAmount');
                $TelecomNumber = $request->input('TelecomNumber');
                $BankAccountNumber = $request->input('BankAccountNumber');
                



                $Agents->CollectorAddress  =   $Collectors->CollectorAddress;
                $Agents->NationalIDScan    =   $Collectors->NationalIDScan;
                $Agents->ProfileImageScan  =   $Collectors->ProfileImageScan;
                $Agents->NationalIDScan    =   $Collectors->NationalIDScan;
                $Agents->CollectorsID      =   $Collectors->CollectorsID;
                $Agents->CollectorsName    =   $Collectors->name;
              //  $Agents->email             =   $Collectors->email;


                    if ($request->input('Bank')!='') {

                        $Banks = Banks::find($request->input('Bank'));

                        $BankName = $Banks->BankName;

                        $Agents->BankName = $BankName; 

                        $Agents->BankAccount = $BankAccountNumber;  
                    
                        
                    }else if ($request->input('Telecom')!='')  {

                        $Telecoms = Telecoms::find($request->input('Telecom'));

                        $TelecomName = $Telecoms->Telecom_Name;

                        $Agents->TelecomName = $TelecomName; 

                        $Agents->TelecomNumber = $TelecomNumber;  


                    }
                    
                    $Assigned_Count =  $Collectors->Assigned_Count;

                    $Collectors->Assigned_Count = $Assigned_Count + 1;


               
                    if ($Agents->save() && $Collectors->save()) {


                         return redirect()->route('ManageCollectors')->with('status', 'Successful, Agent User Successfully Assigned Collector');
                        
                    }
               

               



            }else {

              return redirect()->route('ManageCollectors')->with('status', 'Error, Collector not assigned both Bank and Telecom deposit details can not be empty, try again');

            }



             //   if ($Agents->save()) {return redirect()->route('ManageCollectors')->with('status', 'Collectors Assigned to Agent User Successfully'); }



            }





             public function RemoveCollector($id)


             {



                $ID = User::find($id);










             }
























































































































}/*****class end**********/
