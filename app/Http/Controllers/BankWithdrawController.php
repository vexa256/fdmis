<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat as BankData;
use App\BankCommission as Commission;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\BankCashAtHand as Cashathand;
use App\outlets;
//use App\banks as Banks;
use App\AgentBankAccounts as BankAccounts;
use App\User;
use App\BankTiers as Tiers;
use App\bankfloats_logs as BankLogs;


class BankWithdrawController extends Controller
{
   


			public function BankWithdrawForm()
			{
				


			$Banks = BankAccounts::where("OutletID", "=", Auth::user()->Outlet_ID)->get();



			$data = [

			    		'Title' => 'Bank Withdraw Form',
			    		'page' => 'banks.BankWithdraw',
			    		'Banks' => $Banks,
			    		//'Notifications' => 'True',
			    		//'message' => 'Bank successfully created',
			    		'not_type' => 'success'

    	               ];


    	                return view('accounts.admin', $data);



			}


			public function BankWithdrawLogic(request $request)
	     
	      {

	      	define("AuthorizedUserName", Auth::user()->name);
	      	define("AuthorizedAgencyName", Auth::user()->Agency_name);
	      	define("AuthorizedAgencyID", Auth::user()->Agency_id);
	      	define("AuthorizedAgentID", Auth::user()->Agent_id);
	      	define("AuthorizedAgentName", Auth::user()->name);
	      	define("AuthorizedOutletID", Auth::user()->Outlet_ID);
	      	define("AuthorizedOutletName", Auth::user()->Outlet_Name);
	      	


	      	$request->validate([
					        'ClientBank' => 'required',
					        'TotalWithdraw' => 'required|integer',
					        'BankCarrier' => 'required',
					     
					      
					    ]);



	      	$TotalWithdraw = $request->input("TotalWithdraw");
	      	$RecipientAccount = $request->input('ClientBank');
	      	$BankID = $request->input('BankCarrier');

	      	
	      	

	      	
	      
	      	$Float = 0;
	       	$Cashathand = $TotalWithdraw;
	      	$WithdrawCharge = 0;
	      	$WithdrawCharge = 0;
	      	$WithdrawCommission = 0;
	      	//WithdrawCommission = 0;

	      	
	      	define('TotalWithdrawUser', $request->input("TotalWithdraw"));
	      	
	      	

	      	function generatePin( $Account ) {
						    // Generate set of alpha characters
						    $alpha = array();
						    for ($u = 65; $u <= 90; $u++) {
						        // Uppercase Char
						        array_push($alpha, chr($u));
						    }
						    /***Timothy disable this for now***/


						    /**This whole system is dedicated to jesus christ my lord and is for the purpose of glorigying him and his church on earth. it will stay active and healthy and relevant until christ comes back****/

						    // Just in case you need lower case
						    // for ($l = 97; $l <= 122; $l++) {
						    //    // Lowercase Char
						    //    array_push($alpha, chr($l));
						    // }

						    // Get random alpha character
						    $rand_alpha_key = array_rand($alpha);
						    $rand_alpha = $alpha[$rand_alpha_key];

						    // Add the other missing integers
						    $rand = array($rand_alpha);
						    for ($c = 0; $c < $Account - 1; $c++) {
						        array_push($rand, mt_rand(0, 9));
						        shuffle($rand);

						    }

						    return implode('', $rand);
						}


			  

			   define("TransactionID", generatePin(8));


			

	      	$BankAccount = BankAccounts::find($BankID);

	      	$FloatAccountID = $BankAccount->BankAccount;

	      	$FloatAccount =  BankData::where("BankAccount", "=", $FloatAccountID)->first();

	      		if (is_null($FloatAccount)) {
	      			

	      			 return redirect()->route('BankWithdrawForm')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');
	      		}else {


	        	$FinalFloatAccount = BankData::find($FloatAccount->id);

	        }


	      	/****isolate Bank ID*********/

	      	$id_z = $request->input('BankCarrier');

	      	$data_z = BankAccounts::find($id_z);

	      	$TelLID = $data_z->BankID;

	      	$Approved_Bank = $TelLID;




	      	/****isolate Bank ID*********/




	      	define("SelectedBankID", $Approved_Bank);

			
			$Tiers = Tiers::where("BankID", "=", SelectedBankID);
	      	
			$Checker_init_func = BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->first();



	if (!BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->exists()) {



    return redirect()->route('BankWithdrawForm')->with('status', ' Sorry, You are not authorized to execute this operation, No Float Account Detected, Please create one by requesting for float' );

/*****repeated to prevent Adapter code edits******/	

	} elseif(!BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->exists()) {
			

			 return redirect()->route('BankWithdrawForm')->with('status', ' Sorry, You do not have enough float to execute this operation, Contact your system admin.');


		} else {



			$Tiers_Bank = Tiers::where("BankID", "=", SelectedBankID)

			->where("range_from", "<=", TotalWithdrawUser)
			->where("range_to", ">=", TotalWithdrawUser)
			->first();

		
				if (!is_null($Tiers_Bank)) {


					$DPC =  $Tiers_Bank->WithdrawCommission;

			define('WithdrawCommission',$DPC);
			


				} elseif (is_null($Tiers_Bank)) {
			
				return redirect()->route('BankWithdrawForm')->with('status', ' Sorry, No Tiers assigned.');

			}

			

			if (Commission::where('BankAccount', $BankAccount->BankAccount)

	  		 		->where('OutletID', AuthorizedOutletID)

	  		 		->exists()) {

	     	       $Checker_for_WithdrawCommission = Commission::where('BankAccount', $BankAccount->BankAccount)

	     	   	  ->where('OutletID', AuthorizedOutletID)

	     	   	  ->first();
	  		 		

	  		 		$CurrentWithdrawCommission = $Checker_for_WithdrawCommission->WithdrawCommission;


	  		 		$FinalWithdrawCommission_commit = $CurrentWithdrawCommission + WithdrawCommission;


	  		 		/**Trigger Withdraw Commision Update only ***/



	  		 		$UniqueCommissionID = $Checker_for_WithdrawCommission->id;

	  		 		$UpdateDb = Commission::find($UniqueCommissionID);

	  		 		$UpdateDb->WithdrawCommission = $FinalWithdrawCommission_commit;

	  		 		$UpdateDb->save();




	  		 		/**Trigger Withdraw Commision Update only ***/


	  		 	} else {


	  		 		/**new Withdraw  commission entry***/

	  		 $DeositCommission = new Commission;

	  		 $DeositCommission->BankID = SelectedBankID;
	  		 $DeositCommission->BankName = SelectedBankID;
	  		 $DeositCommission->WithdrawCommission = WithdrawCommission;
	  		 $DeositCommission->AgencyID = $BankAccount->AgencyID;
	  		 $DeositCommission->AgencyName = $BankAccount->AgencyName;
	  		 $DeositCommission->AgentID	 =  AuthorizedAgentID;
	  		 $DeositCommission->AgentName = AuthorizedUserName;
	  		 $DeositCommission->OutletName = $BankAccount->OutletName;
	  		 $DeositCommission->TransactionType = "Bank Withdraw";
	  		 $DeositCommission->OutletID = $BankAccount->OutletID;
	  		 $DeositCommission->AccounID = $BankAccount->OutletID;
	  		 $DeositCommission->BankAccount = $BankAccount->BankAccount ;
	  		 $DeositCommission->save();
	  		


}/****Withdraw commission closure ***/



/****ask about if cash at hand is not enough****/


/****ask about deducting charges from cash at hand***/


	  		 	if (Cashathand::where('BankAccountNumber', $BankAccount->BankAccount)

	  		 		->where('OutletID', AuthorizedOutletID)->exists()) {


	  		 	$IsolateCashAccount = 	Cashathand::where('BankAccountNumber', $BankAccount->BankAccount)

	  		 		->where('OutletID', AuthorizedOutletID)

	  		 		->first();


	  		 		$Cash_uiniqueID = $IsolateCashAccount->id;

	  		 		$CashAccountFinal = Cashathand::find($Cash_uiniqueID);
	  		 		$CurrentCashathand = $CashAccountFinal->amount;
	  		 		$CashAccountFinal->amount = $CurrentCashathand - $Cashathand;
	  		 		$CashAccountFinal->save();


	  		 	} else {

	  		 		$NewcashCashathand = new Cashathand;

	  		 		$NewcashCashathand->BankID = SelectedBankID;
	  		 		$NewcashCashathand->BankName = $BankAccount->BankName;
	  		 		$NewcashCashathand->BankAccountNumber = $BankAccount->BankAccount ;
	  		 		$NewcashCashathand->TransactionPerson = $BankAccount->AuthorizedUserName ;
	  		 		$NewcashCashathand->AgencyID = $BankAccount->AgencyID ;
	  		 		$NewcashCashathand->AgencyName = $BankAccount->AgencyName;
	  		 		$NewcashCashathand->AgentID = $BankAccount->Agent_id ;
	  		 		$NewcashCashathand->AgentName = $BankAccount->AuthorizedUserName;
	  		 		$NewcashCashathand->OutletName = $BankAccount->OutletName ;
	  		 		$NewcashCashathand->OutletID = $BankAccount->OutletID ;
	  		 		$NewcashCashathand->amount = $BankAccount->Cashathand ;
	  		 		$NewcashCashathand->AccounID = $BankAccount->OutletID ;

	  		 		$NewcashCashathand->save();



	  		 	}






/***Float Account***/ 
/****ask about deducting charges from float account***/



if (BankData::where('BankAccount', $BankAccount->BankAccount)->where('OutletID', AuthorizedOutletID)->exists()) {

	     	       $IsolateFloatAccount = BankData::where('BankAccount', $BankAccount->BankAccount)

	     	   	  ->where('OutletID', AuthorizedOutletID)

	     	   	  ->first();
	     	  

	     	       $FloatAccount_isolated = BankData::find($IsolateFloatAccount->id);

	     	       $CurrentFloatAmount =  $FloatAccount_isolated->amount;

	     	       $FloatAccount_isolated->amount = $CurrentFloatAmount + $TotalWithdraw;

	     	       $FloatAccount_isolated->save();

	     	       $Float =   $FloatAccount_isolated->amount;


	  		 		}




	  			/**Create a transaction ***********/

						


	  			$BankTransactions = new BankTransactions;
	  			$BankTransactions->TransID =  TransactionID ;
	  			$BankTransactions->Customer_Name = '' ;

	  			

	  					$BankTransactions->Transaction_Type = "Withdraw" ;
	
	  			
	  			$BankTransactions->Running_Balance = $Float;

	  			$BankTransactions->Withdraw_Commission = WithdrawCommission;


	  			//$BankTransactions->Client_Phone = $RecipientAccount  ;
	  			$BankTransactions->AgencyID =  AuthorizedAgencyID;
	  			$BankTransactions->Reciever_Acc = $RecipientAccount;
	  			$BankTransactions->AgencyName = AuthorizedAgencyName ;
	  			$BankTransactions->AgentID = AuthorizedAgentID ;
	  			$BankTransactions->AgentName = AuthorizedUserName;
	  			$BankTransactions->OutletName = AuthorizedOutletName ;
	  			$BankTransactions->OutletID = AuthorizedOutletID  ;
	  			$BankTransactions->status ="Successfull";
	  			$BankTransactions->amount = $TotalWithdraw;
	  			$BankTransactions->BankID = $BankAccount->BankID;
	  			$BankTransactions->BankName = $BankAccount->BankName;
	  			$BankTransactions->AgentBankAcc = $BankAccount->BankAccount;
	  			$BankTransactions->save();


	  			/**Create a transaction ***********/





             /*****log transaction***************/


                $BankTransactionsLogs = new BankTransactionsLogs;
	  			$BankTransactionsLogs->TransID =  TransactionID ;
	  			$BankTransactionsLogs->Customer_Name = '' ;

	  			

	  					$BankTransactionsLogs->Transaction_Type = "Withdraw" ;
	  		
	  			
	  			$BankTransactionsLogs->Running_Balance = $Float;

	  			$BankTransactionsLogs->Withdraw_Commission = WithdrawCommission;


	  			//$BankTransactionsLogs->Client_Phone = $RecipientAccount  ;
	  			$BankTransactionsLogs->AgencyID =  AuthorizedAgencyID;
	  			$BankTransactionsLogs->Reciever_Acc = $RecipientAccount;
	  			$BankTransactionsLogs->AgencyName = AuthorizedAgencyName ;
	  			$BankTransactionsLogs->AgentID = AuthorizedAgentID ;
	  			$BankTransactionsLogs->AgentName =AuthorizedUserName;
	  			$BankTransactionsLogs->OutletName = AuthorizedOutletName ;
	  			$BankTransactionsLogs->OutletID = AuthorizedOutletID  ;
	  			$BankTransactionsLogs->status ="Successfull";
	  			$BankTransactionsLogs->amount = $TotalWithdraw;
	  			$BankTransactionsLogs->BankID = $BankAccount->BankID;
	  			$BankTransactionsLogs->BankName = $BankAccount->BankName;
	  			$BankTransactionsLogs->AgentBankAcc = $BankAccount->BankAccount;
	  			$BankTransactionsLogs->save();




	  			  return redirect()->route('home')->with('statuss', 'Hello , Withdraw Made successfully');








	      	

}/****if***/

}/****Mother FUNCTION***********/





}
