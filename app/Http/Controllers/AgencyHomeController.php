<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
use App\telecom;
use App\Telecomfloat;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use Illuminate\Support\Facades\Validator;

class AgencyHomeController extends Controller
{





					public function AgencyHome()
						{

					$Username = Auth::user()->User_id;

					$Users  = User::where('User_id', '=', $Username)->first();


					$id  = $Users->id;




				if (Auth::user()->User_role == 'Agency') {


				$TelecomTransactions= TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)->get();

				$BankTransactions= BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

				->get();



				$T1 = TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
					->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Deposit_Commission');


				$T2 = TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

				->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Withdraw_Commission');




				$B1 = BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

				->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Deposit_Commission');


				$B2 = BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

				->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Withdraw_Commission');


				$TelecomSum = $T1+$T2;
				$BankSum = $B1+$B2;


								 $data = [

								                'Title' => 'Transaction Reports of all your Outlets',
								                'page' => 'AgencyReports.AgencyTransaction',
								                'Users' => $Users,
								                'Banks' => $BankTransactions,
								                'start' => "",
								                'end' =>"",
								                'Bstart' => "",
								                'Bend' =>"",
								                'Telecoms' => $TelecomTransactions,
								                'TelecomTotal' => $TelecomSum,
								                'BankTotal' => $BankSum,
								                'Filter' => '',
								                'Modal_true' => '',


			             					  ];





			                    return view('accounts.admin', $data);




								}else {


								 $data = [

								                'Title' => 'Account Dashboard',
								                //'page' => 'True',
								                'Users' => $Users,


			             					  ];




			                    return view('accounts.admin', $data);



								}


						}













	public function AgencyFilterDatesFromTransactions(request $request)
	{


			/**
			 * switch to Agency account
			 */


        $Username = Auth::user()->User_id;
        $Uselees = null;

					$Users  = User::where('User_id', '=', $Username)->first();

/****Tsts */
			$request->validate([
                            'start_date' => 'required|date',
   							'end_date' => 'required|date|after_or_equal:start_date',

                        ]);

  $start = Carbon::parse($request->start_date);
  $end = Carbon::parse($request->end_date);


	$TelecomTransactions= TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
	->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->get();

	$BankTransactions= BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)->get();



	$T1 = TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Deposit_Commission');


	$T2 = TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Withdraw_Commission');





	$B1 = BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Deposit_Commission');


	$B2 = BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Withdraw_Commission');


	$TelecomSum = $T1+$T2;
	$BankSum = $B1+$B2;


	$TelecomSum = $T1+$T2;


					 $data = [

					                'Title' => 'Transaction Reports (Filtered)',
					                'page' => 'AgencyReports.AgencyTransaction',
					                'Users' => $Users,
					                'Banks' => $BankTransactions,
					                'Bstart' => "",
								    'Bend' =>"",
					                'start' => $request->start_date,
					                'end' => $request->end_date,
					                'Telecoms' => $TelecomTransactions,
					                'TelecomTotal' => $TelecomSum,
					                'BankTotal' => $BankSum,
					                'Filter' => 'True',
					                'Modal_true' => '',


             					  ];

                    return view('accounts.admin', $data);





	}





	public function AgencyBankFilterDatesFromTransactions(request $request)
	{

		$Username = Auth::user()->User_id;

					$Users  = User::where('User_id', '=', $Username)->first();


			$request->validate([
                            'start_date' => 'required|date',
   							'end_date' => 'required|date|after_or_equal:start_date',

                        ]);

  $start = Carbon::parse($request->start_date);
  $end = Carbon::parse($request->end_date);


	$TelecomTransactions= TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)->get();

	$BankTransactions= BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->get();



	$T1 = TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
		->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Deposit_Commission');


	$T2 = TelecomTransactions::where("AgencyID", "=", Auth::user()->Agency_id)

	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Withdraw_Commission');




	$B1 = BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Deposit_Commission');


	$B2 = BankTransactions::where("AgencyID", "=", Auth::user()->Agency_id)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("AgencyID", "=", Auth::user()->Agency_id)->sum('Withdraw_Commission');


	$TelecomSum = $T1+$T2;
	$BankSum = $B1+$B2;


					 $data = [

					                'Title' => 'Transaction Reports (Filtered)',
					                'page' => 'AgencyReports.AgencyTransaction',
					                'Users' => $Users,
					                'start' => "",
								    'end' =>"",
					                'Banks' => $BankTransactions,
					                'Bstart' => $request->start_date,
					                'Bend' => $request->end_date,
					                'Telecoms' => $TelecomTransactions,
					                'TelecomTotal' => $TelecomSum,
					                'BankTotal' => $BankSum,
					                'Filter' => 'True',
					                'Modal_true' => 'True',


             					  ];




                    return view('accounts.admin', $data);





	}













} /************Classs***********************/
