<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
//use App\Agency;
use App\AgentUsers;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\outlets as Outlets;
use App\telcom as Telecoms;
//use App\Telecomfloat as floatData;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use App\TelecomTiers as Tiers;
use App\AgentPhoneNumbers as PhoneNumbers;
use App\telecomfloats_logs as TelecomLogs;
use App\Agency;
//use App\AgentUsers;
//use App\AgencyBankFloat as floatData;
use App\banks as Banks;
//use App\outlets;
//use App\User;
use App\AgencyBankAccounts as BankAccounts;
use App\AgencyBankFloatLogs as BankLogs;
use App\bankfloats_logs as AgentBankLogs;
use App\Bankfloat as AgentfloatData;


class AgencyStatsController extends Controller
{
    //




             public function AgencyStats()
             {


               $Outlets = Outlets::where("AgencyID", "=", Auth::user()->Agency_id)->get()->count();
               $AgentUsers = User::where("Agency_id", "=", Auth::user()->Agency_id)
                                ->where("User_role", "=", "Agent_User")
                                  ->count();

                          $data = [

                                         'Title' => 'Transaction Reports (Filtered)',
                                         'page' => 'AgencyReports.Stats',
                                      //   'Users' => $Users,
                                         'Outlets' => $Outlets,
                                         'AgentUsers' => $AgentUsers,


                                       ];

                                   return view('accounts.admin', $data);
             }





}
