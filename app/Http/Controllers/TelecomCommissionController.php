<?php
namespace App\Http\Controllers;

use App\TelecomCommission as Commission;
use App\User;
use Illuminate\Support\Facades\Auth;

class TelecomCommissionController extends Controller
{
  public function TelecomCommission()
  {
    $Commission = Commission::where("OutletID", "=", Auth::user()->Outlet_ID)
      ->get();

    $SumDeposit = Commission::where("OutletID", "=", Auth::user()->Outlet_ID)->get()->sum('DepositCommission');

    $SumWithdraw = Commission::where("OutletID", "=", Auth::user()->Outlet_ID)->get()->sum('WithdrawCommission');

    $Sum = $SumDeposit + $SumWithdraw;

    $data = [

      'Title'    => 'Wallet Cash at Hand Accounts',
      'page'     => 'Commission.ManageTelecomCommission',
      'Telecoms' => $Commission,
      'Total'    => number_format($Sum),
      //'Notifications' => 'True',
      //'message' => 'Telecom successfully created',
      'not_type' => 'success'

    ];

    return view('accounts.admin', $data);

  }
}
