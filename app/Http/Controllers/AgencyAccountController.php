<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\AgencyBankAccounts as BankAcc;
use App\AgencyTelecomNumbers as TelecomNo;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\outlets;
use App\telcom as Telecoms;
use App\Telecomfloat as floatData;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use App\TelecomTiers as Tiers;
use App\AgentPhoneNumbers as PhoneNumbers;
use App\telecomfloats_logs as TelecomLogs;
use App\Bankfloat as BankData;
//use App\BankCommission as Commission;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
//use App\BankCashAtHand as Cashathand;
use App\AgentBankAccounts as BankAccounts;
//use App\User;
//use App\BankTiers as Tiers;
use App\bankfloats_logs as BankLogs;

class AgencyAccountController extends Controller
{



					public function AgencyBankAccountSetUpForm()
					{
						
						$Telecoms = Telecoms::all();
						$Banks = Banks::all();
						$BankAcc = BankAcc::all();
						$TelecomNo = TelecomNo::all();

							
					 $data = [

					                'Title' => 'Agent Setup Bank Accounts',
					                'page' => 'Agency.AgencyBankAccountSetUpForm',
					                'Banks' => $Banks,
					                'Telecoms' => $Telecoms,
					                'Agents' => $BankAcc,
					                
                

             					  ];

                    return view('accounts.admin', $data);





					}




					public function AddBankAccount(request $request)
					{

						$id = $request->input('BankID');
						$AccountNumber = $request->input('AccountNumber');
						
						$BankAcc = new BankAcc;

						$Banks = Banks::find($id);


						$BankAcc->BankName =  $Banks->BankName;
						$BankAcc->AgencyID = Auth::user()->Agency_id;
						$BankAcc->AgencyName = Auth::user()->Agency_name;
						$BankAcc->BankAccount = $AccountNumber;
						$BankAcc->BankID =  $Banks->Bank_ID;


						if ($BankAcc->save()) {

						   return redirect()->route('AgencyBankAccountSetUpForm')->with('status', 'Banks Account Created  Successfully');

						}

					}







					public function AgencyTelecomAccountSetUpForm()
					{
						
						$Telecoms = Telecoms::all();
						$TelecomNo = TelecomNo::all();
					

							
					 $data = [

					                'Title' => 'Agent Setup Telecom Accounts',
					                'page' => 'Agency.AgencyTelecomAccountSetUpForm',
					                'Telecoms' => $Telecoms,
					                'Agents' => $TelecomNo,
					                
                

             					  ];

                    return view('accounts.admin', $data);





					}






					public function AddTelecomAccount(request $request )
					{

						$id = $request->input('TelecomID');
						$PhoneNumbers = $request->input('PhoneNumbers');
						
						$TelecomNo = new TelecomNo;

						$Telecoms = Telecoms::find($id);


						$TelecomNo->TelecomName =  $Telecoms->Telecom_Name;
						$TelecomNo->AgencyID = Auth::user()->Agency_id;
						$TelecomNo->AgencyName = Auth::user()->Agency_name;
						$TelecomNo->PhoneNumber = $PhoneNumbers;
						$TelecomNo->TelecomID =  $Telecoms->Telecom_ID;


						if ($TelecomNo->save()) {

						   return redirect()->route('AgencyTelecomAccountSetUpForm')->with('status', 'Telecoms Account Created  Successfully');

						}

					}



					public function DeleteAgencyTelecomNumber($id)
					{
						

						$TelecomNo = TelecomNo::find($id);

						if ($TelecomNo->delete()) {

						   return redirect()->route('AgencyTelecomAccountSetUpForm')->with('status', 'Telecoms Account Delete  Successfully');

						}



					}




					public function DeleteAgencyBankAcc($id)
					{
						

						$BankAcc = BankAcc::find($id);

						if ($BankAcc->delete()) {

						   return redirect()->route('AgencyTelecomAccountSetUpForm')->with('status', 'Telecoms Account Delete  Successfully');

						}



					}
						


    
}
