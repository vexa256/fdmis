<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\TelecomCashAtHand as Cashathand;
use App\TelecomCommission as Commission;
use App\outlets;
use App\telcom as Telecoms;
use App\Telecomfloat as floatData;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use App\TelecomTiers as Tiers;
use App\AgentPhoneNumbers as PhoneNumbers;
use App\telecomfloats_logs as TelecomLogs;


class AgencyTelecomCashController extends Controller
{
          public function AgencyManageTelecomCash()
        				{

        					$Cashathand = Cashathand::where("AgencyID", "=", Auth::user()->Agency_id)
        					->get();

        					$Sum = Cashathand::where("AgencyID", "=", Auth::user()->Agency_id)
        					->get()->sum('amount');


        						$data = [

        			    		'Title' => 'Telecom Cash at Hand Accounts',
        			    		'page' => 'AgencyReports.TelecomCash',
        			    		'Telecoms' => $Cashathand,
        			    		'Total' => number_format($Sum) ,
        			    		//'Notifications' => 'True',
        			    		//'message' => 'Telecom successfully created',
        			    		'not_type' => 'success'

            	               ];


            	                return view('accounts.admin', $data);




        				}
}
