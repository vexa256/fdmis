<?php
namespace App\Http\Controllers;

use App\AgentPhoneNumbers;
use App\outlets;
use App\telcom as Telecoms;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TelecomController extends Controller
{
    public function telecomsetupForm()
    {

        $data = [

            'Title' => 'Telecom Setup',
            'page'  => 'telecom.TelecomSetup',

        ];

        return view('accounts.admin', $data);
    }

    /**
     * @param request $request
     */
    public function TelecomSetup(request $request)
    {

        $Telecom               = new Telecoms();
        $Telecom->Telecom_Name = $request->input('TelecomName');
        $hasher                = $request->input('BankName');
        $hasher2               = Carbon::now();
        $hasher3               = $hasher2->toDateTimeString();
        $finalHash             = $hasher3 . 'chaAGEsNCYBANK' . $hasher;
        $Telecom->Telecom_ID   = Hash::make($finalHash);

        $Telecom->save();

        return redirect()->route('ListTelecoms')->with('status', 'Telecom Added Successfully');
    }

    public function ListTelecoms()
    {

        $Telecoms = Telecoms::all();

        $data = [

            'Title'    => 'Telecom Management',
            'page'     => 'telecom.AllTelecoms',
            'Telecoms' => $Telecoms,

//'Notifications' => 'True',
            //'message' => 'Bank successfully created',
            'not_type' => 'success',

        ];

        return view('accounts.admin', $data);
    }

    public function DepositsTelecom()
    {
        $Telecoms = AgentPhoneNumbers::where("OutletID", "=", Auth::user()->Outlet_ID)->get();

        $data = [

            'Title'    => 'Wallet Deposit',
            'page'     => 'telecom.TelecomDeposits',
            'Telecoms' => $Telecoms,

//'Notifications' => 'True',
            //'message' => 'Bank successfully created',
            'not_type' => 'success',

        ];

        return view('accounts.admin', $data);
    }

    public function TelecomAddPhoneNumbers()
    {

        $Telecoms = Telecoms::all();

        $AgentPhoneNumbers = AgentPhoneNumbers::all();

        $AgencyID = Auth::user()->Agency_id;

        $AgencyName = Auth::user()->Agency_name;

        $Users = User::where('Account_type', '=', 'Agent_User')->where('Agency_id', '=', $AgencyID)->get();

        $Outlets = outlets::where('AgencyID', '=', $AgencyID)->where('AgencyName', '=', $AgencyName)->get();

        $data = [

            'Title'    => 'Manage Agent User Phone Numbers ',
            'page'     => 'telecom.ManageAgentUserTelecomNumbers',
            'Agents'   => $AgentPhoneNumbers,
            'Outlets'  => $Outlets,
            'Telecoms' => $Telecoms,
        ];

        return view('accounts.admin', $data);
    }

    /**
     * @param $id
     */
    public function DeleteTelecomOutletNumber($id)
    {
        $Telecoms = AgentPhoneNumbers::find($id);

        if ($Telecoms->delete()) {
            return redirect()->back()->with('status', 'Phone number account deleted successfully ');
        }
    }

    /**
     * @param request $request
     */
    public function SubmitTelecomNumberAccount(request $request)
    {

        $request->validate([
            'OutletPhoneNumber' => 'required',
            'telecomID'         => 'required',
            'OutletID'          => 'required',

        ]);

        $number = $request->input('OutletPhoneNumber');

        //$checker = AgentPhoneNumbers::where("PhoneNumber", "=", $id)->get();
        $TelecomID = $request->input('OutletID');

        if (AgentPhoneNumbers::where('PhoneNumber', '=', $number)->exists()) {
            return redirect()->route('TelecomAddPhoneNumbers')->with('status', 'Error Duplicate Merchant phone number, Please try again');
        } else {

            $AgentPhoneNumbers = new AgentPhoneNumbers();
            $TelecomID         = $request->input('telecomID');
            $OutletID          = $request->input('OutletID');
            $Telecoms          = Telecoms::find($TelecomID);
            $outlets           = outlets::find($OutletID);

            $AgentPhoneNumbers->AgencyID   = Auth::user()->Agency_id;
            $AgentPhoneNumbers->AgencyName = Auth::user()->Agency_name;
            $AgentPhoneNumbers->OutletName = $outlets->outlet_name;
            $AgentPhoneNumbers->OutletID   = $outlets->outlet_id;

            $AgentPhoneNumbers->TelecomName = $Telecoms->Telecom_Name;
            $AgentPhoneNumbers->TelecomID   = $Telecoms->Telecom_ID;
            $AgentPhoneNumbers->PhoneNumber = $request->input('OutletPhoneNumber');

            if ($AgentPhoneNumbers->save()) {
                return redirect()->route('TelecomAddPhoneNumbers')->with('status', 'Merchant phone number account attached to outlet succesfully');
            }
        }
    }

/***************************CLOSURE***************************/
}