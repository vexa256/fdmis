<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat as BankData;
use App\BankCommission as Commission;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\BankCashAtHand as Cashathand;
use App\outlets;
//use App\banks as Banks;
use App\AgentBankAccounts as BankAccounts;
use App\User;
use App\BankTiers as Tiers;
use App\bankfloats_logs as BankLogs;


class AgencyBankCommissionController extends Controller
{


              public function AgencyBankCommission()
              {
                $Commission = Commission::where("AgencyID", "=", Auth::user()->Agency_id)
                ->get();

                $SumDeposit = Commission::where("AgencyID", "=", Auth::user()->Agency_id)->get()->sum('DepositCommission');

                $SumWithdraw = Commission::where("AgencyID", "=", Auth::user()->Agency_id)->get()->sum('WithdrawCommission');


                $Sum = $SumDeposit + $SumWithdraw;


                  $data = [

                    'Title' => 'Bank Commission Accounts',
                    'page' => 'AgencyReports.BankCommissionReports',
                    'Banks' => $Commission,
                    'Total' => number_format($Sum) ,
                    //'Notifications' => 'True',
                    //'message' => 'Bank successfully created',
                    'not_type' => 'success'

                           ];


                            return view('accounts.admin', $data);

              }



  }
