<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
use App\telecom;
use App\Telecomfloat;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    	//$Title = preg_replace('/\s+/', '_', $journalName);


	public function FilterDatesFromTransactions(request $request)
	{


			/**
			 * switch to Agency account
			 */


        $Username = Auth::user()->User_id;
        $Uselees = null;

					$Users  = User::where('User_id', '=', $Username)->first();

/****Tsts */
			$request->validate([
                            'start_date' => 'required|date',
   							'end_date' => 'required|date|after_or_equal:start_date',

                        ]);

  $start = Carbon::parse($request->start_date);
  $end = Carbon::parse($request->end_date);


	$TelecomTransactions= TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
	->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->get();

	$BankTransactions= BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)->get();



	$T1 = TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Deposit_Commission');
		$data_sshhsh = 3463636;

	$T2 = TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Withdraw_Commission');





	$B1 = BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Deposit_Commission');


	$B2 = BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Withdraw_Commission');


	$TelecomSum = $T1+$T2;
	$BankSum = $B1+$B2;


	$TelecomSum = $T1+$T2;


					 $data = [

					                'Title' => 'Transaction Reports (Filtered)',
					                'page' => 'Transactions.ManageTelecomTransactions',
					                'Users' => $Users,
					                'Banks' => $BankTransactions,
					                'start' => $request->start_date,
					                'end' => $request->end_date,
					                'Bstart' => "",
					                'Bend' => "",
					                'Telecoms' => $TelecomTransactions,
					                'TelecomTotal' => $TelecomSum,
					                'BankTotal' => $BankSum,
					                'Filter' => 'True',
					                'Modal_true' => '',


             					  ];

                    return view('accounts.admin', $data);





	}





	public function BankFilterDatesFromTransactions(request $request)
	{

		$Username = Auth::user()->User_id;

					$Users  = User::where('User_id', '=', $Username)->first();


			$request->validate([
                            'start_date' => 'required|date',
   							'end_date' => 'required|date|after_or_equal:start_date',

                        ]);

  $start = Carbon::parse($request->start_date);
  $end = Carbon::parse($request->end_date);


	$TelecomTransactions= TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)->get();

	$BankTransactions= BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->get();



	$T1 = TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Deposit_Commission');


	$T2 = TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Withdraw_Commission');




	$B1 = BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Deposit_Commission');


	$B2 = BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->whereDate('created_at','<=',$end->format('y-m-d'))->whereDate('created_at','>=',$start->format('y-m-d'))
	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Withdraw_Commission');


	$TelecomSum = $T1+$T2;
	$BankSum = $B1+$B2;


					 $data = [

					                'Title' => 'Transaction Reports (Filtered)',
					                'page' => 'Transactions.ManageTelecomTransactions',
					                'Users' => $Users,
					                'Banks' => $BankTransactions,
					                'start' => "",
					                'end' => "",
					                'Bstart' => $request->start_date,
					                'Bend' => $request->end_date,
					                'Telecoms' => $TelecomTransactions,
					                'TelecomTotal' => $TelecomSum,
					                'BankTotal' => $BankSum,
					                'Filter' => 'True',
					                'Modal_true' => 'True',


             					  ];




                    return view('accounts.admin', $data);





	}

















			public function bootstrap()
			{


					$Username = Auth::user()->User_id;

					$Users  = User::where('User_id', '=', $Username)->first();


					$id  = $Users->id;


					if (is_null($Username) && Auth::user()->User_role == "Agency" || Auth::user()->User_role == "Master")

					{


						$Update = User::find($id);

						$Update->User_id = Hash::make($Users->password);
						$Update->Agency_id = Hash::make($Users->password);
						$Update->Agency_name = Auth::user()->name;

						$Update->save();


					}



							 if (Auth::user()->User_role == "Agency") {


								 	 return redirect()->route('AgencyHome');


							 	// code...
							 }





							 if (Auth::user()->User_role == "Master") {

							 	 return redirect()->route('AdminVirtualCloudOffice');


							 }




	if (Auth::user()->User_role == 'Agent_User') {


	$TelecomTransactions= TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)->get();

	$BankTransactions= BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->get();



	$T1 = TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)
		->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Deposit_Commission');


	$T2 = TelecomTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Withdraw_Commission');




	$B1 = BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Deposit_Commission');


	$B2 = BankTransactions::where("OutletID", "=", Auth::user()->Outlet_ID)

	->where("OutletID", "=", Auth::user()->Outlet_ID)->sum('Withdraw_Commission');


	$TelecomSum = $T1+$T2;
	$BankSum = $B1+$B2;


					 $data = [

					                'Title' => 'Transaction Reports (Filtered)',
					                'page' => 'Transactions.ManageTelecomTransactions',
					                'Users' => $Users,
					                'Banks' => $BankTransactions,
					                'start' => "",
					                'end' =>"",
					                'Bstart' => "",
					                'Bend' => "",
					                'Telecoms' => $TelecomTransactions,
					                'TelecomTotal' => $TelecomSum,
					                'BankTotal' => $BankSum,
					                'Filter' => '',
					                'Modal_true' => '',


             					  ];





                    return view('accounts.admin', $data);




					}else {


					 $data = [

					                'Title' => 'Account Dashboard',
					                //'page' => 'True',
					                'Users' => $Users,


             					  ];




                    return view('accounts.admin', $data);



					}


			}




			public function CreateAgentUserAccount()
			{
					 $data = [

					                'Title' => 'Create Agent User Account',
					                'page' => 'AgentUsers.CreateAgentUserAccount',
					                //'Users' => $Users,


             					  ];




                    return view('accounts.admin', $data);





			}



			public function AddAgentUserAccount(request $request)
			{




					$Users = new User;
					$Users->agent_phone = $request->input('phone');
					$Users->name = $request->input('name');
					$Users->password = Hash::make($request->input('password'));
					$Users->email = $request->input('email');
					$Users->Account_type = 'Agent_User';
					$Users->User_id = Hash::make($request->input('email'));
					$Users->Agency_id = Auth::user()->Agency_id;
					$Users->Agency_name = Auth::user()->name;
					$Users->Agent_name = $request->input('name');
					$Users->User_role = 'Agent_User';
					$Users->agency_phone = Auth::user()->agency_phone;
					$Users->Username = $request->input('email');

				if ($Users->save())

						{


				 return redirect()->route('ManageAgentUsers')->with('status', 'Agent Account Created Successfully');


						}



			}




			public function ManageAgentUsers()
			{

				$AgencyID = Auth::user()->Agency_id;

				$AgencyName = Auth::user()->Agency_name;


				$Users = User::where('Account_type', '=', 'Agent_User')->where('Agency_id', '=', $AgencyID)->get();

				$Outlets = outlets::where('AgencyID', '=', $AgencyID)->where('AgencyName', '=', $AgencyName)->get();

				          $data = [

					                'Title' => 'Manage Agent User Accounts',
					                'page' => 'AgentUsers.ManageAgentUsers',
					                'Agents' => $Users,
					                'Outlets' => $Outlets,
             					  ];




                    return view('accounts.admin', $data);




			}






			public function AssignAgentToOutlet(request $request)

			{


					$idz =  $request->input('OutletsID');

					$outlets = outlets::find($idz);


					$OutletName = $outlets->outlet_name;
					$OutletID = $outlets->outlet_id;


					$UserID = $request->input('ID');

					$User = User::find($UserID);


					$User->Outlet_Name = $OutletName ;
					$User->Outlet_ID = $OutletID;





					if ($User->save())

						{


				 return redirect()->route('ManageAgentUsers')->with('status', 'Agent Assigned Outlet Successfully');


						}





			}


}
