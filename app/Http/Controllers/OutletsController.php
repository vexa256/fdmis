<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\AgentUsers;
use App\Bankfloat;
use App\banks as Banks;
use App\BankTransactions;
use App\BankTransactionsLogs;
use App\Cashathand;
use App\outlets;
use App\telecom;
use App\Telecomfloat;
use App\TelecomTransactions;
use App\TelecomTransactionsLogs;
use App\User;
class OutletsController extends Controller
{
    



		public function CreateOutletForm()
		{
			


			         $data = [

					                'Title' => 'Create a new Outlet',
					                'page' => 'outlets.CreateOutlet',
					               
                

             					  ];


					

                    return view('accounts.admin', $data);




		}



			public function CreateOutletLogic(request $request)
			{
				
				$AgencyID = Auth::user()->Agency_id;
				$AgencyName = Auth::user()->name;
				$outlet_name =  $request->input('outlet_name');
				$Agency_location =  $request->input('Agency_location');


				$outlet = new outlets;

				$outlet->AgencyID = $AgencyID;
				$outlet->AgencyName = $AgencyName;
				$outlet->outlet_name = $outlet_name;
				$outlet->Agency_location = $Agency_location;
				$hasher =  $request->input('outlet_name');
			    $hasher2 =  Carbon::now();
			    $hasher3 = $hasher2->toDateTimeString();
			    $finalHash= $hasher3.'ch33ak'.$hasher;
			    $outlet->outlet_id = Hash::make($finalHash);

						if ($outlet->save()) 

						{
										

				 return redirect()->route('AllOutlets')->with('status', 'Outlet Added Successfully');
						


						}				





			}




			public function AllOutlets()
			{
				

				$outlets = outlets::all();





			                   $data = [

					                'Title' => 'List Outlets',
					                'page' => 'outlets.ListOutlets',
					                'Outlets' => $outlets,
					               
                

             					  ];


					

                    return view('accounts.admin', $data);



			}





		public function deleteOutlet($id)
		{
			

			$outlets = outlets::find($id);



			if ($outlets->delete()) 

						{
										

				 return redirect()->route('AllOutlets')->with('status', 'Outlet Deleted Successfully');
						


						}		


		}




		



}
