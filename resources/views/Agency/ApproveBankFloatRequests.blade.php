<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong> Approve bank float requests from child  agent-user accounts 

      </strong>
    </h4>

    <div class="row">
      <div class="col-lg-12">
        <a href="#FloatREquestHistory" class="mt-3 mb-5 btn jesus btn-dark" data-toggle="modal">View  Float Request Log</a>
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
               
               
                <th>Bank Name</th>
                <th class="bg-dark jesus text-light">Bank Acc</th>
                <th>Requested Amount (UGX)</th>
                <th class="bg-warning text-light">Requested by</th>
                <th class="bg-warning text-light">Available Amount</th>
                <th class="bg-danger jesus text-light"> Status</th>
                <th>Date</th>
                <th class="">Delete</th>
                <th class="">Approve Request</th>
                



             
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Floats))

              @foreach($Floats as $Float)
              <tr>
                <td>{{$Float->BankName}}</td>
                <td  class="bg-dark jesus text-light">{{$Float->BankAccount}}</td>
                <td>{{$Float->request_amount}} @if($Float->status == 'Approved') <span class="badge-danger badge">APPROVED </span> @endif </td>
                <td class="bg-primary jesus text-light">{{$Float->AgentName}}</td>
                <td class="bg-dark jesus text-light">{{$Float->amount}}</td>
                <td   class="bg-danger jesus text-light">{{$Float->status}}</td>
                <td>{{$Float->created_at->format('d-M-Y')}}</td>
                <td class=""><a href="{{route('DeleteBankFloatRequestHistory', ['id' => $Float->id])}}" class="btn btn-sm btn-danger jesus ">Delete</a></td> 
                <td class=""><a href="#modal23344444" data-toggle="modal" data-id="{{$Float->id}}" class="btn btn-sm setFloatAccount btn-dark jesus ">Approve</a></td> 
             
               
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
               <tr>
               
               
                <th>Bank Name</th>
                <th class="bg-dark jesus text-light">Bank Account</th>
                <th>Requested Amount (UGX)</th>
                <th class="bg-warning text-light">Requested by</th>
                 <th class="bg-warning text-light">Available Amount</th>
              
                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>
                <th class="">Delete</th>
                  <th class="">Approve Request</th>
                
               
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>




 <div class="modal fade" id="FloatREquestHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">Child  Agent-User Bank Float Request History</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                 

 <table class="table ters table-hover nowrap">
            <thead>
              <tr>
               
               
                <th>Bank Name</th>
                <th class="bg-dark jesus text-light">Bank Account</th>
                <th>Requested Amount (UGX)</th>
                <th class="bg-warning text-light">Requested by</th>
              
                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>
             
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Float_logs))

              @foreach($Float_logs as $Float_log)
              <tr>
                <td>{{$Float_log->BankName}}</td>
                <td  class="bg-dark jesus text-light">{{$Float_log->BankAccount}}</td>
                <td>{{$Float_log->request_amount}}</td>
                <td class="bg-primary jesus text-light">{{$Float_log->AgentName}}</td>
               
                <td   class="bg-danger jesus text-light">{{$Float_log->status}} </td>
                <td>{{$Float_log->created_at->format('d-M-Y')}}</td>
              

                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
               <tr>
               
               
                <th>Bank Name</th>
                <th class="bg-dark jesus text-light">Bank Account</th>
                <th>Requested Amount (UGX)</th>
                <th class="bg-warning text-light">Requested by</th>
              
                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>
               
               
               
              </tr>
            </tfoot>
          </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
            </div>
          </div>




<div class="modal fade" id="modal23344444" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xs" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="mb-4">
           <strong>Approve Agent-User Bank Requests </strong>
         </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="{{route('Agency_Approve_Bank_Float_request')}}" method="POST">
                    <div class="card card-body bg-primary jesus text-light">
                      <h5 class="text-light">Which Bank  account should we deduct float from to approve request?</h5>
                    </div>
                    <div class="form-group row">

                      <div class="col-md">
                         <input type="hidden" name="AgentFloatRequestID" value="" class="AgentFloatRequestID">
                        
                        <label></label>
                       <select name="AgencyFloatAccID" class="form-control" value="">

                       
                      
                      <option value="">Choose Bank Float Account</option>

                         

                        @if(!is_null($Banks))

                       @foreach($Banks as $Bank)

                       <option value="{{ $Bank->id}}"> {{$Bank->BankName}} ({{$Bank->BankAccount}})</option>


                        @endforeach
                        @endif


                       </select>
                      </div>
                      
                    </div>


     @csrf
              


                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-dark btn-light">Approve</button>
    </form>
                </div>
              </div>
            </div>
          </div>
