<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong> Hello {{Auth::user()->Agency_name}}, Approve  Wallet float requests from child agent-user accounts, Currency :: UGX

      </strong>
    </h4>

    <div class="row">
      <div class="col-lg-12">
        <a href="#FloatREquestHistory" class="mt-3 mb-5 btn jesus btn-dark" data-toggle="modal">View  Float Request Log</a>
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>


                <th>Wallet</th>
                <th>Outlet </th>
                <th class="bg-dark jesus text-light">Phone Number</th>
                <th>Requested Amount</th>
                <th class="bg-warning text-light">Agent-User</th>
                <th class="bg-warning text-light">Running Balance</th>
                <th class="bg-danger jesus text-light"> Status</th>
                <th>Date </th>
                <th class="">Delete</th>
                <th class="">Approve</th>






              </tr>
            </thead>
            <tbody>


            @if(!is_null($Floats))

              @foreach($Floats as $Float)
              <tr>
                <td>{{$Float->TelecomName}}</td>
                <td>{{$Float->OutletName}}</td>
                <td  class="bg-dark jesus text-light">{{$Float->PhoneNumber}}</td>
                <td>{{$Float->request_amount}} @if($Float->status == 'Approved') <span class="badge-danger badge">CLEARED</span> @endif </td>
                <td class="bg-primary jesus text-light">{{$Float->AgentName}}</td>
                <td class="bg-dark jesus text-light">{{$Float->amount}}</td>
                <td   class="bg-danger jesus text-light">{{$Float->status}}</td>
                <td>{{$Float->created_at->format('d-M-Y')}}</td>
                <td class=""><a href="{{route('DeleteFloatRequestHistory', ['id' => $Float->id])}}" class="btn btn-sm btn-danger jesus ">Delete</a></td>
                <td class=""><a href="#modal23344444" data-id="{{$Float->id}}" data-toggle="modal" class="btn btn-sm btn-dark jesus setFloatAccount">Approve</a></td>


              </tr>


              @endforeach
              @endif

            </tbody>
               <tr>


                                 <th>Wallet</th>
                                 <th>Outlet </th>
                                 <th class="bg-dark jesus text-light">Phone Number</th>
                                 <th>Requested Amount</th>
                                 <th class="bg-warning text-light">Agent-User</th>
                                 <th class="bg-warning text-light">Running Balance</th>
                                 <th class="bg-danger jesus text-light"> Status</th>
                                 <th>Date </th>
                                 <th class="">Delete</th>
                                 <th class="">Approve</th>





              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>




 <div class="modal fade" id="FloatREquestHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="mb-4">
           <strong>Wallet float request history from child agent-user accounts </strong>
         </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">


 <table class="table ters table-hover nowrap">
            <thead>
              <tr>


                <th>Wallet Name</th>
                <th>Agent Name</th>
                <th>Outlet Name</th>
                <th class="bg-dark jesus text-light">Phone Number</th>
                <th>Requested Amount (UGX)</th>
                <th class="bg-warning text-light">Requested by</th>

                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>


              </tr>
            </thead>
            <tbody>


               @if(!is_null($Float_logs))

              @foreach($Float_logs as $Float_log)
              <tr>
                <td>{{$Float_log->TelecomName}}</td>
                <td>{{$Float_log->AgentName}}</td>
                <td>{{$Float_log->OutletName}}</td>
                <td  class="bg-dark jesus text-light">{{$Float_log->PhoneNumber}}</td>
                <td>{{$Float_log->request_amount}}</td>
                <td class="bg-primary jesus text-light">{{$Float_log->AgentName}}</td>

                <td   class="bg-danger jesus text-light">{{$Float_log->status}} </td>
                <td>{{$Float_log->created_at->format('d-M-Y')}}</td>



              </tr>


              @endforeach
              @endif

            </tbody>
               <tr>


                <th>Wallet Name</th>
                  <th>Agent Name</th>
                <th>Outlet Name</th>
                <th class="bg-dark jesus text-light">Phone Number</th>
                <th>Requested Amount (UGX)</th>
                <th class="bg-warning text-light">Requested by</th>

                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>


              </tr>
            </tfoot>
          </table>







                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-dark btn-light" data-dismiss="modal">Close</button>

                </div>
              </div>
            </div>
          </div>







<div class="modal fade" id="modal23344444" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xs" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="mb-4">
           <strong>Approve Agent-User Wallet Requests </strong>
         </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="{{route('Agency_Approve_Telecom_Float')}}" method="POST">
                    <div class="card card-body bg-primary jesus text-light">
                      <h5 class="text-light">Which wallet  account should we deduct float from to approve request?</h5>
                    </div>
                    <div class="form-group row">

                      <div class="col-md">
                         <input type="hidden" name="AgentFloatRequestID" value="" class="AgentFloatRequestID">

                        <label></label>
                       <select name="AgencyFloatAccID" class="form-control" value="">



                      <option value="">Choose Wallet Float Account</option>



                        @if(!is_null($Telecoms))

                       @foreach($Telecoms as $Telecom)

                       <option value="{{ $Telecom->id}}"> {{$Telecom->TelecomName}} ({{$Telecom->PhoneNumber}})</option>


                        @endforeach
                        @endif


                       </select>
                      </div>

                    </div>


     @csrf



                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-dark btn-light">Save</button>
    </form>
                </div>
              </div>
            </div>
          </div>
