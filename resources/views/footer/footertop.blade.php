<div class="air__layout__footer">
        <div class="air__footer">
          <div class="air__footer__inner">
            <div class="row">
              <div class="col-md-8">
                <p>
                  <strong>
                  Financial Data Management Information System (FDMIS)
                  </strong>
                </p>
               
                <p>
                  &copy; 2019 HD Resources LTD
                </p>
              </div>
              <div class="col-md-4">
                <div class="air__footer__logo">
                  <img src="{{url('components/core/img/air-logo.png')}}" alt="Air UI" />
                  <div class="air__footer__logo__name">FDMIS</div>
                  <div class="air__footer__logo__descr">{{ Auth::user()->name }}'s Account </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>