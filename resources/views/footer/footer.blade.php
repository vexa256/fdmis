



 

   
  <script src="{{url('vendors/bootstrap-sweetalert/dist/sweetalert.min.js')}}"></script>
 
  
  

<script src="{{url('js/chm.js')}}"></script>


  <script src="{{url('vendors/popper.js/dist/umd/popper.js')}}"></script>
  <script src="{{url('vendors/bootstrap/dist/js/bootstrap.js')}}"></script>
  <script src="{{url('vendors/jquery-mousewheel/jquery.mousewheel.min.js')}}"></script>
 
  <script src="{{url('vendors/peity/jquery.peity.min.js')}}"></script>
  <script type="text/javascript"
    src="{{url('cdn.datatables.net/v/bs4/dt-1.10.18/fc-3.2.5/r-2.2.2/datatables.min.js')}}"></script>
  <script src="{{url('vendors/editable-table/mindmup-editabletable.js')}}"></script>
  <script src="{{url('vendors/moment/min/moment.min.js')}}"></script>
  <script src="{{url('vendors/tempus-dominus-bs4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
  <script src="{{url('vendors/fullcalendar/dist/fullcalendar.min.js')}}"></script>
  
  

  <!----========================other scripts==============================-->


  <script src="{{url('components/core/index.js')}}"></script>
  <script src="{{url('components/menu-left/index.js')}}"></script>
  <script src="{{url('components/menu-top/index.js')}}"></script>
  <script src="{{url('components/sidebar/index.js')}}"></script>
  <script src="{{url('components/topbar/index.js')}}"></script>
  



@include('notifications.notifications')




@isset($ManageCollectors)


  <script type="text/javascript">
    

    $(function() {


        $(".trash").on( "click", function() {

          
    
          var id = $(this).data('id');

          var BASEURL = $('#url').val();


          



      swal({
      title: "Are you sure?",
      text: "You would like to delete this collections account. This action is not reversible",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
     window.location = BASEURL+'/DeleteCollectorsAccount/'+id;
    });

       });
    

  });
    
  </script>
  @endisset



  <script>
@isset($Modal_true)

@if($Modal_true == "True") 

$(document).ready(function() {

        $("#BankTRansactions9292292929").modal('show');
});

@endif

@endisset
  

    $(document).ready(function () {


  @if(Auth::user()->User_role == 'Agency')

  $('.setFloatAccount').on('click', function(event) {

      var id = $(this).data('id');

      $('.AgentFloatRequestID').val(id);
      
      });

  @endif

    
    
     /* $('.air__initialLoading').delay(100).fadeOut(100);
      $('.air__initialLoading').hide();*/


      $('.waiters').hide();

      $(".DateQuery").flatpickr( {altInput: true,   dateFormat: "y-m-d" });



    });
  </script>





</body>


</html>