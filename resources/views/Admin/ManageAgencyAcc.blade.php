<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>{{$Desc}}</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th class="jesus btn-primary">Agent</th>
                <th class="jesus btn-dark">Email</th>
                <th class="jesus btn-dark">Account Type</th>
                <th class="jesus btn-danger">Date Created</th>
                <th class="jesus btn-primary">Delete</th>
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Agents))

              @foreach($Agents as $Agent)
              <tr>
                <td>{{$Agent->name}}</td>
                <td>{{$Agent->email}}</td>
               
                <td class="btn-danger"> @if($Agent->User_role == 'Master') 

                  Admin Account

                 @endif


                 @if($Agent->User_role == 'Agency')  

                  Agent Account

                 @endif

               </td>
               
                <td>{{$Agent->created_at->format('d-M-Y')}}</td>
              
                <td><a href="{{route('DeleteAgency', ['id' => $Agent->id])}}" class="jesus btn btn-sm btn-danger jesus ">Delete</a></td> 
               
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
               <tr>
                <th class="jesus btn-primary">Agent</th>
                <th class="jesus btn-dark">Email</th>
           <th class="jesus btn-dark">Account Type</th>
                <th class="jesus btn-danger">Date Created</th>
                <th class="jesus btn-primary">Delete</th>
               
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

