
<div class="row">
  <div class="col-md-4">
    <div class="card">
      <div class="card-body jesus bg-primary text-light">
        <div class="d-flex flex-wrap  align-items-center">
            <i class="fa fa-home fa-3x float-right p-2"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Outlets
            </p>
            <p class="text-gray-5 mb-0">
              All registered Outlets
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0">
          {{$outlets}}
          </p>
        </div>
      </div>
    </div>
  </div>




  <div class="col-md-4">
    <div class="card">
      <div class="card-body jesus bg-dark text-light">
        <div class="d-flex flex-wrap  align-items-center">
            <i class="fa  fa-user-o fa-3x float-right p-2"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Agent Users
            </p>
            <p class="text-gray-5 mb-0">
              All Agent  User Accounts
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0">
          {{$AgentUsers}}
          </p>
        </div>
      </div>
    </div>
  </div>




   <div class="col-md-4">
    <div class="card">
      <div class="card-body jesus bg-info text-light">
        <div class="d-flex flex-wrap  align-items-center">
            <i class="fa fa-users fa-3x float-right p-2"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Agents
            </p>
            <p class="text-gray-5 mb-0">
              All Agent Accounts
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0">
          {{$Agency}}
          </p>
        </div>
      </div>
    </div>
  </div>




   <div class="col-md-4">
    <div class="card">
      <div class="card-body jesus bg-danger text-light">
        <div class="d-flex flex-wrap  align-items-center">
            <i class="fa fa-superpowers fa-3x float-right p-2"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Admins
            </p>
            <p class="text-gray-5 mb-0">
              All Administrator Accounts
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0">
          {{$Admins}}
          </p>
        </div>
      </div>
    </div>
  </div>




   <div class="col-md-4">
    <div class="card">
      <div class="card-body jesus bg-primary text-light">
        <div class="d-flex flex-wrap  align-items-center">
            <i class="fa fa-phone-square fa-3x float-right p-2"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Telecoms
            </p>
            <p class="text-gray-5 mb-0">
              All Telecom Carriers
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0">
          {{$telecoms}}
          </p>
        </div>
      </div>
    </div>
  </div>



  <div class="col-md-4">
    <div class="card bg-dark text-light">
      <div class="card-body jesus">
        <div class="d-flex flex-wrap align-items-center">
          <i class="fa fa-bank fa-3x float-right p-2"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Banks
            </p>
            <p class="text-gray-5 mb-0">
           All Bank Carriers
            </p>
          </div>
          <p class="text-primary font-weight-bold font-size-24 mb-0">
          {{$banks}}
          </p>
        </div>
      </div>
    </div>
  </div>












</div>