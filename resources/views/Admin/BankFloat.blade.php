<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Manage Agent Bank Float Requests</strong></span>


      </strong>
    </h4>

    <div class="row">
      <div class="col-lg-12">
        <a href="#FloatREquestHistory" class="mt-3 mb-5 btn jesus btn-dark" data-toggle="modal">View  Float Request Log</a>
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>


                <th>Bank Name</th>
                <th>Agent</th>
                <th class="bg-dark jesus text-light">Bank Account</th>
                <th>Requested Amount (UGX)</th>

                <th class="bg-warning text-light">Available Amount</th>

                <th class="bg-danger jesus text-light">Approval Status</th>
                <th>Date of Request</th>
                <th>Approve</th>




              </tr>
            </thead>
            <tbody>


               @if(!is_null($Floats))

              @foreach($Floats as $Float)
              <tr>
                <td>{{$Float->BankName}}</td>
                <td>{{$Float->AgencyName}}</td>
                <td  class="bg-dark jesus text-light">{{$Float->BankAccount}}</td>
                <td>{{$Float->request_amount}} @if($Float->status == 'Approved') <span class="badge-danger badge">CLEARED</span> @endif </td>

                <td class="bg-dark jesus text-light">{{$Float->amount}}</td>
                <td   class="bg-danger jesus text-light">{{$Float->status}}</td>
                <td>{{$Float->created_at->format('d-M-Y')}}</td>
                <td><a href="{{route('AdminApproveAgencyBankFloat', ['id'=>$Float->id])}}" class="btn btn-dark jesus">Approve</a></td>

              </tr>


              @endforeach
              @endif

            </tbody>
               <tr>



                                 <th>Bank Name</th>
                                    <th>Agent</th>
                                 <th class="bg-dark jesus text-light">Bank Account</th>
                                 <th>Requested Amount (UGX)</th>

                                 <th class="bg-warning text-light">Available Amount</th>

                                 <th class="bg-danger jesus text-light">Approval Status</th>
                                 <th>Date of Request</th>
                           <th>Approve</th>

              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>




 <div class="modal fade" id="FloatREquestHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">Bank Float Request History for all Agent Accounts</span></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">


 <table class="table ters table-hover nowrap">
            <thead>
              <tr>


                <th>Bank Name</th>
                <th>Requested By</th>
                <th class="bg-dark jesus text-light">Bank Account</th>
                <th>Requested Amount (UGX)</th>


                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>


              </tr>
            </thead>
            <tbody>


               @if(!is_null($Float_logs))

              @foreach($Float_logs as $Float_log)
              <tr>
                <td>{{$Float_log->BankName}}</td>
                <td>{{$Float_log->AgencyName}}</td>
                <td  class="bg-dark jesus text-light">{{$Float_log->BankAccount}}</td>
                <td>{{$Float_log->request_amount}}</td>


                <td   class="bg-danger jesus text-light">{{$Float_log->status}} </td>
                <td>{{$Float_log->created_at->format('d-M-Y')}}</td>



              </tr>


              @endforeach
              @endif

            </tbody>
               <tr>
                <tfoot>


                <th>Bank Name</th>
                <th>Requested By</th>
                <th class="bg-dark jesus text-light">Bank Account</th>
                <th>Requested Amount (UGX)</th>


                <th class="bg-danger jesus text-light">Approval Satus</th>
                <th>Date of Request</th>



              </tr>
            </tfoot>
          </table>







                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>

                </div>
              </div>
            </div>
          </div>
