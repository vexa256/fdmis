<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong> Cash at hand records. Your Parent Agent is :: <span class="jesus ml-1 badge bg-dark text-white font-size-16  air__topbar__status">{{Auth::user()->Agency_name}}</span>  
         and your outlet is <span class="jesus ml-1 badge bg-primary text-white font-size-16  air__topbar__status">{{Auth::user()->Outlet_Name}}</span>

      </strong>
    </h4>

    <div class="row">
      <div class="col-lg-12">
        <div class="card jesus bg-dark text-light">
      <div class="card-body">
        <p class="text-light font-size-40 font-weight-bold mb-2">
       {{$Total}} (UGX)   <i class=" fa fa-money fa-2x float-right"></i>
        </p>
        <p class="text-uppercase text-light mb-3">
          Total Cash At Hand from all you agent bank accounts
        </p>
        
    </div>
    </div>
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
               
                
                <th>Outlet Name</th>
                <th>Agency Name</th>
                <th>Agent Bank Name</th>
                <th> Agent Bank Account</th>
                <th >Amount</th>
                <th >Mark as Picked</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <tr>
               
                 <td  class="bg-dark jesus text-light">{{$Bank->OutletName}}</td>
                <td  class="bg-dark jesus text-light">{{$Bank->AgencyName}}</td>
                <td  class="bg-dark jesus text-light">{{$Bank->BankName}}</td>
                <td  class="bg-dark jesus text-light">{{$Bank->BankAccountNumber}}</td>
                <td  class="bg-secondary jesus text-light">{{$Bank->amount}} (UGX)</td>
                <td  class="">
                  
                  <a href="#" class="btn btn-sm btn-danger">Mark as Picked</a>
                </td>
               
               
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
               <tr>
               
            
               <th>Outlet Name</th>
                <th>Agency Name</th>
                <th>Source Bank Name</th>
                <th > Source Bank Account</th>
                <th >Amount</th>
                <th >Mark as Picked</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>