@include('head.header')
@include('head.loader')
  <div class="air__layout air__layout--hasSider">
    
    @include('nav.sidenav')
    @include('nav.themesettings')
    @include('chat.chat')

    <div class="air__layout">
      
      @include('head.topheader')

      <div class="air__layout__content">
        <div class="air__utils__content">


            @if(isset($page))

              @include($page)
            
            @endif


  
   </div>
      </div>
      
@include('footer.footertop')

@include('footer.footer')