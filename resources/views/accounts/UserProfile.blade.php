<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Hello, this is your account profile</strong> </h4>

    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th class="btn-dark">Parent Agent</th>
                <th class=" btn-danger jesus text-light">Assigned Outlet</th>
                <th class="btn-dark">Name</th>
                <th class="btn-primary">Username</th>
                <th class="btn-danger">Account Type</th>
                <th class="btn-dark">Date Created</th>
           
               
              </tr>
            </thead>
            <tbody>


                <td>{{Auth::user()->Agency_name}}</td>
                <td>{{Auth::user()->Outlet_Name}}</td>
                <td>{{Auth::user()->name}}</td>
                <td>{{Auth::user()->email}}</td>
                <td>{{Auth::user()->Account_type}}</td>
             <td>{{Auth::user()->created_at->format('d-M-Y')}}</td>
                
               
                
              </tr>


            
            </tbody>
             <tr>
                <th class="btn-dark">Parent Agent</th>
                <th class=" btn-danger jesus text-light">Assigned Outlet</th>
                <th class="btn-dark">Name</th>
                <th class="btn-primary">Username</th>
                <th class="btn-danger">Account Type</th>
                <th class="btn-dark">Date Created</th>
           
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


