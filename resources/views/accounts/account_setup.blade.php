<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>Account Setup</strong>
        </h4>
        <div class="mb-5">
          <div  class="wizard Wizard">
            <h3>
              <i class="fe fe-user wizard-steps-icon"></i>
              <span class="wizard-steps-title">Account Info</span>
            </h3>
            <section class="text-center pb-5 pt-5">
              <h3 class="d-none">Title</h3>
              <p>Try the keyboard navigation by clicking arrow left or right!</p>
            </section>
            <h3>
              <i class="fe fe-book wizard-steps-icon"></i>
              <span class="wizard-steps-title">Billing Info</span>
            </h3>
            <section class="text-center">
              <h3 class="d-none">Title</h3>
              <p>Wonderful transition effects.</p>
            </section>
            <h3>
              <i class="fe fe-check wizard-steps-icon"></i>
              <span class="wizard-steps-title">Confirmation</span>
            </h3>
           <section class="text-center">
              <h3 class="d-none">Title</h3>
              <p>Wonderful transition effects.</p>
            </section>
          </div>
        </div>
      </div>
    
    </div>
  </div>
</div>
