<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>This interface allows you manage all user accounts in the system, Use delete with caution</strong>
    </h4>
    <a href="#sudo9" data-toggle="modal"  class="btn btn-danger jesus mb-5">Create Super User</a>

     <a href="{{route('CreateAgencyAccount')}}"  class="btn btn-dark jesus mb-5 float-right">Create Agent</a>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" >
            <thead>
              <tr>
                <th class="bg-primary jesus text-color">Name</th>
                <th class="bg-danger jesus text-color">User Role</th>
                <th>Username</th>
                
                <th>Delete</th>
               
                
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Users))

              @foreach($Users as $User)
              <tr>
                <td>{{$User->name}}</td>
                <td class="bg-dark jesus text-color">{{$User->User_role}}</td>
                <td>{{$User->email}}</td>
                <td><a href="{{route('DeleteUserAccount', ['id' => $User->id])}}"  class="btn btn-danger jesus ">Delete</a></td>
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
              <th class="bg-primary jesus text-color">Name</th>
                <th class="bg-danger jesus text-color">Account Type</th>
                <th>Username</th>
                <th>Delete</th>
               
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>



          <div class="modal fade" id="sudo9" tabindex="-1" role="dialog"aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header jesus">
                  <h5 class="modal-title" id="exampleModalLabel">Super Users</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

 <div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Add Super Users</strong>
    </h4>
    <form action="{{route('CreateSuperUser')}}" method="POST">
      <div class="form-row">
         @csrf
        <div class="form-group col-md">
          <label for="inputEmail4">Email (Username)</label>
          <input type="email" name="email" class="form-control"  placeholder="Email (Username)" />
        </div>
        <div class="form-group col-md">
          <label for="inputPassword4">Password</label>
          <input type="password" name="password" class="form-control"  placeholder="Password" />
        </div>

         <div class="form-group col-md">
          <label for="inputPassword4">Super User's Name</label>
          <input type="text" name="name" class="form-control"  placeholder="Super User's Name" />
        </div>
       
      </div>
   
  </div>
</div> </div>
                <div class="modal-footer">
                  <button type="button" class="btn jesus btn-light" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn jesu btn-primary">Save changes</button>
                </div>
                 </form>
              </div>
            </div>
          </div>





