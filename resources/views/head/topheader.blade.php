 <div class="air__utils__header shadow-lg">
          <div class="air__topbar">
            <div class="air__topbar__searchDropdown dropdown mr-md-4 mr-auto">
              <div class="air__topbar__search dropdown-toggle" data-toggle="dropdown" data-offset="5,15">
                <div class="air__topbar__searchContainer">
                <h2><span class="badge badge-primary"><i class="fa fa-user"> </i> {{ Auth::user()->name }} </span></h2>
                </div>
              </div>
              <div class="dropdown-menu" role="menu">

              </div>
            </div>
            <div class="dropdown mr-auto d-none d-md-block">
              <a
                href="#"
                class="dropdown-toggle text-nowrap"
                data-toggle="dropdown"
                aria-expanded="false"
                data-offset="0,15"
              style="display: none !important" >
                <i class="dropdown-toggle-icon fe fe-book-open"></i>
                <span class="dropdown-toggle-text">Quick Actions</span>
              </a>
              <div class="dropdown-menu" role="menu">
                <div class="dropdown-header">Active</div>
                <a class="dropdown-item" href="javascript:void(0)">Project Management</a>
                <a class="dropdown-item" href="javascript:void(0)">User Inetrface Development</a>
                <a class="dropdown-item" href="javascript:void(0)">Documentation</a>
                <div class="dropdown-header">Inactive</div>
                <a class="dropdown-item" href="javascript:void(0)">Marketing</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:void(0)">
                  <i class="dropdown-icon fe fe-settings"></i>
                  Settings
                </a>
              </div>
            </div>
            <p class="mb-0 mr-4 d-xl-block d-none ">
           @if(Auth::user()->User_role == "Agent_User")

            <span class="jesus ml-1 badge bg-dark text-white font-size-12 text-uppercase air__topbar__status"
                >   Agent User Account</span
              >

           @endif


            @if(Auth::user()->User_role == "Agency" )

            <span  class="jesus ml-1 badge bg-dark text-white font-size-12 text-uppercase air__topbar__status"
                >   Parent Agent Account</span
              >

           @endif






            @if(Auth::user()->User_role == "Master" )

            <span  class="jesus ml-1 badge bg-dark text-white font-size-12 text-uppercase air__topbar__status"
                >   <i class="fa fa-superpowers  p-2"></i>  Super Admin
                (HD Resources)</span
              >

           @endif
            </p>


            <div class="dropdown">
              <a
                href="/logout"
                class="text-nowrap btn btn-danger btn-sm"

              >
                <i class="fa fa-sign-out fa-2x"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" role="menu">

                <div class="dropdown-divider"></div>


              </div>
            </div>
          </div>
          <div class="air__subbar">
            <ul class="air__subbar__breadcrumbs mr-4">
              <li class="air__subbar__breadcrumb">
                <a href="#" class="air__subbar__breadcrumbLink">{{Auth::user()->User_role}}</a>
              </li>
              <li class="air__subbar__breadcrumb">
                <a href="#" class="air__subbar__breadcrumbLink air__subbar__breadcrumbLink--current"
                  >{{$Title}}</a
                >
              </li>
            </ul>
            <div class="air__subbar__divider mr-4 d-none d-xl-block"></div>

            <a href="{{ route('home') }}" class="jesus btn btn-danger  mr-auto  text-nowrap d-none d-md-block">
              <span class="btn-addon">
                <i class="btn-addon-icon fa fa-arrow-left"></i>
              </span>
              Home
            </a>


 @if(Auth::user()->User_role == "Agency")

        <a href="{{route('AgencyStats')}}" class="jesus btn btn-dark  btm-sm mr-auto  text-nowrap d-none d-md-block">

           <span class="btn-addon">
                <i class="btn-addon-icon fa fa-pie-chart"></i>
              </span>

              My Statistics
            </a>

               @endif


      @if(Auth::user()->User_role == "Agent_User")

        <a href="{{route('DepositsBank')}}" class="jesus btn btn-dark  btm-sm mr-auto  text-nowrap d-none d-md-block">

              Bank Deposit
            </a>



             <a href="{{route('DepositsTelecom')}}" class="jesus btn btn-dark  btm-sm mr-auto  text-nowrap d-none d-md-block">

              Wallet Deposit
            </a>




      <a href="{{route('BankWithdrawForm')}}" class="jesus btn btn-primary  mr-auto  text-nowrap d-none d-md-block">

              Bank Withdraw
            </a>




      <a href="{{route('TelecomWithdrawForm')}}" class="jesus btn btn-primary  mr-auto  text-nowrap d-none d-md-block">

              Wallet Withdraw
            </a>


      @endif

            <div class="air__subbar__amount mr-3 ml-auto d-none d-sm-flex" style="display: none !important;">
              <p class="air__subbar__amountText">
                This month
                <span class="air__subbar__amountValue">$251.12</span>
              </p>
              <div class="air__subbar__amountGraph">
                <i class="air__subbar__amountGraphItem" style="height: 80%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 50%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 70%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 60%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 50%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 65%"></i>
              </div>
            </div>
            <div class="air__subbar__amount d-none d-sm-flex" style="display: none !important;">
              <p class="air__subbar__amountText">
                Last month
                <span class="air__subbar__amountValue">$12,256.12</span>
              </p>
              <div class="air__subbar__amountGraph">
                <i class="air__subbar__amountGraphItem" style="height: 60%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 65%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 75%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 55%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 100%"></i>
                <i class="air__subbar__amountGraphItem" style="height: 85%"></i>
              </div>
            </div>
          </div>
        </div>
