<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>

  {{ config('app.name', 'FDMIS © 2019 HD Resources LTD ') }}  {{ Auth::user()->name }}
 
  </title>
  <link href="{{url('components/core/img/favicon.png')}}" rel="shortcut icon">
  <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i,900" rel="stylesheet">-->

  <!-- VENDORS -->
  <!-- v1.0.0 -->
  <link rel="stylesheet" type="text/css"   href="{{url('css/app.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('vendors/font-feathericons/dist/feather.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('fontawesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('vendors/font-linearicons/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('vendors/font-icomoon/style.css')}}">
  
  <link rel="stylesheet" type="text/css"
    href="{{url('cdn.datatables.net/v/bs4/dt-1.10.18/fc-3.2.5/r-2.2.2/datatables.min.css')}}">
  <link rel="stylesheet" type="text/css"
    href="{{url('vendors/tempus-dominus-bs4/build/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('vendors/bootstrap-sweetalert/dist/sweetalert.css')}}">
 
  <!-- AIR UI HTML ADMIN TEMPLATE MODULES-->
  <!-- v1.0.0 -->
  <link rel="stylesheet" type="text/css" href="{{url('components/vendors/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/core/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/widgets/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/system/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/menu-left/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/menu-top/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/footer/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/topbar/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/subbar/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/sidebar/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/chat/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/apps/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/apps/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/extra-apps/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('components/ecommerce/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('css/chm.css')}}">

  

  
    <script src="{{url('vendors/jquery/dist/jquery.min.js')}}"></script>



<style type="text/css">


      @if(Auth::user()->User_role == 'Agent_User')

           
            .agent_user {

              display: none !important;
            }
  
            
       @endif



  
        .jesus{

             webkit-box-shadow: -3px 13px 24px -1px rgba(0,0,0,0.3); box-shadow: -3px 13px 24px -1px rgba(0,0,0,0.3) !important;


        }




        .text-color{

          color: white !important;


        }

        .table {

          font-size: 12px !important;


        }



        .Agent_User_C {

          padding: 0.1rem !important;
        }


        .table th, .table td {
         padding: 0.45rem !important;
         vertical-align: top;


       }


       .btn {

          
        
             webkit-box-shadow: -3px 13px 24px -1px rgba(0,0,0,0.3); box-shadow: -3px 13px 24px -1px rgba(0,0,0,0.3) !important;



       }


       .air__initialLoading {


            display: none !important;


       }




      @if( $checker = Auth::user()->User_role != "Super_User") 


       .Super_User {


         display: none !important;


       }

        @endif  


        


      @if( $checker = Auth::user()->User_role != "Agency_Account") 


       .Agency_Account {


         display: none !important;


       }

        @endif       










           @isset($CreateAgencyAccount)
            

            .actions {


              display: none !important;



             }

                       

          @endisset

       

</style>
</head>
<body class="air__menu--flyout air__menu--dark air__menu__submenu--blue .air__layout--fixedHeader air__layout--fixedHeader">