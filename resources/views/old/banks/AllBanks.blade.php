<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Manage all Banks in the system, Use delete with caution</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Date Created</th>
                <th>Delete</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <tr>
                <td>{{$Bank->BankName}}</td>
                <td>{{$Bank->BranchName}}</td>
                <td>{{ $Bank->created_at->format('d-M-Y') }}</td>
                <td><a href="{{route('UpdateBankForm', ['id' => $Bank->id])}}" class="btn btn-danger jesus ">Update</a></td> 
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Date Created</th>
                <th>Delete</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
