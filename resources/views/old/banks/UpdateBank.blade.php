<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>{{$Title}} , The bank selected for update is {{$Bank->BankName}}</strong>
        </h4>
        <div class="mb-5">
          <div  class="wizard Wizard">
            <h3>
              <i class="fe fe-user wizard-steps-icon"></i>
              <span class="wizard-steps-title">Update Bank Information,  </span>
            </h3>
            <section class="text-center">
              <h3 class="d-none"></h3>
            

                    <div class="card jesus">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Please fill in all the fields</strong>
    </h4>
    <form method="POST" action="{{route('UpdateBank')}}" class="form" class="">
      <div class="form-row">
         @csrf
        <div class="form-group col-md">
          <label for="aaaaa">Bank Name</label>
          <input type="text"  value="{{$Bank->BankName}}"  required  class="form-control"  name="BankName" />

          <input type="hidden" name="id" value="{{$Bank->id}}">
        </div>
         <div class="form-group col-md">
          <label for="ss">Branch Name</label>
          <input type="text"  value="{{$Bank->BranchName}}"   required  class="form-control" name="BranchName" />
        </div>
      </div>
      <div class="form-group">
         
          <input type="submit" class="btn btn-dark jesus" value="Update Bank " />
        </div>
    </form>
  </div>
</div> 
</section>
            
          </div>
        </div>
      </div>
    
    </div>
  </div>
</div>
