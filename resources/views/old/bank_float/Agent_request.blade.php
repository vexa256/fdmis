
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>Bank Agent Float Request</strong>

        </h4>
        <div class="mb-5">
          <div id="example-numbers" class="wizard Wizard wizard-numbers">
            <h3>
              <span class="wizard-steps-title">Request Information</span>
            </h3>
      <section class="text-center">
              <h3 class="d-none">Title</h3>
        


              <form method="POST" action="{{route('BankFloatRequest_submit')}}" class="form" class="">
      <div class="form-row">
         @csrf
        <div class="form-group col-md">
          <label for="aaaaa">Float Request Amount</label>
          <input type="text"    required  class="form-control"  name="Float_amount" />
        </div>

        <div class="form-group col-md">
          <label for="ss">Bank Name</label>
        <select class="form-control " name="Bank_id">

        	<option value="">Choose Bank</option>
         		 @if(!is_null($Banks))

              @foreach($Banks as $Bank)
         		
         		<option value="{{$Bank->id}}">{{$Bank->BankName}}</option>
         		


         		 @endforeach 
              @endif       


         </select>

         <input type="hidden" name="Agency_ID" value="{{ Auth::user()->Agency_id }}">
         <input type="hidden" name="Agency_Name" value="{{ Auth::user()->Agency_name }}">
         <input type="hidden" name="Request_Agent_ID" value="{{ Auth::user()->Agent_id }}">
         <input type="hidden" name="Request_Agent_Name" value="{{ Auth::user()->name }}">
        </div>





       
      </div>
      <div class="form-group">
         
          <input type="submit" class="btn btn-dark jesus" value="Send Float Request " />
        </div> </section>
            
          </div>
           </form>
        </div>
      </div>
    </div>
  </div>
</div>













