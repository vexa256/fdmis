<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>View all Bank float requests, </strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Bank</th>
                <th>Branch </th>
                <th>Agency </th>
                <th>Agent Name</th>
                <th>Requested Float (UGX)</th>
                <th>Date Created</th>
                <th class="btn-danger jesus">Request Status</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($BankFloats))

              @foreach($BankFloats as $BankFloat)
              <tr>
                <td>{{$BankFloat->BankName}}</td>
                <td>{{$BankFloat->BranchName}}</td>
                <td>{{$BankFloat->Agency_Name}}</td>
                <td>{{$BankFloat->Request_Agent_Name}}</td>
                <td>{{$BankFloat->Requested_float}}</td>
                <td>{{$BankFloat->created_at->format('d-M-Y') }}</td>
                <td class="btn-dark jesus">{{$BankFloat->Request_status}}</td>
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
               <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Agency Name</th>
                <th>Agent Name</th>
                <th>Requested Float (UGX)</th>
                   <th>Date Created</th>
                <th class="btn-danger jesus">Request Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
