<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Hello , {{Auth::user()->name }}, View all Bank Float Account(s)</strong>

        
    </h4>


 

   


    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" >
            <thead>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Date Created</th>
                <th>Users_Name</th>
                <th class=" btn-primary text-color">Float Account</th>
                <th class=" btn-danger text-color">Amount Available (UGX)</th>
             
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <tr>
                <td>{{$Bank->BankName}}</td>
                <td>{{$Bank->BranchName}}</td>
                <td>{{$Bank->created_at->format('d-M-Y') }}</td>
                <td  class=" btn-success jesus text-color">{{$Bank->Users_Name}}</td>
                <td  class=" btn-danger text-color">{{$Bank->FloatAccountID}}</td>
                <td  class=" btn-dark text-color">{{$Bank->Float_Amount}}</td>

                
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Date Created</th>
                <th>Users_Name</th>
                <th class=" btn-primary text-color">Float Account</th>
                <th class=" btn-danger text-color">Amount Available</th>
              
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
