<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>{{$Title}}   <span class="btn btn-primary">Agent Name :: {{Auth::user()->name}}</span> </strong>
        </h4>
        

        <div class="mb-5">
          <div  class="wizard Wizard">
            <h3>
              <i class="fe fe-user wizard-steps-icon"></i>
              <span class="wizard-steps-title">Selected Telecom :: <span style="color:red">{{$Telecoms->Telecom_Name}}</span></span>
            
            </h3>
            <section class="text-center">
              <h3 class="d-none"></h3>
            

                    <div class="card jesus">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Please fill in all the fields</strong>
    </h4>
    <form method="POST" action="{{route('MasterTelecomAgencyFloatSubmit')}}" class="form" class="">
      <div class="form-row">
         @csrf
        <div class="form-group col-md">
          <label for="aaaaa">Float Amount to load</label>
          <input type="text"    required  class="form-control"  name="floatAmount" />

          <input type="hidden" name="Telecom_Name" value="{{$Telecoms->Telecom_Name}}">
          <input type="hidden" name="Telecom_ID" value="{{$Telecoms->Telecom_ID}}">
          <input type="hidden" name="User_ID" value="{{ Auth::user()->User_id }} ">
          <input type="hidden" name="Users_Name" value="{{ Auth::user()->name }} ">
        
          
        </div>
      </div>
      <div class="form-group">
         
          <input type="submit" class="btn btn-dark jesus" value="Recharge Float " />
        </div>
    </form>
  </div>
</div> 
</section>
            
          </div>
        </div>
      </div>
    
    </div>
  </div>
</div>
