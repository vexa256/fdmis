<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Select Telecom to attach float to</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Telecom Name</th>
                <th>Date Created</th>
                <th>Select Telecom</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>
                <td>{{$Telecom->Telecom_Name}}</td>
                <td>{{$Telecom->created_at->format('d-M-Y') }}</td>
                <td><a href="{{route('SelectMasterTelecom', ['id' => $Telecom->id])}}" class="btn btn-danger jesus ">Select Telecom</a></td>
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
              <th>Telecom Name</th>
                <th>Date Created</th>
                <th>Select Telecom</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
