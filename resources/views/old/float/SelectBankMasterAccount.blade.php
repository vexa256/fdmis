<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Select Bank to attach float to</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Date Created</th>
                <th>Select Bank</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <tr>
                <td>{{$Bank->BankName}}</td>
                <td>{{$Bank->BranchName}}</td>
                <td>{{ $Bank->created_at->format('d-M-Y') }}</td>
                <td><a href="{{route('MasterFloatRecharge', ['id' => $Bank->id])}}" class="btn btn-danger jesus ">Select Bank</a></td>
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                 <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Date Created</th>
                <th>Select Bank</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
