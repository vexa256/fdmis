<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Hello , {{Auth::user()->name }}, View Your Telecom Float Account(s)</strong>

        @if(Auth::user()->User_role == 'Super_User')
    


    <a href="#" class="btn btn-danger float-right btn-sm">Manage all Telecom Float Accounts</a>



      @endif

    </h4>


 

   


    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Telecom Name</th>
                <th>Date Created</th>
                <th>User's / Agency's Name</th>
                <th class=" btn-primary text-color">Float Account</th>
                <th class=" btn-danger text-color">Amount Available (UGX)</th>

              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>
                <td>{{$Telecom->TelecomName}}</td>
               
                <td>{{$Telecom->created_at->format('d-M-Y') }}</td>
              
                    @if(Auth::user()->User_role == 'Super_User')
                    <td  class=" btn-success jesus text-color"> {{$Telecom->Users_Name}}</td>
                    @endif  

                    @if(Auth::user()->User_role == 'Agency_Account')
                    <td  class=" btn-success jesus text-color"> {{$Telecom->Agency_Name}}</td>
                    @endif  
                <td  class=" btn-danger text-color">{{$Telecom->FloatAccountID}}</td>
                <td  class=" btn-dark text-color">{{$Telecom->Float_Amount}}</td>

              
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Telecom Name</th>
              
                <th>Date Created</th>
                <th>User's Name</th>
                <th class=" btn-primary text-color">Float Account</th>
                <th class=" btn-danger text-color">Amount Available</th>
           
               
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
