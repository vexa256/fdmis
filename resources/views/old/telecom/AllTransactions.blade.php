<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>View Telecom Transaction logs, </strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Telecom Name</th>
                <th>Agency Name</th>
                <th>Agent Name</th>
                <th>Transaction Type</th>
                <th class="btn-danger jesus">Amount (UGX)</th>
                <th>REF NO</th>
                <th>Client's Phone</th>
                <th>Transaction Phone</th>
                <th class="btn-dark jesus">Date</th>
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>
                <td>{{$Telecom->Telecom_Name}}</td>
                <td>{{$Telecom->Agency_Name}}</td>
                <td>{{$Telecom->Agent_Name}}</td>
                <td>{{$Telecom->Transaction_Type_w_d}}</td>
                <td>{{$Telecom->Amount}}</td>
                <td>{{$Telecom->Transaction_refference_number}}</td>
                <td>{{$Telecom->Customer_Phone}}</td>
                <td>{{$Telecom->Client_phone}}</td>
                <td>{{$Telecom->created_at->format('d-M-Y') }}</td>
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
               <th>Telecom Name</th>
                <th>Agency Name</th>
                <th>Agent Name</th>
                <th>Transaction Type</th>
                <th class="btn-danger jesus">Amount (UGX)</th>
                <th>REF NO</th>
                <th>Client's Phone</th>
                <th>Transaction Phone</th>
                <th class="btn-dark jesus">Date</th>
               
               
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
