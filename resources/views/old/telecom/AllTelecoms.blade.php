<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Manage all Telecoms in the system, Use delete with caution</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Telecom Name</th>
                <th>Date Created</th>
                <th>Updated</th>
                <th>Delete</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>
                <td>{{$Telecom->Telecom_Name}}</td>
                <td>{{ $Telecom->created_at->format('d-M-Y') }}</td>
                <td><a href="{{route('UpdateTelecoms_form', ['id' => $Telecom->id])}}" class="btn btn-primary jesus ">Update</a></td>
                <td><a href="{{route('DeleteTelecoms', ['id' => $Telecom->id])}}" class="btn btn-danger jesus ">Delete</a></td>
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Telecom Name</th>
                <th>Date Created</th>
                <th>Updated</th>
                <th>Delete</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
