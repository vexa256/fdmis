<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>View Telecom  Float Request Logs</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Telecom Name</th>
            
                   <th class="bg-dark jesus text-color">Float Source</th>
                   <th class="bg-primary jesus text-color">Account</th>
                <th>Agency Name</th>
                <th>Transaction Type</th>
                <th>Request Status</th>
                <th class="bg-danger jesus text-color">Float Amount Requested</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>
                <td>{{$Telecom->TelecomName}}</td>
               
                <td>HSDG Resources LTD</td>
                <td class="bg-danger text-color ">{{$Telecom->FloatAccountID}}</td>
                <td>{{$Telecom->Agency_Name}}</td>
                <td>{{$Telecom->Transaction_Type}}</td>
                <td>{{$Telecom->Request_Status}}</td>
                <td>{{$Telecom->Float_Amount}}</td>
                
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Telecom Name</th>
            
                   <th class="bg-dark jesus text-color">Float Source</th>
                   <th class="bg-primary jesus text-color">Account</th>
                <th>Agency Name</th>
                <th>Transaction Type</th>
                <th>Request Status</th>
                <th class="bg-danger jesus text-color">Float Amount Requested</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
