<div class="row">
  <div class="col-lg-4">
    <div class="card">
      <div class="card-body bg-primary jesus">
        <div class="d-flex flex-wrap align-items-center">
          <div class="mr-auto">
            <i class="fa fa-user-circle fa-5x text-color mb-2"></i> 
            <p class="text-uppercase text-dark font-weight-bold mb-1 text-color" >
            Total Child Agent Users
            </p>
            <p class="text-gray-5 mb-0 text-color">
               Bank / Telecom
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0 text-color">
           {{$Agents}}
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="card">
      <div class="card-body bg-dark jesus">
        <div class="d-flex flex-wrap align-items-center">
          <div class="mr-auto">
             <i class="fa fa-btc fa-5x text-color mb-2"></i> 
            <p class="text-uppercase text-dark font-weight-bold mb-1 text-color ">
            Total Float Accounts
            </p>
            <p class="text-gray-5 mb-0 text-color">
           Bank / Telecom
            </p>
          </div>
          <p class="text-primary font-weight-bold font-size-24 mb-0 text-color">
          {{$Total}}
          </p>
        </div>
      </div>
    </div>
  </div>
 <div class="col-lg-4">
    <div class="card">
      <div class="card-body bg-danger jesus">
        <div class="d-flex flex-wrap align-items-center">
          <div class="mr-auto">
             <i class="fa fa-money fa-5x text-color mb-2"></i> 
            <p class="text-uppercase text-dark font-weight-bold mb-1 text-color ">
            Total Comission (30 Days)
            </p>
            <p class="text-gray-5 mb-0 text-color">
            Bank / Telecom
            </p>
          </div>
          <p class="text-primary font-weight-bold font-size-24 mb-0 text-color">
            0 
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="card">
      <div class="card-body bg-success jesus">
        <div class="d-flex flex-wrap align-items-center">
          <div class="mr-auto">
             <i class="fa fa-pie-chart fa-5x text-color mb-2"></i> 
            <p class="text-uppercase text-dark font-weight-bold mb-1 text-color" >
             Your Float Requests
            </p>
            <p class="text-gray-5 mb-0 text-color">
               Bank / Telecom
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0 text-color">
          {{$Total_Float}}
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="card">
      <div class="card-body bg-warning jesus">
        <div class="d-flex flex-wrap align-items-center">
          <div class="mr-auto">
            <i class="fa fa-bar-chart fa-5x text-color mb-2"></i> 
            <p class="text-uppercase text-dark font-weight-bold mb-1 text-color ">
             Child Agent Users Online
            </p>
            <p class="text-gray-5 mb-0 text-color">
           Bank / Telecom
            </p>
          </div>
          <p class="text-primary font-weight-bold font-size-24 mb-0 text-color">
          0
          </p>
        </div>
      </div>
    </div>
  </div>
 <div class="col-lg-4">
    <div class="card">
      <div class="card-body bg-info jesus">
        <div class="d-flex flex-wrap align-items-center">
          <div class="mr-auto">
            <i class="fa fa-line-chart fa-5x text-color mb-2"></i> 
            <p class="text-uppercase text-dark font-weight-bold mb-1 text-color ">
         Agent User Float Requests
            </p>
            <p class="text-gray-5 mb-0 text-color">
            Bank / Telecom
            </p>
          </div>
          <p class="text-primary font-weight-bold font-size-24 mb-0 text-color">
            0
          </p>
        </div>
      </div>
    </div>
  </div>
 </div>

