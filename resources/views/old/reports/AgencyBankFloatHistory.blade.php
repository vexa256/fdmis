<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>View Bank  Float Request Logs</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                   <th class="bg-dark jesus text-color">Float Source</th>
                   <th class="bg-primary jesus text-color">Account</th>
                <th>Agency Name</th>
                <th>Transaction Type</th>
                <th>Request Status</th>
                <th class="bg-danger jesus text-color">Float Amount Requested</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <tr>
                <td>{{$Bank->BankName}}</td>
                <td>{{$Bank->BranchName}}</td>
                <td>HSDG Resources LTD</td>
                <td class="bg-danger text-color ">{{$Bank->FloatAccountID}}</td>
                <td>{{$Bank->Agency_Name}}</td>
                <td>{{$Bank->Transaction_Type}}</td>
                <td>{{$Bank->Request_Status}}</td>
                <td>{{$Bank->Float_Amount}}</td>
                
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                   <th class="bg-dark jesus text-color">Float Source</th>
                        <th class="bg-primary jesus text-color">Account</th>
                <th>Agency Name</th>
                <th>Transaction Type</th>
                <th>Request Status</th>
                <th class="bg-danger jesus text-color">Float Amount Requested</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
