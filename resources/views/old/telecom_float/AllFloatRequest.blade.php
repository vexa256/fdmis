<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>View all Telecom float requests, </strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Telecom</th>
                <th>Agency </th>
                <th>Agent Name</th>
                <th>Requested Float (UGX)</th>
                <th>Date Created</th>
                <th class="btn-danger jesus">Request Status</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($TelecomFloats))

              @foreach($TelecomFloats as $TelecomFloat)
              <tr>
                <td>{{$TelecomFloat->Telecom_Name}}</td>
                <td>{{$TelecomFloat->Agency_Name}}</td>
                <td>{{$TelecomFloat->Request_Agent_Name}}</td>
                <td>{{$TelecomFloat->Requested_float}}</td>
                <td>{{$TelecomFloat->created_at->format('d-M-Y') }}</td>
                <td class="btn-dark jesus">{{$TelecomFloat->Request_status}}</td>
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
               <th>Telecom Name</th>
                <th>Agency Name</th>
                <th>Agent Name</th>
                <th>Requested Float (UGX)</th>
                   <th>Date Created</th>
                <th class="btn-danger jesus">Request Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
