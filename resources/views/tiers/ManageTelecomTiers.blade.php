<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Manage Wallet tiers (Currency is UGX) for :: </strong> <span class="jesus ml-1 badge bg-dark text-white font-size-12 text-uppercase air__topbar__status"> {{$TierData->Telecom_Name}}</span>

      <a href="#tiersAdd" data-toggle="modal" class="text-light btn btn-danger float-right"><i class="fa fa-plus"></i> Add a Tier</a>

    </h4>


     @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="">
            <thead>
              <tr>
                <th>Wallet Name</th>
                <th class="text-light btn-primary">Range From </th>
                <th class="text-light btn-danger">Range To </th>
                <th class="text-light btn-dark">Deposit Charge </th>
                <th class="text-light btn-dark">Withdraw Charge </th>
                <th class="btn-info">Deposit Commision </th>
                <th class="text-light btn-primary">Withdraw Commision </th>
                <th>Date Created</th>
                <th>Delete Tier</th>


              </tr>
            </thead>
            <tbody>


               @if(!is_null($Tiers))

              @foreach($Tiers as $Tier)
              <tr>
                <td>{{$Tier->TelecomName}}</td>
                <td class="text-light btn-primary">{{$Tier->range_from}}</td>
                <td class="text-light btn-danger">{{$Tier->range_to}}</td>
                <td class="btn-info">{{$Tier->DepositCharge}}</td>
                <td class="btn-info">{{$Tier->WithdrawCharge}}</td>
                <td class="btn-info">{{$Tier->DepositCommission}}</td>
                <td class=" btn-primary">{{$Tier->WithdrawCommission}}</td>
                <td>{{ $Tier->created_at->format('d-M-Y') }}</td>
                <td><a href="{{route('DeleteTelecomTier', ['id' => $Tier->id])}}" class="text-light btn btn-danger jesus ">Delete</a></td>


              </tr>


              @endforeach
              @endif

            </tbody>
            <tfoot>
              <tr>
                <th>Wallet Name</th>
                <th class="text-light btn-primary">Range From </th>
                <th class="text-light btn-danger">Range To </th>
                <th class="text-light btn-dark">Deposit Charge </th>
                <th class="text-light btn-dark">Withdraw Charge </th>
                <th class="btn-info">Deposit Commision </th>
                <th class="text-light btn-primary">Withdraw Commision </th>
                <th>Date Created</th>
                <th>Delete Tier</th>


              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>



 <div class="modal fade" id="tiersAdd"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add a tier for the Wallet ::  <span class="jesus ml-1 badge bg-dark text-white font-size-12 text-uppercase air__topbar__status"> {{$TierData->Telecom_Name}}</span>

Please ignore the fields you don't need
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <div class="card-body">



                      <form method="POST" action="{{route('SubmitTelecomTiers')}}">
                         @csrf

                          <div class="row">


                            <div class="col-md">

                            <label class="mt-3">Range From</label>

                          <input type="text" class="form-control" name="range_from" placeholder="e.g 2000">

                            </div>


                            <input type="hidden" name="TelecomName" value="{{$TierData->Telecom_Name}}">
                            <input type="hidden" name="TelecomID" value="{{$TierData->Telecom_ID}}">


                            <div class="col-md">

                            <label class="mt-3">Range To</label>

                          <input type="text" class="form-control" name="range_to" placeholder="e.g 10000 ">

                            </div>

           <div class="col-md">

                            <label class="mt-3">Deposit Charge</label>

                <input type="text" class="form-control" name="DepositCharge" placeholder="Deposit Charge">


                            </div>





                          </div>



                          <div class="row">






                            <div class="col-md">

                            <label class="mt-3">Withdraw Charge</label>

                <input type="text" class="form-control" name="WithdrawCharge" placeholder="Withdraw Charge">


                            </div>



                              <div class="col-md">

                            <label class="mt-3">Withdraw Commission</label>

                <input type="text" class="form-control" name="WithdrawCommission" placeholder="Withdraw Commission">


                            </div>



                            <div class="col-md">

                            <label class="mt-3">Deposit Commission</label>

                <input type="text" class="form-control" name="DepositCommission" placeholder="Deposit Commission">


                            </div>

                          </div>







                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="text-light btn btn-primary">Save changes</button>
                </div>

                    </form>
              </div>
            </div>
          </div>
