
            <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="#" class="air__menuLeft__link">
                <i class="fa  fa-building  air__menuLeft__icon"></i>
                <span>Outlets</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class="air__menuLeft__item">
                  <a href="{{route('AllOutlets')}}" class="air__menuLeft__link">
                    <span>Outlet List</span>
                  </a>
                </li>
                <li class=" air__menuLeft__item">
                  <a href="{{route('CreateOutletForm')}}" class="air__menuLeft__link">
                    <span>New Outlet</span>
                  </a>
                </li>

                 <li class=" air__menuLeft__item">
                  <a href="" class="air__menuLeft__link">
                    <span>Outlet Map</span>
                  </a>
                </li>

                <li class=" air__menuLeft__item">
                  <a href="{{route('TelecomAddPhoneNumbers')}}" class="air__menuLeft__link">
                    <span> Outlet Wallet Accounts</span>
                  </a>
                </li>


                <li class=" air__menuLeft__item">
                  <a href="{{route('AssignBankAccounts')}}" class="air__menuLeft__link">
                    <span>Outlet Bank Accounts</span>
                  </a>
                </li>
              </ul>
            </li>


            <li class=" agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa fa-users air__menuLeft__icon"></i>
                <span>Agent Users </span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class="air__menuLeft__item">
                  <a href="{{route('CreateAgentUserAccount')}}" class="air__menuLeft__link">
                    <span>New User Account </span>
                  </a>
                </li>
                <li class=" air__menuLeft__item">
                  <a href="{{route('ManageAgentUsers')}}" class="air__menuLeft__link">
                    <span>Manage Agent Users</span>
                  </a>
                </li>


              </ul>
            </li>



            <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa fa-money air__menuLeft__icon"></i>
                <span>My Float </span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class="air__menuLeft__item">
                  <a href="{{route('AgencyTelecomFloatRequest')}}" class="air__menuLeft__link">
                    <span>  Request Wallet Float </span>
                  </a>
                </li>
                <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyFloatRequestsHistory')}}" class="air__menuLeft__link">
                    <span>Manage Wallet Float Requests</span>
                  </a>
                </li>

                 <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyBankFloatRequestForm')}}" class="air__menuLeft__link">
                    <span>Request Bank Float</span>
                  </a>
                </li>

                 <li class=" air__menuLeft__item">
                  <a href="{{route('BankAgencyFloatRequestsHistory')}}" class="air__menuLeft__link">
                    <span>Manage Bank Float Requests</span>
                  </a>
                </li>
              </ul>
            </li>


            <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa fa-user-circle-o air__menuLeft__icon"></i>
                <span>Agent-User Float </span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class="air__menuLeft__item">
                  <a href="{{route('AgencyApproveAgentFloatRequest')}}" class="air__menuLeft__link">
                    <span>Manage Wallet Float Request</span>
                  </a>
                </li>
                <li class=" air__menuLeft__item">
                  <a href="{{route('Agency_Approve_Bank_Float')}}" class="air__menuLeft__link">
                    <span>Manage Bank Float Request</span>
                  </a>
                </li>
              </ul>
            </li>














             <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa fa-line-chart air__menuLeft__icon"></i>
                <span>My Reports </span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyTelecomCommission')}}" class="air__menuLeft__link">
                    <span>Wallet Commission</span>
                  </a>
                </li>

                <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyBankCommission')}}" class="air__menuLeft__link">
                    <span>Bank Commission</span>
                  </a>
                </li>


                 <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyManageTelecomCash')}}" class="air__menuLeft__link">
                    <span>Wallet Cash at hand</span>
                  </a>
                </li>

                <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyManageBankCash')}}" class="air__menuLeft__link">
                    <span>Bank Cash at hand</span>
                  </a>
                </li>



              </ul>
            </li>




             <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa fa-gears air__menuLeft__icon"></i>
                <span>Account Setup </span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyTelecomAccountSetUpForm')}}" class="air__menuLeft__link">
                    <span>Setup Wallets</span>
                  </a>
                </li>

                <li class=" air__menuLeft__item">
                  <a href="{{route('AgencyBankAccountSetUpForm')}}" class="air__menuLeft__link">
                    <span>Setup Bank Accounts</span>
                  </a>
                </li>
              </ul>
            </li>



             <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa   fa-user air__menuLeft__icon"></i>
                <span>Human Resource</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">

                <li class=" air__menuLeft__item">
                  <a href="http://34.67.254.202:9007/panel" class="air__menuLeft__link">
                    <span>HR Module</span>
                  </a>
                </li>

              </ul>
            </li>
