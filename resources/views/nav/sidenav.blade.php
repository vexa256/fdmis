 <div class="air__menuLeft">
      <div class="air__menuLeft__outer">
        <div class="air__menuLeft__mobileToggleButton air__menuLeft__mobileActionToggle">
          <span></span>
        </div>
        <div class="air__menuLeft__toggleButton air__menuLeft__actionToggle">
          <span></span>
          <span></span>
        </div>
        <a href="javascript: void(0);" class="air__menuLeft__logo">
          <img src="{{url('components/core/img/air-logo.png')}}" alt="Air UI" />
          <div class="air__menuLeft__logo__name">FDMIS</div>
          <div class="air__menuLeft__logo__descr"></div>
        </a>
        <a href="javascript: void(0);" class="air__menuLeft__user">
          <div class="air__menuLeft__user__avatar">
            <img src="{{url('components/core/img/avatars/avatar.png')}}" alt="" />
          </div>
          <div class="air__menuLeft__user__name">
           {{ Auth::user()->name }}
          </div>
          <div class="air__menuLeft__user__role">
           {{ Auth::user()->email }}
          </div>
        </a>
        <div class="air__menuLeft__container air__customScroll">
          <ul class="air__menuLeft__list">
            <li class="air__menuLeft__category">
              <span>Menu</span>
            </li>


@if(Auth::user()->User_role == 'Master')

    @include('nav.Admin')

    @endif




 @if(Auth::user()->User_role == 'Agency')

    @include('nav.Agency')

    @endif



    @if(Auth::user()->User_role == 'Agent_User')

    @include('nav.AgentUser')

    @endif



          
           
            
          </ul>
         <!-- <div class="air__menuLeft__banner">
            <p>Please use the delete functionality cautiously , Data can not be  recovered after deletion.</p>
            <a href="javascript: void();" class="btn btn-white text-center d-block">Read Privacy Policy</a>
          </div>-->
        </div>
      </div>
    </div>