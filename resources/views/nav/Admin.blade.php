<li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
    <a href="javascript: void(0)" class="air__menuLeft__link">
        <i class="fa   fa-bank air__menuLeft__icon"></i>
        <span>Banks</span>
        <span
            class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
    </a>
    <ul class="air__menuLeft__list">
        <li class="air__menuLeft__item">
            <a href="{{ route('BankSetupAccountForm') }}"
                class="air__menuLeft__link">
                <span>Create Bank</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ListBanks') }}" class="air__menuLeft__link">
                <span>Bank List</span>
            </a>
        </li>

        <li class=" air__menuLeft__item">
            <a href="{{ route('ListBanks') }}" class="air__menuLeft__link">
                <span>Manage Bank Tiers</span>
            </a>
        </li>
    </ul>
</li>






<li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
    <a href="javascript: void(0)" class="air__menuLeft__link">
        <i class="fa   fa-phone-square air__menuLeft__icon"></i>
        <span>Telecoms</span>
        <span
            class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
    </a>
    <ul class="air__menuLeft__list">
        <li class="air__menuLeft__item">
            <a href="{{ route('TelecomSetup_Form') }}"
                class="air__menuLeft__link">
                <span>Create Telecom</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ListTelecoms') }}" class="air__menuLeft__link">
                <span>Telecom List</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ListTelecoms') }}" class="air__menuLeft__link">
                <span>Manage Telecom Tiers</span>
            </a>
        </li>
    </ul>
</li>



<li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
    <a href="javascript: void(0)" class="air__menuLeft__link">
        <i class="fa   fa-money air__menuLeft__icon"></i>
        <span>Float</span>
        <span
            class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
    </a>
    <ul class="air__menuLeft__list">
        <li class="air__menuLeft__item">
            <a href="{{ route('AdminBankAgencyFloat') }}"
                class="air__menuLeft__link">
                <span>Agent Bank Float</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('AgencyTelecomFloat') }}"
                class="air__menuLeft__link">
                <span>Agent Wallet Float</span>
            </a>
        </li>

    </ul>
</li>





<li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
    <a href="javascript: void(0)" class="air__menuLeft__link">
        <i class="fa   fa-users air__menuLeft__icon"></i>
        <span>Agents</span>
        <span
            class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
    </a>
    <ul class="air__menuLeft__list">
        <li class="air__menuLeft__item">
            <a href="{{ route('AdminCreateAgencyAccount') }}"
                class="air__menuLeft__link">
                <span>Create Agent Account</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ManageAgency') }}" class="air__menuLeft__link">
                <span>Manage Agent Accounts</span>
            </a>
        </li>

    </ul>
</li>


<li class="agent_user air__menuLeft__item air__menuLeft__submenu d-none">
    <a href="javascript: void(0)" class="air__menuLeft__link">
        <i class="fa   fa-user air__menuLeft__icon"></i>
        <span>Collections</span>
        <span
            class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
    </a>
    <ul class="air__menuLeft__list">
        <li class="air__menuLeft__item">
            <a href="{{ route('ManageCollectors') }}"
                class="air__menuLeft__link">
                <span>Manage Collectors</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ListTelecoms') }}" class="air__menuLeft__link">
                <span>Wallet List</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ListTelecoms') }}" class="air__menuLeft__link">
                <span>Manage Wallet Tiers</span>
            </a>
        </li>
    </ul>
</li>


<li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
    <a href="javascript: void(0)" class="air__menuLeft__link">
        <i class="fa   fa-wrench air__menuLeft__icon"></i>
        <span>Settings</span>
        <span
            class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
    </a>
    <ul class="air__menuLeft__list">
        <li class="air__menuLeft__item">
            <a href="{{ route('AdminCreateAccount') }}"
                class="air__menuLeft__link">
                <span>Create Admin Account</span>
            </a>
        </li>
        <li class=" air__menuLeft__item">
            <a href="{{ route('ManageAdmins') }}" class="air__menuLeft__link">
                <span>Manage Admin Accounts</span>
            </a>
        </li>

    </ul>
</li>


{{-- <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa   fa-user air__menuLeft__icon"></i>
                <span>Human Resource</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">

                <li class=" air__menuLeft__item">
                   <a href="http://34.67.254.202:9007/panel" class="air__menuLeft__link">
                    <span>HR Agent Accounts</span>
                  </a>
                </li>

              </ul>
            </li> --}}
