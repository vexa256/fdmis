
            <li class="air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fe fe-home air__menuLeft__icon"></i>
                <span> Transactions </span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class="air__menuLeft__item">
                  <a href="{{route('DepositsBank')}}" class="air__menuLeft__link">
                    <span>Deposits bank </span>
                  </a>
                </li>
                <li class=" air__menuLeft__item">
                  <a href="{{route('BankWithdrawForm')}}" class="air__menuLeft__link">
                    <span>Withdraw bank</span>
                  </a>
                </li>

                <li class=" air__menuLeft__item">
                  <a href="{{route('DepositsTelecom')}}"  class="air__menuLeft__link">
                    <span>Deposits Wallet</span>
                  </a>
                </li>

                <li class=" air__menuLeft__item">
                  <a href="{{route('TelecomWithdrawForm')}}" class="air__menuLeft__link">
                    <span>Withdraw Wallet</span>
                  </a>
                </li>
              </ul>
            </li>



            <li class="air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fe fe-home air__menuLeft__icon"></i>
                <span>Agent-User Float</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
                <li class="air__menuLeft__item">
                  <a href="{{route('TelecomFloatRequest')}}" class="air__menuLeft__link">
                    <span> Wallet Float Request </span>
                  </a>
                </li>

                 <li class=" air__menuLeft__item">
                  <a href="{{route('AgentFloatRequestsHistory')}}" class="air__menuLeft__link">
                    <span>Manage  Wallet Float </span>
                  </a>
                </li>

                <li class="air__menuLeft__item">
                  <a href="{{route('BankFloatRequestForm')}}" class="air__menuLeft__link">
                    <span> Bank Float Request </span>
                  </a>
                </li>


                <li class="air__menuLeft__item">
                  <a href="{{route('BankAgentFloatRequestsHistory')}}" class="air__menuLeft__link">
                    <span>Manage  Bank Float  </span>
                  </a>
                </li>

              </ul>
            </li>





             <li class="air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fe fe-home air__menuLeft__icon"></i>
                <span>Reports</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">




               <li class="air__menuLeft__item">
                  <a href="{{route('ManageBankCash')}}" class="air__menuLeft__link">
                    <span>Bank Cash at Hand </span>
                  </a>
               </li>
               <li class="air__menuLeft__item">
                  <a href="{{route('ManageTelecomCash')}}" class="air__menuLeft__link">
                    <span>Wallet Cash at Hand </span>
                  </a>
               </li>
                <li class="air__menuLeft__item">
                  <a href="{{route('BankCommission')}}" class="air__menuLeft__link">
                    <span>Bank Commissions </span>
                  </a>
               </li>


               <li class="air__menuLeft__item">
                  <a href="{{route('TelecomCommission')}}" class="air__menuLeft__link">
                    <span>Wallet  Commission </span>
                  </a>
               </li>
              </ul>
            </li>



             <li class="air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fe fe-user air__menuLeft__icon"></i>
                <span>Profile</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">
               <li class="air__menuLeft__item">
                  <a href="{{route('ViewProfile')}}" class="air__menuLeft__link">
                    <span>View Account Profile </span>
                  </a>
               </li>
              </ul>
            </li>

            <li class="agent_user air__menuLeft__item air__menuLeft__submenu ">
              <a href="javascript: void(0)" class="air__menuLeft__link">
                <i class="fa   fa-user air__menuLeft__icon"></i>
                <span>Human Resource</span>
                <span class="badge text-white bg-blue-light float-right mt-1 px-2"></span>
              </a>
              <ul class="air__menuLeft__list">

                <li class=" air__menuLeft__item">
                  <a href="http://34.67.254.202:9007/panel" class="air__menuLeft__link">
                    <span>HR Module</span>
                  </a>
                </li>

              </ul>
            </li>
