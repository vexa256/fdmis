<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <form method="POST" action="{{route('BankDepositLogic')}}"  class="">
        <h4 class="mb-4">
          <strong>{{$Title}}</strong>

           <input type="submit" class=" float-right btn btn-danger" name="" value="Deposit">

        </h4>
   


      @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




                  <table class="table  col-md">
        
        <thead>

          <tr>
            <th>Total Amount Deposited (UGX)</th>
            <th>Recipient's Account</th>
         
            <th>Sender's Name</th>
            <th>Bank Carrier</th>

            
          </tr>

   @csrf
          </thead>
          <tbody>

          <tr>
            
            <td class=""><input type="text" readonly value="0" class="form-control total_sum" name=""> </td>
            <input type="hidden" name="TotalDeposit" value="" class="TotalDeposit">
            <td class="btn-dark "><input type="text" value="" class="form-control " name="RecipientAccount"> </td>

      <input type="hidden" value="Not Applicable" class="form-control " name="RecipientName"> 
            <td class="btn-danger "><input type="text"  class="form-control " name="SenderName"> </td>
            <td class="btn-warning "><select class="form-control" name="BankCarrier">
              
              <option value="">Choose Bank Carrier</option>

              @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <option value="{{$Bank->id}}">{{$Bank->BankName}} ({{$Bank->BankAccount}})</option>
              
              @endforeach 
              @endif       
            </select ></td>
          

          </tr>

        </tbody>


        </table>


 


                
     
      <table class="table col-md">
        
        <thead>
          
        <tr>
          <th>Denomination</th>
          <th>NO</th>
          <th>Amount (UGX)</th>
        </tr>


        </thead>

        <tbody>
          
        <tr>
         <td class="btn-dark">50,000</td>
         <td ><input type="text" class="form-control data_onchange a1" placeholder="NO"></td>
         <td ><input type="text" class="form-control a-1"  readonly></td>
        </tr>

        <tr>
         <td class="btn-dark">20,000</td>
         <td ><input type="text" class="form-control data_onchange a2" placeholder="NO"></td>
         <td ><input type="text" class="form-control a-2"  readonly></td>
        </tr>
        <tr>
         <td class="btn-dark">10,000</td>
         <td ><input type="text" class="form-control data_onchange a3" placeholder="NO"></td>
         <td ><input type="text" class="form-control a-3"  readonly></td>
        </tr>

        <tr>
         <td class="btn-dark">5,000</td>
         <td ><input type="text" class="form-control data_onchange a4" placeholder="NO"></td>
         <td class=""><input type="text" class="form-control a-4"  readonly></td>
        </tr>

        <tr>
         <td class="btn-dark">1,000</td>
         <td ><input type="text" class="form-control data_onchange a5" placeholder="NO"></td>
         <td class=""><input type="text" class="form-control a-5"  readonly></td>
        </tr>

        <tr>
         <td class="btn-dark">500</td>
         <td ><input type="text" class="form-control data_onchange a6" placeholder="NO"></td>
         <td ><input type="text" class="form-control a-6"  readonly></td>
        </tr>

        <tr>
         <td class="btn-dark">100</td>
         <td><input type="text" class="form-control data_onchange a7" placeholder="NO"></td>
         <td class=""><input type="text" class="form-control a-7"  readonly></td>
        </tr>

        <tr>
         <td class="btn-dark">50</td>
         <td ><input type="text" class="form-control data_onchange a8" placeholder="NO"></td>
         <td ><input type="text" class="form-control a-8"  readonly></td>
        </tr>

        

        </tbody>



</table>
</form>
</div>
</div>
</div>
</div>

<script>
  

   $(document).ready(function () {

        function numberWithCommas(number) {
            var parts = number.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
          

          function NotesCounter(c) {
           
              var a = $('.total_sum').val();

              var ab = parseInt(a);
              var ac = parseInt(c);

              $(".total_sum").val(0);
             

              var a1 = $('.a-1').val();
              var a2 = $('.a-2').val();
              var a3 = $('.a-3').val();
              var a4 = $('.a-4').val();
              var a5 = $('.a-5').val();
              var a6 = $('.a-6').val();
              var a7 = $('.a-7').val();
              var a8 = $('.a-8').val();
              
            
             

              var  sum_add = Number(a1)+Number(a2)+Number(a3)+Number(a4)+Number(a5)+Number(a6)+Number(a7)+Number(a8);

             var total_add =  numberWithCommas(sum_add);


               $(".total_sum").val(total_add+" "+" UGX");
               $(".TotalDeposit").val(sum_add);


          }


          $(".data_onchange").on("keyup", function(){

            var a1 = $('.a-1').val();
            var a2 = $('.a-2').val();
            var a3 = $('.a-3').val();
            var a4 = $('.a-4').val();
            var a5 = $('.a-5').val();
            var a6 = $('.a-6').val();
            var a7 = $('.a-7').val();
            var a8 = $('.a-8').val();
           


          });





        $(".a1").on("keyup", function(){

          var sum = $('.total_deposit').val();

          var total_deposit = parseInt(sum);
           
          var a1 = $('.a1').val();
          var b = 50000*a1;
          var c = b; 

          var sum_a = parseInt(b);

          $('.a-1').val(c);
          NotesCounter(b);

          
           

       });


         $(".a2").on("keyup", function(){
           
          var a1 = $('.a2').val();
          var b = 20000*a1;
          var c = b; var sum_a = parseInt(b);
          

          $('.a-2').val(c);
          NotesCounter(b);

           
           

       });


          $(".a3").on("keyup", function(){
           
          var a1 = $('.a3').val();
          var b = 10000*a1;
          var c = b; var sum_a = parseInt(b);

          $('.a-3').val(c);
           NotesCounter(b);

           
           

       });


          $(".a4").on("keyup", function(){
           
          var a1 = $('.a4').val();
          var b = 5000*a1;
          var c = b; var sum_a = parseInt(b);

          $('.a-4').val(c);
           NotesCounter(b);

           
           

       });


            $(".a5").on("keyup", function(){
           
          var a1 = $('.a5').val();
          var b = 1000*a1;
          var c = b; var sum_a = parseInt(b);

           $('.a-5').val(c);
           NotesCounter(b);

           
           

       });


             $(".a6").on("keyup", function(){
           
          var a1 = $('.a6').val();
          var b = 500*a1;
          var c = b; var sum_a = parseInt(b);

          $('.a-6').val(c);
           NotesCounter(b);

           
           

       });




               $(".a20").on("keyup", function(){
           
          var a1 = $('.a20').val();
          var b = 200*a1;
          var c = b; var sum_a = parseInt(b);

          $('.a-20').val(c);
           NotesCounter(b);

           
           

       });




      $(".a7").on("keyup", function(){
           
          var a1 = $('.a7').val();
          var b = 100*a1;
          var c = b; var sum_a = parseInt(b);

          $('.a-7').val(c);
           NotesCounter(b);
          
          
           

       });




      $(".a8").on("keyup", function(){
           
          var a1 = $('.a8').val();
          var b = 50*a1;
          var c = b; var sum_a = parseInt(b);

          $('.a-8').val(c);
           NotesCounter(b);

           
           

       });




            


            });
  </script>
