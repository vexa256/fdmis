<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Assign Outlet merchant bank  account number</strong> <a href="#daf" class="float-right btn btn-danger btn-sm jesus" data-toggle="modal">Attach Bank Account</a>
    </h4>


     @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th>Agency Name</th>
                <th class=" btn-danger jesus text-light">Outlet Name</th>
                <th>Bank Name</th>
                <th class=" btn-dark jesus text-light">Bank Account</th>
                <th>Date Created</th>
                <th>Delete</th>
               
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Agents))

              @foreach($Agents as $Agent)
              <tr>
                <td>{{$Agent->AgencyName}}</td>
                <td class=" btn-danger jesus text-light">{{$Agent->OutletName}}</td>
                <td>{{$Agent->BankName}}</td>
                <td class=" btn-dark jesus text-light">{{$Agent->BankAccount}}</td>
                <td>{{$Agent->created_at->format('d-M-Y')}}</td>
                <td><a href="{{route('DeleteOutletBankAccount', ['id' => $Agent->id])}}" class="btn btn-sm btn-danger jesus ">Delete</a></td> 
              
               
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
              <tr>
                  <th>Agency Name</th>
                <th class=" btn-danger jesus text-light">Outlet Name</th>
                <th>Bank Name</th>
                <th class=" btn-dark jesus text-light">Bank Account</th>
                <th>Date Created</th>
                <th>Delete</th>
               
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


 <div class="modal fade" id="daf" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Assign outlet merchant bank account number </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" style="padding-top: 5% !important; padding-bottom: 10% !important">
                 


                  <div class="container-fluid">
                    
                  
                      <form method="POST" action="{{route('SubmitBankAccountNumber')}}">
                         @csrf
                            <div class="row">
                            <div class="col-md">

                              <input type="hidden" name="ID" value="">

                              <label>Choose Bank </label>
                               
                               <select class="form-control"  name="BankID" >

                                  <option value="">Choose Bank</option>
                                 
                                  @if(!is_null($Banks))

                                  @foreach($Banks as $Bank)
                              

                                   <option value="{{$Bank->id}}">{{$Bank->BankName}}</option>


                                @endforeach 

                                @endif       


                               </select>
                              
                            </div> 

                             <div class="col-md">

                              <label>Choose Outlet</label>
                               
                               <select class="form-control"  name="OutletID" >

                                  <option value="">Choose Outlets</option>
                                 
                                  @if(!is_null($Outlets))

                                  @foreach($Outlets as $Outlet)

                                   <option value="{{$Outlet->id}}">{{$Outlet->outlet_name}}</option>


                                @endforeach 

                                @endif       


                               </select>
                              
                            </div> 

                            <div class="col-md" >
                               <label>Assign Bank Account Number</label>
                               <input type="text" name="OutletAccountNumber" class="form-control" >

                            
                            </div> 
                          </div>




                </div>
                <div class="modal-footer" >
                 
                  <button type="submit" class="btn btn-primary">Save changes</button>

                   </form>
                </div>
              </div>
            </div>
          </div>