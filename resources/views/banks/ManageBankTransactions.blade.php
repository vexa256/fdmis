
          <div class="modal fade" id="BankTRansactions9292292929" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Manage Bank Transaction Records</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                




                      <div class="row">
      <div class="col-lg col-md">
           
          <form class="" method="POST" action="{{route('BankFilterDatesFromTransactions')}}">

      @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



             @csrf
          <div class="form-group row pl-3">

            <button class="btn btn-primary btn-with-addon mr-auto text-nowrap d-none d-md-block">
              <span class="btn-addon">
                <i class="btn-addon-icon fe fe-plus-circle"></i>
              </span>
             {{$BankTotal}}   (UGX)  Total Bank Commission
            </button>



            <div class="col-md">
              <label class="col-form-label">Date From</label>
              <input type="date" name="start_date" class="form-control-sm DateQuery" value=" @if(!is_null($Bstart))

              {{$Bstart}}
              
              @endif">
            </div>
            <div class="col-md">
              <label class="col-form-label">Date  To</label>
              <input type="date" name="end_date" class="form-control-sm DateQuery" value="
              @if(!is_null($Bend))

              {{$Bend}}

              @endif
              ">
            </div>

             <div class="col-md mt-5">
             <input type="submit" name="" value="Search" class="btn btn-danger btn-sm jesus">
            </div>
          </div>

      </form>
     <!--   <div class="card jesus bg-primary text-light">
      <div class="card-body">
        <p class="text-light font-size-40 font-weight-bold mb-2">
       {{$BankTotal}} (UGX)   <i class=" fa fa-money fa-2x float-right"></i>
        </p>
        <p class="text-uppercase text-light mb-3">
          Total Revenue from Bank Commissions
        </p>
        
    </div>
    </div>-->
        <div class="mb-5">
          <table class="table ters table-hover table-bordered table-striped">
            <thead>
              <tr>
               
                <th class="btn-danger">TID</th>
              <th class="btn-dark">Outlet </th>
              <th class="btn-dark">Bank </th>
              <th class="btn-dark">Client Acc</th>
              <th class="btn-dark">Agent Acc</th>
                <th class="btn-danger">Type</th>
              <th class="btn-dark">Date</th>
              <th class="btn-dark">Amount</th>
                <th class="btn-dark">Running Balance</th>
                <th class="btn-danger"> Commission</th>
            
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Banks))

              @foreach($Banks as $Bank)
              <tr>
               
                 <td class="bg-dark jesus text-light">{{$Bank->TransID}}</td>
                <td  class=" text-dark">{{$Bank->OutletName}}</td>
                <td  class=" text-dark">{{$Bank->BankName}}</td>
                <td  class=" text-dark">{{$Bank->Reciever_Acc}}</td>
                <td  class="bg-primary text-light">{{$Bank->AgentBankAcc}}</td>
                <td  class="btn-dark">{{$Bank->Transaction_Type}}</td>
                <td  class=" text-dark"> <?php echo date("d/M/y, H:i:s", strtotime($Bank->created_at));?></td>
                <td  class=" text-dark"><?php echo number_format($Bank->amount);?> (UGX)</td>
                <td  class="">
                  @if($Bank->Transaction_Type == "Deposit")

                  <i class="fa fa-minus fa-1x" style="color: red !important"></i>

                   @endif  


                    @if($Bank->Transaction_Type == "Withdraw")

                  <i class="fa fa-plus fa-1x" style="color: red !important"></i>

                   @endif     


                  <?php echo number_format($Bank->Running_Balance);?> (UGX)</td>
                <td  class="bg-dark jesus text-light">

                  @if($Bank->Deposit_Commission>0)

                  <?php echo number_format($Bank->Deposit_Commission);?>

                   @endif     

                   @if($Bank->Withdraw_Commission>0)

                  <?php echo number_format($Bank->Withdraw_Commission);?>

                   @endif     

                     (UGX)</td>
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
                <tr>
              
                <th class="btn-danger">TID</th>
              <th class="btn-dark">Outlet </th>
              <th class="btn-dark">Bank </th>
              <th class="btn-dark">Client Acc</th>
              <th class="btn-dark">Agent Acc</th>
                <th class="btn-danger">Type</th>
              <th class="btn-dark">Date</th>
              <th class="btn-dark">Amount</th>
                <th class="btn-dark">Running Balance</th>
                <th class="btn-danger"> Commission</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>






                </div>
               
              </div>
            </div>
          </div>
