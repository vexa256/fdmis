<div class="card">
  <div class="card-body">
    <h4 class="mb-4">


      Manage Wallet Transaction Records

      <button data-toggle="modal" data-target="#BankTRansactions9292292929" class="btn btn-danger float-right"><i class="fa fa-credit-card "></i> View Bank Records</button>

    </h4>

    <div class="row">
      <div class="col-lg col-md">

          <form class="" method="POST" action="{{route('FilterDatesFromTransactions')}}">

      @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



             @csrf
          <div class="form-group row pl-3">
            <div class="card jesus bg-dark text-light">
      <div class="card-body">
        <p class="text-light font-size-25 font-weight-bold mb-2">
      {{$TelecomTotal}}   (UGX) <i class=" fa fa-money fa-2x float-right"></i>
        </p>
        @if($Filter == "True")
        <p class="text-uppercase text-light mb-3">
          Total Wallet Commission for the given date range
        </p>
        @endif


         @if($Filter == "")
        <p class="text-uppercase text-light mb-3">
          Total Wallet Commission for all transactions
        </p>
        @endif

    </div>
    </div>
            <div class="col-md">
              <label class="col-form-label">Date From</label>
              <input type="date" name="start_date" class="form-control DateQuery" value=" @if(!is_null($start))

              {{$start}}

              @endif">
            </div>
            <div class="col-md">
              <label class="col-form-label">Date  To</label>
              <input type="date" name="end_date" class="form-control DateQuery" value="
              @if(!is_null($end))

              {{$end}}

              @endif
              ">
            </div>

             <div class="col-md mt-5">
             <input type="submit" name="" value="Search" class="btn btn-danger btn-sm jesus">
            </div>
          </div>

      </form>
     <!--   <div class="card jesus bg-primary text-light">
      <div class="card-body">
        <p class="text-light font-size-40 font-weight-bold mb-2">
       {{$TelecomTotal}} (UGX)   <i class=" fa fa-money fa-2x float-right"></i>
        </p>
        <p class="text-uppercase text-light mb-3">
          Total Revenue from Telecom Commissions
        </p>

    </div>
    </div>-->
        <div class="mb-5">
          <table class="table ters table-hover ">
            <thead>
              <tr>

                <th class="btn-danger">TID</th>
              <th>Outlet </th>
              <th>Wallet </th>
              <th>Client </th>
              <th>Agent </th>
                <th class="btn-danger">Type</th>
              <th>Date</th>
              <th>Amount</th>
                <th class="btn-dark">Running Balance</th>
                <th class="btn-danger"> Commission</th>


              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>

                 <td class="bg-dark jesus text-light">{{$Telecom->TransID}}</td>
                <td  class="">{{$Telecom->OutletName}}</td>
                <td  class="">{{$Telecom->TelecomName}}</td>
                <td  class="">{{$Telecom->Reciever_Phone}}</td>
                <td  class="bg-primary text-light">{{$Telecom->AgentPhoneNumber}}</td>
                <td  class="btn-dark">{{$Telecom->Transaction_Type}}</td>
                <td  class="">                                                             <?php echo date("d/M/y, H:i:s", strtotime($Telecom->created_at)); ?></td>
                <td  class=""><?php echo number_format($Telecom->amount); ?> (UGX)</td>
                <td  class="">
                  @if($Telecom->Transaction_Type == "Deposit")

                  <i class="fa fa-minus fa-1x" style="color: red !important"></i>

                   @endif


                    @if($Telecom->Transaction_Type == "Withdraw")

                  <i class="fa fa-plus fa-1x" style="color: red !important"></i>

                   @endif


                  <?php echo number_format($Telecom->Running_Balance); ?> (UGX)</td>
                <td  class="bg-dark jesus text-light">

                  @if($Telecom->Deposit_Commission>0)

                  <?php echo number_format($Telecom->Deposit_Commission); ?>

                   @endif

                   @if($Telecom->Withdraw_Commission>0)

                  <?php echo number_format($Telecom->Withdraw_Commission); ?>

                   @endif

                     (UGX)</td>
              </tr>


              @endforeach
              @endif

            </tbody>
                <tr>

               <th class="btn-danger">TID</th>
              <th>Outlet </th>
              <th>Wallet </th>
              <th>Client </th>
              <th>Agent </th>
               <th class="btn-danger">Type</th>
              <th>Date</th>
              <th>Amount</th>
                <th class="btn-dark">Running Balance</th>
                <th class="btn-danger"> Commission</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>





@include('banks.ManageBankTransactions')




