<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>Agent User Request Telecom Float  to the Agent <b style="color: red">{{Auth::user()->Agency_name}}</b></strong>
        </h4>
       

     @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

      <form method="POST" action="{{route('TelecomAgentFloatRequest')}}" class="form" class="">
      <div class="form-row">
         @csrf
        <div class="form-group col-md">
          <label for="aaaaa">Float Amount</label>
          <input type="text"     required  class="form-control"  name="Amount" />
        </div>



         <div class="form-group col-md">
          <label for="ss">Approving Agent  </label>
          <input type="text"  readonly   required  class="form-control" value="{{Auth::user()->Agency_name}}" />
        </div>



          <div class="form-group col-md">
              <label for="ss">Telecom Carrier  (Approved By Parent Agent) </label>
        <select class="form-control" name="TelecomID">
              
              <option value="">Choose Telecom Carrier</option>

              @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <option value="{{$Telecom->id}}">{{$Telecom->TelecomName}} ({{$Telecom->PhoneNumber}})</option>
              
              @endforeach 
              @endif       
            </select >
          </div>





      </div>
     

      
      <div class="form-row">
        
        <div class="form-group col-md">
          <input type="submit"  class="btn jesus btn-dark" value="Request Float" />
        </div>
        
      </div> 


  </form>
</div>
</div>
</div>
</div>
