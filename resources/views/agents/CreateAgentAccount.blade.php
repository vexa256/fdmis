<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>Agent account set up wizard, Attach an agent account to the agency <b style="color: red">{{$Agency->Agency_name}}</b></strong>
        </h4>
        <div class="mb-5">
          <div id="example-numbers" class="wizard Wizard wizard-numbers">
            <h3>
              <span class="wizard-steps-title">Agent Account information </span>
            </h3>
            <section class="text-center">
              <h3 class="d-none">Title</h3>
        


      <form method="POST" action="{{route('CreateAgentAccounts')}}" class="form" class="">
      <div class="form-row">
         @csrf
        <div class="form-group col-md">
          <label for="aaaaa">Agent Name</label>
          <input type="text"     required  class="form-control"  name="AgentName" />

          <input type="hidden" name="Agency_id" value="{{$Agency->Agency_id}}">
          <input type="hidden" name="Agency_name" value="{{$Agency->Agency_name}}">
         


        </div>
         <div class="form-group col-md">
          <label for="ss">Agent Email  </label>
          <input type="email"    required  class="form-control" name="email" />
        </div>
      </div>
      <div class="form-row">
        
        <div class="form-group col-md">
          <label for="aaaaa">New Account Password</label>
          <input type="text"    required  class="form-control"  name="Password" />
        </div>
         <div class="form-group col-md">
          <label for="ss">Agent Phone </label>
          <input type="text"    required  class="form-control" name="Phone" />
        </div>
      </div>


      <div class="form-row">
        
        <div class="form-group col-md">
          <label for="aaaaa">Opening Float</label>
          <input type="text"    required  class="form-control"  name="Agent_Float" />
        </div>
         <div class="form-group col-md">
          <label for="ss">Agent Location </label>
          <input type="text"    required  class="form-control" name="loc" />
        </div>
      </div> 


      <div class="form-row">
        
        <div class="form-group col-md">
          <input type="submit"  class="btn jesus btn-dark" value="Create Agent" />
        </div>
        
      </div> 


    </section>
            
          </div>
           </form>
        </div>
      </div>
    </div>
  </div>
</div>

