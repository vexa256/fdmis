<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Before creating an agent account, Please select the parent agency account</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Agency Name</th>
                <th>Username</th>
                <th>Location</th>
                <th>Opening Float</th>
                <th>Phone</th>
                <th>Attach Agent</th>
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Agencies))

              @foreach($Agencies as $Agency)
              <tr>
                <td>{{$Agency->Agency_name}}</td>
                <td>{{$Agency->email}}</td>
                <td>{{$Agency->Agency_loc}}</td>
                <td>{{$Agency->Agency_Float}}</td>
                <td>{{$Agency->agency_phone}}</td>
                
              
                <td><a href="{{route('CreateAgentAccount', ['id' => $Agency->id])}}" disabled class="btn btn-danger jesus ">Attach Agent to agent</a></td>
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
               <th>Agency Name</th>
                <th>Username</th>
                <th>Location</th>
                <th>Opening Float</th>
                <th>Phone</th>
                <th>Attach Agent</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
