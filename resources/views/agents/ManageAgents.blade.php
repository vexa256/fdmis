<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>This interface allows you manage all agent accounts in the system, Use delete with caution</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" id="example1">
            <thead>
              <tr>
                <th>Parent Agent</th>
                <th>Name</th>
                <th>Location</th>
               
                <th>Phone</th>
                <th>Username</th>
                <th>Update</th>
                <th>Delete</th>
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Agents))

              @foreach($Agents as $Agent)
              <tr>
                <td>{{$Agent->Agency_name}}</td>
                <td>{{$Agent->name}}</td>
                <td>{{$Agent->Agent_loc}}</td>
              
                <td>{{$Agent->agent_phone}}</td>
                <td>{{$Agent->email}}</td>
                <td><a href="#" disabled class="btn btn-dark jesus ">Update</a></td>

                <td><a href="{{route('DeleteAgentAccount', ['id' => $Agent->id])}}" disabled class="btn btn-danger jesus ">Delete</a></td>
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
              <th>Parent Agent</th>
                <th>Name</th>
                <th>Location</th>
              
                <th>Phone</th>
                <th>Username</th>
                <th>Update</th>
                <th>Delete</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
