<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Manage all collectors in the system, Use delete with caution</strong>

    </h4>

    <div class="row">
       <button data-toggle="modal" data-target="#CreateNewCollectorAccount" class="ml-3 mb-5  btn-danger btn jesus">

        <i class="fa fa-user-plus"></i>
       Create Collector</button>



       <button data-toggle="modal" data-target="#CreateNewCollectorAccount" class="ml-3 mb-5  btn-dark btn jesus">

        <i class="fa fa-cogs"></i>
      Assign Duty</button>



        <button data-toggle="modal" data-target="#CreateNewCollectorAccount" class="ml-3 mb-5  btn-primary btn jesus">

        <i class="fa fa-info"></i>
       Not Assigned</button>

      <div class="col-lg-12">

        @if (count($errors) > 0)
          <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.
          <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
          </ul>
          </div>
        @endif

        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th>Name</th>
                <th>ID</th>
                <th> Photo</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Date Created</th>
                <th>Address</th>
                <th>Delete </th>
                <th>Update </th>
                <th class="btn-danger">Assigned to </th>
                <th class="btn-dark">Assigned Duty</th>
               
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Collectors))

              @foreach($Collectors as $Data)
              <tr>
                <td>{{$Data->name}}</td>
                <td><a data-fancybox="gallery"  href="{{ url(''.$Data->NationalIDScan) }}" class="btn btn-danger jesus">
                    
                    <i class="fa fa-binoculars"></i>

                    

                </a></td>

            <td><a data-fancybox="gallery"  href="{{ url(''.$Data->ProfileImageScan) }}" class="btn btn-danger jesus">
                    
                    <i class="fa fa-binoculars"></i>

                    
          <input type="text" value="{{url('/')}}" id="url" name="url" style="display: none !important">



                </a></td>


                 <td>{{$Data->CollectorsPhone}}</td>
                 <td>{{$Data->email}}</td>
                 <td>{{$Data->created_at->format('d-M-Y')}}</td>
                 <td>{{$Data->CollectorAddress}}</td>
               
                <td><a data-id="{{$Data->id}}" href="#" class="btn btn-danger jesus trash"> <i class="fa fa-trash"></i>
                </a></td>

                <td><a href="#UpdateCollectorsAccount{{$Data->id}}" data-toggle="modal" class="btn btn-primary jesus ">

                  <i class="fa fa-edit"></i> 
                </a></td> 


                  <td class="bg-dark text-light">{{$Data->Assigned_Count}}</td> 
                  <td class="">

                  <a href="#AssignCollectingAgents"  data-toggle="modal" class="btn btn-dark jesus AssignCollectingAgents" data-id="{{$Data->id}}">

                  <i class="fa fa-plus"></i> 
                </a>

                  </td> 

               
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>
</div>



@include('collections.AddCollectorsModal')
@include('collections.UpdateCollectorsModal')
@include('collections.AssignAgentModal')
@include('collections.CreateTriggerAlarm')