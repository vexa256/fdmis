
 <div class="modal fade" id="CreateNewCollectorAccount" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg " role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Create a new collectors account </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" style="padding-top: 5% !important; padding-bottom: 10% !important"> <div class="container-fluid">
                    
                  
                   <form method="POST" action="{{ route('CreateCollectorsAccount') }}"  enctype="multipart/form-data">
                         @csrf
                            <div class="row">
                              <div class="col-6">
                                  
                               <input type="text" name="name" placeholder="Full Name" class="form-control">

                              </div>



                              <div class="col-6">
                                  
                               <input type="text" name="phone" placeholder="Phone Number" class="form-control">

                              </div>

                            </div>
                             <div class="row pt-xl-4 ">

                              <div class="col-6">
                                 <label class="col-form-label">National ID Scan</label> 
                               <input type="file" name="NationalIDScan" placeholder="" class="form-control">

                              </div>

                              <div class="col-6">
                                   <label class="col-form-label">Profile Image Scan</label>   
                               <input type="file" name="ProfileImageScan" placeholder="" class="form-control">

                              </div>
                            </div>

                             <div class="row pt-xl-4 ">

                              <div class="col-6">
                                  
                               <input type="email" name="email" placeholder="Email/Username" class="form-control">

                              </div>


                              <div class="col-6">
                                  
                               <input type="text" name="CollectorAddress" placeholder="Physical Address" class="form-control">

                              </div>



                             </div>


                             <div class="row pt-xl-4 ">

                              <div class="col-6">
                                  
                               <input type="password" name="password" placeholder="Password" class="form-control">

                              </div>


                              <div class="col-6">
                                  
                               <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control">

                              </div>



                             </div>
                      


                      





                    

                </div>
                 </div>
                <div class="modal-footer" >
                 
                  <button type="submit" class="btn btn-primary">Create</button>

                  
                </div>
                 </form>
            </div>
    </div>
  </div>
  