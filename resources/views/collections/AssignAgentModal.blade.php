   
  




 <div class="modal fade" id="AssignCollectingAgents" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-xl " role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">Assign collector  agent user </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pt-2" style="width: 100% !important; overflow-x: scroll !important"> <div class="container-fluid">
                     
                        <h4 class=" mb-5 mt-3">
                          <strong>Select Agent User to be assigned a collector and set collection triggers</strong>

                        </h4>
                   
                  


                       <div class="mt-3">


                         <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap" >
            <thead>
              <tr>
                              <th>Agent</th>
                              <th>Collector</th>
                              <th>Email</th>
                              <th>Outlet</th>
                              <th>Parent Agent</th>
                              <th class="bg-danger text-light">Assigned Status</th>
                              <th>Assign Collector</th>
                              <th>Update Collector</th>
              </tr>
            </thead>
            <tbody>
               @if(!is_null($Agents)) @foreach($Agents as $Data)
                          <tr>
                            
                            <td>{{$Data->name}}</td>

                             @if($Data->CollectorsName != null || $Data->CollectorsName != '')

                                   <td>{{$Data->CollectorsName}}</td>


                              @endif 


                               @if($Data->CollectorsName == null || $Data->CollectorsName == '')

                                   <td>NULL</td>


                              @endif 
                         

                            <td>{{$Data->email}}</td>
                            <td>{{$Data->Outlet_Name}}</td>
                            <td>{{$Data->Agency_name}}</td>
                            <td class="bg-dark text-light">
                              
                              @if($Data->CollectorsID == null || $Data->CollectorsID == '')

                                {{'No collector asssigned'}}

                              @endif 

                                @if($Data->CollectorsID != null || $Data->CollectorsID != '')

                                {{' Asssigned collector'}}

                              @endif 
 

                            </td>
                            <td>

                               @if($Data->CollectorsID == null || $Data->CollectorsID == '')

                            <a href="#AssignCollectingTriggers" class="btn btn-danger jesus assigned " data-id="{{$Data->id}}"

                              data-toggle="modal" data-dismiss="modal">


                                         <i class="fa fa-plus"></i>

                                     </a>


                                @endif

                            </td>
                           
                            <td>

                      
                                  @if($Data->CollectorsID != null || $Data->CollectorsID != '')

                                   <a href="#" class="btn btn-danger jesus ">


                                         <i class="fa fa-edit"></i>

                                     </a>


                                    @endif
                                
                               

                            </td>
                            

                          </tr>


                           @endforeach 
                            @endif       
             
              
            </tbody>
            <tfoot>
              <tr>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Outlet</th>
                              <th>Parent Agent</th>
                              <th>Assigned Status</th>
                              <th>Assign Collector</th>
                              <th>Update Collector</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div></div></div> </div> </div> </div> </div>


             