
               @if(!is_null($Collectors))

              @foreach($Collectors as $Data)
 <div class="modal fade" id="UpdateCollectorsAccount{{$Data->id}}" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-lg " role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">Update collectors account for the user <strong>{{$Data->name}}</strong> </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" style="padding-top: 5% !important; padding-bottom: 10% !important"> <div class="container-fluid">
                    
                  
                   <form method="POST" action="{{ route('UpdateCollectorsAccount') }}"  enctype="multipart/form-data">
                         @csrf
                            <div class="row">
                              <div class="col-6">
                                        <label class="col-form-label">Full Name</label> 
                               <input type="text" name="name" value="{{$Data->name}}" class="form-control">

                              </div>

                              <input type="hidden" name='id' value="{{$Data->id}}">

                              <div class="col-6">
                                         <label class="col-form-label">Phone</label>  
                               <input type="text" name="phone" value="{{$Data->CollectorsPhone}}" class="form-control">

                              </div>

                            </div>
                          

                            <div class="row pt-xl-4 ">

                              <div class="col-6">
                                 <label class="col-form-label">National ID Scan</label> 
                               <input type="file" name="NationalIDScan" placeholder="" class="form-control">

                              </div>

                              <div class="col-6">
                                   <label class="col-form-label">Profile Image Scan</label>   
                               <input type="file" name="ProfileImageScan" placeholder="" class="form-control">

                              </div>
                            </div>




                             <div class="row pt-xl-4 ">

                              <div class="col-6">
                                       <label class="col-form-label">Email/Username</label> 
                               <input type="email" name="email" value="{{$Data->email}}" class="form-control">

                              </div>


                              <div class="col-6">
                                   <label class="col-form-label">Physical Addresss</label> 
                                  
                  <input type="text" name="CollectorAddress" value="{{$Data->CollectorAddress}}" class="form-control">

                              </div>



                             </div>


                             <div class="row pt-xl-4 ">

                              <div class="col-6">
                                     <label class="col-form-label">Password</label> 
                               <input type="password" name="password"  value="{{$Data->password}}"  class="form-control">

                              </div>


                              <div class="col-6">
                                          <label class="col-form-label">Confirm Password</label> 
                               <input type="password" name="password_confirmation"  value="{{$Data->password}}"  class="form-control">

                              </div>



                             </div>
                      


                  <button type="submit" class="btn btn-primary mt-3 float-lg-right">Create</button>

            
                </div>
                 </div>
              
                 
                 </form>
          </div>
        </div>
      </div>
             @endforeach 
              @endif       