<div class="modal fade" id="AssignCollectingTriggers" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">One More step , Assigns Collection Triggers </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pt-2" > <div class="container-fluid">

                	    <h5 class=" mb-3 mt-3">
                          <strong>Assign a cash at hand vlaue that will trigger a collection from this Agent User <strong>

                        </h5>


                        <div class="card-body mb-3 ">
                        	
                        	<h6>
                        		<i class="fa fa-info fa-2x"></i>
                        	You can choose to fill in all the bank and telecom deposit details or one depending on the applicable situation.</h6>

                        </div>
                   

                		 <form method="POST" action="{{ route('AssignCollectingAgents') }}" >
                         @csrf
                            <div class="row">
                              <div class="col-12">
                                  
                               <input type="text" name="TriggerAmount" placeholder="Trigger Amount" class="form-control">

                              </div>

                              <div class="col-6 mt-4">
                              
                                  <select class="form-control" name="Telecom">
                                  		
                                  		<option value="">Choose Telecom</option>

                                  		 @if(!is_null($Telecoms))

                                         @foreach($Telecoms as $Data)

                               <option value="{{$Data->id}}">{{$Data->Telecom_Name}}</option>


                                          @endforeach 
             								 @endif       
             

                                  </select>
	                              
                              </div>


                               <div class="col-6 mt-4">
                               
                                  <select class="form-control" name="Bank">
                                  		
                                  		<option value="">Choose Bank</option>

                                  		 @if(!is_null($Banks))

                                         @foreach($Banks as $Data)

                               <option value="{{$Data->id}}">{{$Data->BankName}}</option>


                                         @endforeach 
              								@endif       
             

                                  </select>
	                              
                              </div>




                              <div class="col-6 mt-4">
                                  
                               <input type="text" name="TelecomNumber" placeholder="Telecom Deposit Account" class="form-control">

                              </div>


                               <div class="col-6 mt-4">
                                  
                               <input type="text" name="BankAccountNumber" placeholder="Bank Account Deposit Number" class="form-control">

                              </div>



                               <input type="text" name="AgentID"  class="AgentUserID invisible">
                               <input type="text" name="CollectingAgentID"   class="CollectorID  invisible">

                          </div>


                        <input type="submit" class="float-lg-left btn btn-danger jesus" value="Assign Collector">

                      <a href="#AssignCollectingAgents" data-dismiss="modal" data-toggle="modal" class="btn btn-dark float-lg-right">Cancel</a>

                      </form>








                </div>
            </div>
        </div>
    </div>
</div>

                     




<div class="modal fade" id="AssignCollectingTriggers" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">One More step , Assigns Collection Triggers </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pt-2" > <div class="container-fluid">

                	    <h5 class=" mb-3 mt-3">
                          <strong>Assign a cash at hand vlaue that will trigger a collection from this Agent User <strong>

                        </h5>


                        <div class="card-body mb-3 ">
                        	
                        	<h6>
                        		<i class="fa fa-info fa-2x"></i>
                        	You can choose to fill in all the bank and telecom deposit details or one depending on the applicable situation.</h6>

                        </div>
                   

                		 <form method="POST" action="{{ route('AssignCollectingAgents') }}" >
                         @csrf
                            <div class="row">
                              <div class="col-12">
                                  
                               <input type="text" name="TriggerAmount" placeholder="Trigger Amount" class="form-control">

                              </div>

                              <div class="col-6 mt-4">
                              
                                  <select class="form-control" name="Telecom">
                                  		
                                  		<option value="">Choose Telecom</option>

                                  		 @if(!is_null($Telecoms))

                                         @foreach($Telecoms as $Data)

                               <option value="{{$Data->id}}">{{$Data->Telecom_Name}}</option>


                                          @endforeach 
             								 @endif       
             

                                  </select>
	                              
                              </div>


                               <div class="col-6 mt-4">
                               
                                  <select class="form-control" name="Bank">
                                  		
                                  		<option value="">Choose Bank</option>

                                  		 @if(!is_null($Banks))

                                         @foreach($Banks as $Data)

                               <option value="{{$Data->id}}">{{$Data->BankName}}</option>


                                         @endforeach 
              								@endif       
             

                                  </select>
	                              
                              </div>




                              <div class="col-6 mt-4">
                                  
                               <input type="text" name="TelecomNumber" placeholder="Telecom Deposit Account" class="form-control">

                              </div>


                               <div class="col-6 mt-4">
                                  
                               <input type="text" name="BankAccountNumber" placeholder="Bank Account Deposit Number" class="form-control">

                              </div>



                               <input type="text" name="AgentID"  class="AgentUserID invisible">
                               <input type="text" name="CollectingAgentID"   class="CollectorID  invisible">

                          </div>


                        <input type="submit" class="float-lg-left btn btn-danger jesus" value="Assign Collector">

                      <a href="#AssignCollectingAgents" data-dismiss="modal" data-toggle="modal" class="btn btn-dark float-lg-right">Cancel</a>

                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

          








