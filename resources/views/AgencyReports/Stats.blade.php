
<div class="row">
  <div class="col-md">
    <div class="card">
      <div class="card-body jesus bg-primary text-light">
        <div class="d-flex flex-wrap  align-items-center">
            <i class="fa fa-home fa-3x float-right p-4"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
             Total Outlets
            </p>
            <p class="text-gray-5 mb-0">
              All your child Outlets
            </p>
          </div>
          <p class="text-success font-weight-bold font-size-24 mb-0">
          {{$Outlets}}
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md">
    <div class="card bg-dark text-light">
      <div class="card-body jesus">
        <div class="d-flex flex-wrap align-items-center">
          <i class="fa fa-users fa-3x float-right p-4"></i>
          <div class="mr-auto">
            <p class="text-uppercase text-light font-weight-bold mb-1">
              Total Agent User Accounts 
            </p>
            <p class="text-gray-5 mb-0">
           All Child agent users
            </p>
          </div>
          <p class="text-primary font-weight-bold font-size-24 mb-0">
          {{$AgentUsers}}
          </p>
        </div>
      </div>
    </div>
  </div>
</div>