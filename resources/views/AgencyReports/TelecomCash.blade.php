<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
     <strong>Wallet Cash at hand Records for all your outlets</strong>
   </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="card jesus bg-dark text-light">
      <div class="card-body">
        <p class="text-light font-size-40 font-weight-bold mb-2">
       {{$Total}} (UGX)   <i class=" fa fa-money fa-2x float-right"></i>
        </p>
        <p class="text-uppercase text-light mb-3">
          Total Cash At Hand from all you agent Wallet accounts
        </p>

    </div>
    </div>
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>

               <th>Outlet Name</th>
                <th>Parent Agent Name</th>
                <th> Wallet Name</th>
                <th> Agent Phone Number</th>
                <th >Amount</th>
                <th >Mark as Picked</th>

              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>

                 <td  class="bg-dark jesus text-light">{{$Telecom->OutletName}}</td>
                <td  class="bg-dark jesus text-light">{{$Telecom->AgencyName}}</td>
                <td  class="bg-dark jesus text-light">{{$Telecom->TelecomName}}</td>
                <td  class="bg-dark jesus text-light">{{$Telecom->TelecomPhoneNumber}}</td>
                <td  class="bg-secondary jesus text-light"><?php echo number_format($Telecom->amount); ?> (UGX)</td>
                <td  class="">

                  <a href="#" class="btn btn-sm btn-danger">Mark as Picked</a>
                </td>



              </tr>


              @endforeach
              @endif

            </tbody>
               <tr>


              <th>Outlet Name</th>
                <th>Parent Agent Name</th>
                <th>Source Wallet Name</th>
                <th > Source Wallet Account</th>
                <th >Amount</th>
                <th >Mark as Picked</th>

              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
