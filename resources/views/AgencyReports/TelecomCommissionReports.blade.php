<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong> Wallet Commission Records for all your outlets

      </strong>
    </h4>

    <div class="row">
      <div class="col-lg-12">
        <div class="card jesus bg-dark text-light">
      <div class="card-body">
        <p class="text-light font-size-40 font-weight-bold mb-2">
       {{$Total}} (UGX)   <i class=" fa fa-money fa-2x float-right"></i>
        </p>
        <p class="text-uppercase text-light mb-3">
          Total Commission from all your agent Wallet accounts
        </p>

    </div>
    </div>
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>

               <th>Outlet Name</th>
                <th>Parent Agent</th>
                <th>Agent Wallet Name</th>
                <th> Agent Wallet Number</th>
                <th class="btn-danger">Deposit Commission</th>
                <th class="btn-danger">Withraw Commission</th>

              </tr>
            </thead>
            <tbody>


               @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)
              <tr>

                 <td  class="bg-dark jesus text-light">{{$Telecom->OutletName}}</td>
                <td  class="bg-dark jesus text-light">{{$Telecom->AgencyName}}</td>
                <td  class="bg-dark jesus text-light">{{$Telecom->TelecomName}}</td>
                <td  class="bg-dark jesus text-light">{{$Telecom->PhoneNumber}}</td>
                <td  class="bg-dark jesus text-light"><?php echo number_format($Telecom->DepositCommission); ?> (UGX)</td>
                <td  class="bg-dark jesus text-light"><?php echo number_format($Telecom->WithdrawCommission); ?> (UGX)</td>
              </tr>


              @endforeach
              @endif

            </tbody>
               <th>Outlet Name</th>
                <th>Parent Agent</th>
                <th>Agent Wallet Name</th>
                <th> Agent Wallet Number</th>
               <th class="btn-danger">Deposit Commission</th>
                <th class="btn-danger">Withraw Commission</th>

              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>