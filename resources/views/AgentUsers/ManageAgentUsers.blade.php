<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Manage your Child Agent-User Accounts</strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th>Agent User Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Parent Agent</th>
                <th class="btn-danger jesus">Outlet</th>
                <th>Date Created</th>
                <th>Assign Outlet</th>
                <th>Delete</th>
                <th>Update</th>
               
              </tr>
            </thead>
            <tbody>


               @if(!is_null($Agents))

              @foreach($Agents as $Agent)
              <tr>
                <td>{{$Agent->name}}</td>
                <td>{{$Agent->email}}</td>
                <td>{{$Agent->agent_phone}}</td>
                <td>{{$Agent->Agency_name}}</td>
                <td class="btn-dark jesus">{{$Agent->Outlet_Name}}</td>
                <td>{{$Agent->created_at->format('d-M-Y')}}</td>
                <td><a href="#daf{{$Agent->id}}" data-toggle="modal" class="btn btn-sm btn-danger jesus ">Assign Outlet</a></td> 
                <td><a href="" class="btn btn-sm btn-dark jesus ">Delete</a></td> 
                <td><a href="" class="btn btn-sm btn-primary jesus ">Update</a></td> 
                
              </tr>


              @endforeach 
              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Agent User Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Parent Agent</th>
                <th class="btn-danger jesus">Outlet</th>
                <th>Date Created</th>
                <th>Assign Outlet</th>
                <th>Delete</th>
                <th>Update</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


 @if(!is_null($Agents))

              @foreach($Agents as $Agent)

 <div class="modal fade" id="daf{{$Agent->id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Assign Households</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                 


                  <div class="container-fluid">
                    
                  
                      <form method="POST" action="{{route('AssignAgentToOutlet')}}">
                         @csrf
                            <div class="col-md">

                              <input type="hidden" name="ID" value="{{$Agent->id}}">
                               
                               <select class="form-control"  name="OutletsID" >

                                  <option>Choose Outlet</option>
                                 
                                  @if(!is_null($Outlets))

                                  @foreach($Outlets as $Outlet)
                              

                                   <option value="{{$Outlet->id}}">{{$Outlet->outlet_name}}</option>


                                @endforeach 

                                @endif       


                               </select>
                              
                            </div>
                        
                     



                  </div>




                </div>
                <div class="modal-footer">
                 
                  <button type="submit" class="btn btn-primary">Save changes</button>

                   </form>
                </div>
              </div>
            </div>
          </div> @endforeach 
              @endif       
             