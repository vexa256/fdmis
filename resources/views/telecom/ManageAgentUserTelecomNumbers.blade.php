<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Assign Outlet merchant phone number account</strong> <a href="#daf" class="float-right btn btn-dark btn-sm jesus" data-toggle="modal">Attach Phone Number</a>
    </h4>
 @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th>Agency Name</th>
                <th class="bg-danger text-light">Outlet Name</th>
                <th>Wallet Name</th>

                <th class="bg-dark text-light">Phone Number</th>
                <th>Date Created</th>
                <th>Delete</th>



              </tr>
            </thead>
            <tbody>


               @if(!is_null($Agents))

              @foreach($Agents as $Agent)
              <tr>
                <td>{{$Agent->AgencyName}}</td>
                <td class="bg-danger text-light">{{$Agent->OutletName}}</td>
                <td>{{$Agent->TelecomName}}</td>
                <td class="bg-dark text-light">{{$Agent->PhoneNumber}}</td>
                <td>{{$Agent->created_at->format('d-M-Y')}}</td>
                <td><a href="{{route('DeleteTelecomOutletNumber', ['id' => $Agent->id])}}" class="btn btn-sm btn-danger jesus ">Delete</a></td>



              </tr>


              @endforeach
              @endif

            </tbody>
              <tr>
                <th>Agency Name</th>
                   <th class="bg-danger text-light">Outlet Name</th>
                <th>Wallet Name</th>
                <th class="bg-dark text-light">Phone Number</th>
                <th>Date Created</th>
                <th>Delete</th>



              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


 <div class="modal fade" id="daf" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Assign outlet merchant phone number account </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" style="padding-top: 5% !important; padding-bottom: 10% !important">



                  <div class="container-fluid">


                      <form method="POST" action="{{route('SubmitTelecomNumberAccount')}}">
                         @csrf
                            <div class="row">
                            <div class="col-md">

                              <input type="hidden" name="ID" value="">

                              <label>Choose Wallet </label>

                               <select class="form-control"  name="telecomID" >

                                  <option value="">Choose Wallet</option>

                                  @if(!is_null($Telecoms))

                                  @foreach($Telecoms as $Telecom)


                                   <option value="{{$Telecom->id}}">{{$Telecom->Telecom_Name}}</option>


                                @endforeach

                                @endif


                               </select>

                            </div>

                             <div class="col-md">



                              <label>Choose Outlet</label>

                               <select class="form-control"  name="OutletID" >

                                  <option value="">Choose Outlets</option>

                                  @if(!is_null($Outlets))

                                  @foreach($Outlets as $Outlet)

                                   <option value="{{$Outlet->id}}">{{$Outlet->outlet_name}}</option>


                                @endforeach

                                @endif


                               </select>

                            </div>

                            <div class="col-md" >
                               <label>Assign Outlet Phone Number</label>
                               <input type="text" name="OutletPhoneNumber" class="form-control">


                            </div>
                          </div>




                </div>
                <div class="modal-footer" >

                  <button type="submit" class="btn btn-primary">Save changes</button>

                   </form>
                </div>
              </div>
            </div>
          </div>
