
<style type="text/css"> .actions{display: none !important; } </style> <div class="card"> <div class="card-body"> <div class="row"> <div class="col-lg-12">
        <h4 class="mb-5"><strong>Fill in all the form Fields to complete transaction</strong></h4>



      <form method="POST" action="{{route('TelecomWithdrawLogic')}}" class="form" class="mt-5">

      @if ($errors->any())
    <div class="alert alert-danger jesus">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

      <div class="form-row">
         @csrf
        <div class="form-group col-md">
            <label for="aaaaa">Withdraw Amount</label>
         <input type="text" name="TotalWithdraw" class="form-control">
        </div>


        <div class="form-group col-md">
          <label for="aaaaa">Client Phone Number</label>
         <input type="text" class="form-control" name="ClientPhone">
        </div>

        <div class="form-group col-md">
          <label for="ss">Wallet Carrier</label>
        <select class="form-control " name="TelecomCarrier">

        	<option value="">Choose Wallet Carrier</option>
         		 @if(!is_null($Telecoms))

              @foreach($Telecoms as $Telecom)

         		<option value="{{$Telecom->id}}">{{$Telecom->TelecomName}} ({{$Telecom->PhoneNumber}})</option>



         		 @endforeach
              @endif


         </select> </div> </div> <div class="form-group"> <input type="submit" class="btn btn-dark jesus" value="Approve Transaction " /> </div> </section> </form> </div> </div> </div> </div>
