<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="mb-4">
          <strong>Outlet Creation</strong>
        </h4>
        <div class="mb-5">
          <div  class="wizard Wizard">
            <section class="text-center">
              <h3 class="d-none"></h3>
            

                    <div class="card jesus">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>Please fill in all the fields</strong>
    </h4>
    <form method="POST" action="{{route('CreateOutletLogic')}}"  class="">
      <div class="form-row">
        <div class="form-group col-md">
          <label for="inputEmail4">Outlet Name</label>
          <input type="text" required name="outlet_name" class="form-control"  />
        </div>
        @csrf
      
        <div class="form-group col-md">
          <label for="inputEmail4">Outlet Location</label>
          <input type="text" required name="Agency_location" class="form-control"  />
        </div>
      
      </div>
     
      <div class="form-group">
         
          <input type="submit" class="btn btn-dark jesus" value="Create Outlet " />
        </div>
    </form>
  </div>
</div> 
</section>
            
          </div>
        </div>
      </div>
    
    </div>
  </div>
</div>
