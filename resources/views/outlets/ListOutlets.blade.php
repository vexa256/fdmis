<div class="card">
  <div class="card-body">
    <h4 class="mb-4">
      <strong>List of  all Outlets  attched to the agent  <span style="color: red !important">{{Auth::user()->name}}</span></strong>
    </h4>
    <div class="row">
      <div class="col-lg-12">
        <div class="mb-5">
          <table class="table ters table-hover nowrap">
            <thead>
              <tr>
                <th>Outlet Name</th>
                <th>Outlets Location</th>
                <th>Agency Name</th>
                <th>Date Created</th>
                <th>Delete</th>
                <th>Update</th>
               
              </tr>
            </thead>
            <tbody>


              @if(!is_null($Outlets))

              @foreach($Outlets as $Outlet)
              <tr>

                <td>{{$Outlet->outlet_name}}</td>
                <td>{{$Outlet->Agency_location}}</td>
                <td>{{$Outlet->AgencyName}}</td>
                <td>{{$Outlet->created_at->format('d-M-Y')}}</td>
                <td><a href="{{route('deleteOutlet', ['id' => $Outlet->id])}}" class="btn btn-sm btn-danger jesus ">Delete</a></td> 
                <td><a href="" class="btn btn-sm btn-primary jesus ">Update</a></td> 
                
              </tr>


              @endforeach 

              @endif       
             
            </tbody>
            <tfoot>
              <tr>
                <th>Outlet Name</th>
                <th>Outlets Location</th>
                <th>Agency Name</th>
                <th>Date Created</th>
                <th>Delete</th>
                <th>Update</th>
               
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
