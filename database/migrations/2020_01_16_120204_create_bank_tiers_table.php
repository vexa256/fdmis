<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankTiersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bank_tiers', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('BankName')->nullable();
      $table->string('BankID')->nullable();
      $table->bigInteger('range_to')->nullable();
      $table->bigInteger('range_from')->nullable();
      $table->bigInteger('commission')->nullable();
      $table->string('status')->nullable();
      $table->bigInteger('DepositCharge')->nullable();
      $table->bigInteger('WithdrawCharge')->nullable();
      $table->bigInteger('WithdrawCommission')->nullable();
      $table->bigInteger('DepositCommission')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('bank_tiers');
  }
}
