<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('collections', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('CollectorsName');
      $table->string('User_ID');
      $table->string('Collect_User_ID');
      $table->string('CollectorsID');
      $table->string('CollectFromAgency');
      $table->string('CollectFromAgent');
      // $table->string('CollectAmount');
      $table->string('CollectFromAgencyID');
      $table->string('CollectFromAgentName');
      $table->bigInteger('CollectAmount');
      $table->string('CollectOutletID');
      $table->string('CollectOutletName');
      $table->string('CollectProfilePicture');
      $table->string('CollectSecrerCode');
      $table->bigInteger('Trigger_Amount');
      $table->bigInteger('Bank_Account_Number');
      $table->bigInteger('BankName');
      $table->bigInteger('BankID');
      $table->bigInteger('Mobile_Money_Numbwer')->defaul('NA');
      $table->string('ApprovingAuthority')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('collections');
  }
}
