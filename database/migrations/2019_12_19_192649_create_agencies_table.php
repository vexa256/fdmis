<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenciesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agencies', function (Blueprint $table)
    {
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('Location')->nullable();
      //$table->string('AgentID')->nullable();
      // $table->string('AgentName')->nullable();
      //$table->string('OutletName')->nullable();
      // $table->string('OutletID')->nullable();
      // $table->string('amount')->nullable();
      //$table->string('AccounID')->nullable();
      $table->string('MasterAccount')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agencies');
  }
}
