<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyBankFloatLogsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agency_bank_float_logs', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      //  $table->string('AgentID')->nullable();
      //$table->string('AgentName')->nullable();
      //$table->string('OutletName')->nullable();
      //$table->string('OutletID')->nullable();
      $table->bigInteger('amount')->nullable();
      $table->string('AccountID')->nullable();
      $table->string('BankID')->nullable();
      $table->string('BankBranch')->nullable();
      $table->string('BankName')->nullable();
      $table->string('status')->nullable();
      $table->bigInteger('request_amount')->nullable();
      $table->string('BankAccount')->nullable();
      $table->string('MasterAccount')->nullable();
      $table->string('transactionID')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agency_bank_float_logs');
  }
}
