<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyBankAccountsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agency_bank_accounts', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('BankAccount')->unique()->nullable();
      $table->string('BankName')->nullable();
      $table->string('BankID')->nullable();
      $table->string('status')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agency_bank_accounts');
  }
}
