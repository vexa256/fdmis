<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutletsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('outlets', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('outlet_name')->nullable();
      $table->string('outlet_id')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('AgencyID')->nullable();
      $table->string('AgentID')->nullable();
      $table->string('AgentName')->nullable();
      $table->string('outlet_lat')->nullable();
      $table->string('outlet_long')->nullable();
      $table->string('Agency_location')->nullable();
      //$table->string('outlet_name');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('outlets');
  }
}
