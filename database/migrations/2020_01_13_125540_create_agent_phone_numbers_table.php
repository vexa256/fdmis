<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentPhoneNumbersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agent_phone_numbers', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('AgencyName')->nullable();
      $table->string('AgencyID')->nullable();
      $table->string('OutletName')->nullable();
      $table->string('OutletID')->nullable();
      $table->string('TelecomName')->nullable();
      $table->string('TelecomID')->nullable();
      $table->string('PhoneNumber')->unique()->nullable();
      $table->string('status')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agent_phone_numbers');
  }
}
