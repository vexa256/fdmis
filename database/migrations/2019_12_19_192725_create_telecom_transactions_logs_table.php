<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelecomTransactionsLogsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('telecom_transactions_logs', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('TransID')->nullable();
      $table->string('status')->nullable();
      $table->bigInteger('amount')->nullable();
      $table->string('Customer_ID')->nullable();
      $table->string('Customer_Name')->nullable();
      $table->string('Transaction_Type')->nullable();
      $table->string('TelecomName')->nullable();
      $table->string('TelecomID')->nullable();
      $table->string('AgentPhoneNumber')->nullable();
      $table->bigInteger('Deposit_Commission')->nullable();
      $table->bigInteger('Withdraw_Commission')->nullable();
      $table->bigInteger('Running_Balance')->nullable();
      $table->bigInteger('Commission')->nullable();
      $table->string('Client_Phone')->nullable();
      $table->string('Reciever_Phone')->nullable();
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('AgentID')->nullable();
      $table->string('AgentName')->nullable();
      $table->string('OutletName')->nullable();
      $table->string('OutletID')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('telecom_transactions_logs');
  }
}
