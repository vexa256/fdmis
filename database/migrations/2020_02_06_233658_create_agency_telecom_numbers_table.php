<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTelecomNumbersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agency_telecom_numbers', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('PhoneNumber')->unique()->nullable();
      $table->string('TelecomName')->nullable();
      $table->string('TelecomID')->nullable();
      $table->string('status')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agency_telecom_numbers');
  }
}
