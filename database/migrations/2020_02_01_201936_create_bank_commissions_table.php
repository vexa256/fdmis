<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankCommissionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bank_commissions', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('BankID')->nullable();
      $table->string('BankName')->nullable();
      $table->bigInteger('WithdrawCommission')->nullable();
      $table->bigInteger('DepositCommission')->nullable();
      $table->string('AgencyID')->nullable();
      $table->string('BankAccount')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('AgentID')->nullable();
      $table->string('AgentName')->nullable();
      $table->string('OutletName')->nullable();
      $table->string('TransactionType')->nullable();
      $table->string('OutletID')->nullable();
      $table->bigInteger('amount')->nullable();
      $table->string('AccounID')->nullable();
      $table->string('MasterAccount')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('bank_commissions');
  }
}
