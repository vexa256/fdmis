<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankTransactionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bank_transactions', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('TransID')->nullable();
      $table->string('status')->nullable();
      $table->bigInteger('amount')->nullable();
      $table->string('Customer_ID')->nullable();
      $table->string('BankName')->nullable();
      $table->string('BankID')->nullable();
      $table->string('AgentBankAcc')->nullable();
      $table->string('Customer_Name')->nullable();
      $table->string('Transaction_Type')->nullable();
      $table->bigInteger('Deposit_Commission')->nullable();
      $table->bigInteger('Withdraw_Commission')->nullable();
      $table->bigInteger('Running_Balance')->nullable();
      $table->bigInteger('Commission')->nullable();
      $table->string('Reciever_Name')->nullable();
      $table->string('Sender_Name')->nullable();
      $table->string('Client_Acc')->nullable();
      $table->string('Reciever_Acc')->nullable();
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('AgentID')->nullable();
      $table->string('AgentName')->nullable();
      $table->string('OutletName')->nullable();
      $table->string('OutletID')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('bank_transactions');
  }
}
