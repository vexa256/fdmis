<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentBankAccountsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agent_bank_accounts', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('AgencyName')->nullable();
      $table->string('AgencyID')->nullable();
      $table->string('OutletName')->nullable();
      $table->string('OutletID')->nullable();
      $table->string('BankName')->nullable();
      $table->string('BankID')->nullable();
      $table->string('BranchName')->nullable();
      $table->string('BankAccount')->unique()->nullable();
      $table->string('status')->nullable();
      $table->timestamps();

    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agent_bank_accounts');
  }
}
