<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTelecomFloatsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('agency_telecom_floats', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('AgencyID')->nullable();
      $table->string('AgencyName')->nullable();
      $table->string('transactionID')->nullable();
      // $table->string('AgentID')->nullable();
      //  $table->string('AgentName')->nullable();
      // $table->string('OutletName')->nullable();
      $table->bigInteger('amount')->nullable();
      $table->string('OutletID')->nullable();
      $table->string('TelecomID')->nullable();
      $table->string('TelecomName')->nullable();
      $table->string('AccountID')->nullable();
      $table->string('PhoneNumber')->nullable();
      $table->string('status')->nullable();
      $table->bigInteger('request_amount')->nullable();
      $table->string('MasterAccount')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('agency_telecom_floats');
  }
}
