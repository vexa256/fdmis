<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table)
    {
      $table->bigIncrements('id');
      $table->string('name');
      $table->string('email')->unique();
      $table->timestamp('email_verified_at')->nullable();
      $table->string('password');
      $table->string('Username')->nullable();
      $table->string('Account_type')->nullable();
      $table->string('User_id')->nullable();
      $table->string('Agent_id')->nullable();
      $table->string('Agency_id')->nullable();
      $table->string('Agency_name')->nullable();
      $table->string('Agent_name')->nullable();
      $table->string('Master_account')->nullable();
      $table->string('suspend')->default('false');
      $table->string('User_role')->nullable();
      $table->string('Agency_loc')->nullable();
      $table->string('Agent_loc')->nullable();
      $table->string('Agent_Float')->nullable();
      $table->string('Agency_Float')->nullable();
      $table->string('Agency_loc_latitude')->nullable();
      $table->string('Agency_loc_longtitude')->nullable();
      $table->string('agent_loc_latitude')->nullable();
      $table->string('agent_loc_longtitude')->nullable();
      $table->string('agency_phone')->nullable();
      $table->string('agent_phone')->nullable();
      $table->string('Outlet_Name')->nullable();
      $table->string('Outlet_ID')->nullable();
      $table->string('CollectorAddress')->nullable();
      $table->string('NationalIDScan')->nullable();
      $table->string('ProfileImageScan')->nullable();
      $table->string('CollectorsSecretCode')->nullable();
      $table->string('CollectorsID')->nullable();
      $table->string('CollectorsPhone')->nullable();
      $table->bigInteger('TriggerAmount')->nullable();
      $table->string('BankName')->nullable();
      $table->string('BankAccount')->nullable();
      $table->string('TelecomName')->nullable();
      $table->string('TelecomNumber')->nullable();
      $table->string('CollectorsName')->nullable();
      $table->string('Assigned_Count')->default(0);
      $table->rememberToken();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('users');
  }
}
