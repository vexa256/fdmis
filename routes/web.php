<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();
/*************Admin Routes*********************************/

Route::get('/ManageCollectors', 'CollectionsController@ManageCollectors')->name('ManageCollectors')->middleware('auth');

Route::post('/AssignCollectingAgents', 'CollectionsController@AssignCollectingAgents')->name('AssignCollectingAgents')->middleware('auth');

Route::post('/UpdateCollectorsAccount', 'CollectionsController@UpdateCollectorsAccount')->name('UpdateCollectorsAccount')->middleware('auth');

Route::get('/DeleteCollectorsAccount/{id}', 'CollectionsController@DeleteCollectorsAccount')->name('DeleteCollectorsAccount')->middleware('auth');

Route::post('/CreateCollectorsAccount', 'CollectionsController@CreateCollectorsAccount')->name('CreateCollectorsAccount')->middleware('auth');

Route::get('/AdminVirtualCloudOffice', 'AdminController@AdminStats')->name('AdminVirtualCloudOffice')->middleware('auth');

Route::get('/ManageAdmins', 'AdminController@ManageAdmins')->name('ManageAdmins')->middleware('auth');

Route::get('/AdminCreateAccount', 'AdminController@AdminCreateAccount')->name('AdminCreateAccount')->middleware('auth');

Route::get('/DeleteAgency/{id}', 'AdminController@DeleteAgency')->name('DeleteAgency')->middleware('auth');

Route::get('/ManageAgency', 'AdminController@ManageAgency')->name('ManageAgency')->middleware('auth');

Route::post('/AddAgencyAccount', 'AdminController@AddAgencyAccount')->name('AddAgencyAccount')->middleware('auth');

Route::get('/AdminCreateAgencyAccount', 'AdminController@AdminCreateAgencyAccount')->name('AdminCreateAgencyAccount')->middleware('auth');

Route::get('/AdminBankAgencyFloat', 'AdminBankFloatController@AdminBankAgencyFloat')->name('AdminBankAgencyFloat')->middleware('auth');

Route::get('/AgencyTelecomFloat', 'AdminTelecomFloatController@AgencyTelecomFloat')->name('AgencyTelecomFloat')->middleware('auth');

Route::get('/AdminTelecomApproveAgencyFloat/{id}', 'AdminTelecomFloatController@AdminTelecomApproveAgencyFloat')->name('AdminTelecomApproveAgencyFloat')->middleware('auth');

Route::get('/AdminApproveAgencyBankFloat/{id}', 'AdminBankFloatController@AdminApproveAgencyBankFloat')->name('AdminApproveAgencyBankFloat')->middleware('auth');

/*************Admin Routes*********************************/

Route::get('/home', 'AccountController@bootstrap')->name('home')->middleware('auth');

/******************************Agency Account*****************************************/

Route::get('/AgencyHome', 'AgencyHomeController@AgencyHome')->name('AgencyHome')->middleware('auth');

Route::get('/AgencyStats', 'AgencyStatsController@AgencyStats')->name('AgencyStats')->middleware('auth');

Route::post('/AgencyBankFilterDatesFromTransactions', 'AgencyHomeController@AgencyBankFilterDatesFromTransactions')->name('AgencyBankFilterDatesFromTransactions')->middleware('auth');

Route::post('/AgencyFilterDatesFromTransactions', 'AgencyHomeController@AgencyFilterDatesFromTransactions')->name('AgencyFilterDatesFromTransactions')->middleware('auth');

Route::get('/AgencyBankAccountSetUpForm', 'AgencyAccountController@AgencyBankAccountSetUpForm')->name('AgencyBankAccountSetUpForm')->middleware('auth');

Route::post('/AddBankAccount', 'AgencyAccountController@AddBankAccount')->name('AddBankAccount')->middleware('auth');

Route::get('/AgencyTelecomAccountSetUpForm', 'AgencyAccountController@AgencyTelecomAccountSetUpForm')->name('AgencyTelecomAccountSetUpForm')->middleware('auth');

Route::post('/AddTelecomAccount', 'AgencyAccountController@AddTelecomAccount')->name('AddTelecomAccount')->middleware('auth');

Route::get('/DeleteAgencyTelecomNumber/{id}', 'AgencyAccountController@DeleteAgencyTelecomNumber')->name('DeleteAgencyTelecomNumber')->middleware('auth');

Route::get('/DeleteAgencyBankAcc/{id}', 'AgencyAccountController@DeleteAgencyBankAcc')->name('DeleteAgencyBankAcc')->middleware('auth');

Route::get('/AgencyTelecomFloatRequest', 'AgencyTelecomFloatController@AgencyTelecomFloatRequest')->name('AgencyTelecomFloatRequest')->middleware('auth');

Route::get('/AgencyFloatRequestsHistory', 'AgencyTelecomFloatController@AgencyFloatRequestsHistory')->name('AgencyFloatRequestsHistory')->middleware('auth');

Route::post('/TelecomAgencyFloatRequest', 'AgencyTelecomFloatController@TelecomAgencyFloatRequest')->name('TelecomAgencyFloatRequest')->middleware('auth');

Route::get('/AgencyBankFloatRequestForm', 'AgencyBankFloatController@AgencyBankFloatRequestForm')->name('AgencyBankFloatRequestForm')->middleware('auth');

Route::post('/AgencyBankFloatRequest', 'AgencyBankFloatController@AgencyBankFloatRequest')->name('AgencyBankFloatRequest')->middleware('auth');

Route::get('/BankAgencyFloatRequestsHistory', 'AgencyBankFloatController@BankAgencyFloatRequestsHistory')->name('BankAgencyFloatRequestsHistory')->middleware('auth');

Route::get('/AgencyDeleteBankFloatRequestHistory/{id}', 'AgencyBankFloatController@AgencyDeleteBankFloatRequestHistory')->name('AgencyDeleteBankFloatRequestHistory')->middleware('auth');

Route::get('/AgencyApproveAgentFloatRequest', 'AgencyTelecomFloatController@AgencyApproveAgentFloatRequest')->name('AgencyApproveAgentFloatRequest')->middleware('auth');

Route::post('/Agency_Approve_Telecom_Float', 'AgencyTelecomFloatController@Agency_Approve_Telecom_Float')->name('Agency_Approve_Telecom_Float')->middleware('auth');

Route::get('/Agency_Approve_Bank_Float', 'AgencyBankFloatController@AgencyApproveAgentFloatRequest')->name('Agency_Approve_Bank_Float')->middleware('auth');

Route::post('/Agency_Approve_Bank_Float_request', 'AgencyBankFloatController@Agency_Approve_Bank_Float_request')->name('Agency_Approve_Bank_Float_request')->middleware('auth');

Route::get('/AgencyBankCommission', 'AgencyBankCommissionController@AgencyBankCommission')->name('AgencyBankCommission')->middleware('auth');

Route::get('/AgencyTelecomCommission', 'AgencyTelecomCommissionController@AgencyTelecomCommission')->name('AgencyTelecomCommission')->middleware('auth');

Route::get('/AgencyManageBankCash', 'AgencyBankCashController@AgencyManageBankCash')->name('AgencyManageBankCash')->middleware('auth');

Route::get('/AgencyManageTelecomCash', 'AgencyTelecomCashController@AgencyManageTelecomCash')->name('AgencyManageTelecomCash')->middleware('auth');

/******************************Agency Account*****************************************/

Route::get('/CreateAgentUserAccount', 'AccountController@CreateAgentUserAccount')->name('CreateAgentUserAccount')->middleware('auth');

Route::get('/ManageAgentUsers', 'AccountController@ManageAgentUsers')->name('ManageAgentUsers')->middleware('auth');

Route::post('/AssignAgentToOutlet', 'AccountController@AssignAgentToOutlet')->name('AssignAgentToOutlet')->middleware('auth');

Route::post('/AddAgentUserAccount', 'AccountController@AddAgentUserAccount')->name('AddAgentUserAccount')->middleware('auth');

Route::get('/RegisterAgentUserAccount', 'AccountController@RegisterAgentUserAccount')->name('RegisterAgentUserAccount')->middleware('auth');

Route::get('/', 'AccountController@bootstrap')->name('root')->middleware('auth');

Route::get('/BankSetupAccountForm', 'BankController@BankSetupAccountForm')->name('BankSetupAccountForm')->middleware('auth');

Route::post('/SubmitBank', 'BankController@SubmitBank')->name('SubmitBank')->middleware('auth');

Route::get('/ListBanks', 'BankController@ListBanks')->name('ListBanks')->middleware('auth');

Route::get('/TelecomSetup_Form', 'TelecomController@telecomsetupForm')->name('TelecomSetup_Form')->middleware('auth');

Route::post('/TelecomSetup', 'TelecomController@TelecomSetup')->name('TelecomSetup')->middleware('auth');

Route::get('/ListTelecoms', 'TelecomController@ListTelecoms')->name('ListTelecoms')->middleware('auth');

Route::get('/CreateOutletForm', 'OutletsController@CreateOutletForm')->name('CreateOutletForm')->middleware('auth');

Route::post('/CreateOutletLogic', 'OutletsController@CreateOutletLogic')->name('CreateOutletLogic')->middleware('auth');

Route::get('/AllOutlets', 'OutletsController@AllOutlets')->name('AllOutlets')->middleware('auth');

Route::get('/deleteOutlet/{id}', 'OutletsController@deleteOutlet')->name('deleteOutlet')->middleware('auth');

/************Deposits Telecom***********************/

Route::get('/DepositsTelecom', 'TelecomController@DepositsTelecom')->name('DepositsTelecom')->middleware('auth');

/*****************Agent User float request**************************/

Route::get('/TelecomFloatRequest', 'TelecomFloatController@TelecomFloatRequest')->name('TelecomFloatRequest')->middleware('auth');

/*****************Agent User Assign Phone Numbers*******************/

Route::get('/TelecomAddPhoneNumbers', 'TelecomController@TelecomAddPhoneNumbers')->name('TelecomAddPhoneNumbers')->middleware('auth');

Route::post('/SubmitTelecomNumberAccount', 'TelecomController@SubmitTelecomNumberAccount')->name('SubmitTelecomNumberAccount')->middleware('auth');

/******Delete Telecom Outlet phone Numbers (float accounts)******/

Route::get('/DeleteTelecomOutletNumber/{id}', 'TelecomController@DeleteTelecomOutletNumber')->name('DeleteTelecomOutletNumber')->middleware('auth');

Route::get('logout', 'Auth\LoginController@logout');

/**********Telecom Tiers**************/

Route::get('/ManageTelecomTiers/{id}', 'TiersTelecomController@ManageTelecomTiers')->name('ManageTelecomTiers')->middleware('auth');

Route::post('/SubmitTelecomTiers', 'TiersTelecomController@SubmitTelecomTiers')->name('SubmitTelecomTiers')->middleware('auth');

Route::get('/DeleteTelecomTier/{id}', 'TiersTelecomController@DeleteTelecomTier')->name('DeleteTelecomTier')->middleware('auth');

Route::get('/ManageBankTiers/{id}', 'TiersBankController@ManageBankTiers')->name('ManageBankTiers')->middleware('auth');

Route::post('/SubmitBankTiers', 'TiersBankController@SubmitBankTiers')->name('SubmitBankTiers')->middleware('auth');

Route::get('/DeleteBankTier/{id}', 'TiersBankController@DeleteBankTier')->name('DeleteBankTier')->middleware('auth');

/**Agent Float Request***************/

Route::get('/AgentFloatRequestsHistory', 'TelecomFloatController@AgentFloatRequestsHistory')->name('AgentFloatRequestsHistory')->middleware('auth');

Route::post('/TelecomAgentFloatRequest', 'TelecomFloatController@TelecomAgentFloatRequest')->name('TelecomAgentFloatRequest')->middleware('auth');

/************Assign Outlets Bank Accounts****************/

Route::get('/AssignBankAccounts', 'BankController@AssignBankAccounts')->name('AssignBankAccounts')->middleware('auth');

Route::get('/DeleteOutletBankAccount/{id}', 'BankController@DeleteOutletBankAccount')->name('DeleteOutletBankAccount')->middleware('auth');

Route::post('/SubmitBankAccountNumber', 'BankController@SubmitBankAccount')->name('SubmitBankAccountNumber')->middleware('auth');

Route::get('/Approve_Telecom_Float/{id}', 'TelecomFloatController@Approve_Telecom_Float')->name('Approve_Telecom_Float')->middleware('auth');

Route::get('/DeleteFloatRequestHistory/{id}', 'TelecomFloatController@DeleteFloatRequestHistory')->name('DeleteFloatRequestHistory')->middleware('auth');

/***Deposit Telecom Deposit***/
Route::post('/TelecomDepositLogic', 'TelecomDepositsController@TelecomDepositLogic')->name('TelecomDepositLogic')->middleware('auth');

/****Bank Float***********/

Route::get('/BankFloatRequestForm', 'BankFloatController@BankFloatRequestForm')->name('BankFloatRequestForm')->middleware('auth');

Route::post('/BankAgentFloatRequest', 'BankFloatController@BankAgentFloatRequest')->name('BankAgentFloatRequest')->middleware('auth');

Route::get('/BankAgentFloatRequestsHistory', 'BankFloatController@BankAgentFloatRequestsHistory')->name('BankAgentFloatRequestsHistory')->middleware('auth');

Route::get('/Approve_Bank_Float/{id}', 'BankFloatController@Approve_Bank_Float')->name('Approve_Bank_Float')->middleware('auth');

Route::get('/DeleteBankFloatRequestHistory/{id}', 'BankFloatController@DeleteBankFloatRequestHistory')->name('DeleteBankFloatRequestHistory')->middleware('auth');

Route::get('/DepositsBank', 'BankDepositController@DepositsBank')->name('DepositsBank')->middleware('auth');

Route::post('/BankDepositLogic', 'BankDepositController@BankDepositLogic')->name('BankDepositLogic')->middleware('auth');

/****Telecom Withdraw*****/

Route::get('/TelecomWithdrawForm', 'TelecomWithdrawController@TelecomWithdrawForm')->name('TelecomWithdrawForm')->middleware('auth');

Route::post('/TelecomWithdrawLogic', 'TelecomWithdrawController@TelecomWithdrawLogic')->name('TelecomWithdrawLogic')->middleware('auth');

/******Bank Withdraw******/

Route::get('/BankWithdrawForm', 'BankWithdrawController@BankWithdrawForm')->name('BankWithdrawForm')->middleware('auth');

Route::post('/BankWithdrawLogic', 'BankWithdrawController@BankWithdrawLogic')->name('BankWithdrawLogic')->middleware('auth');

/****Bank Cash at hand ****/

Route::get('/ManageBankCash', 'BankCashController@ManageBankCash')->name('ManageBankCash')->middleware('auth');

/***************Telecom Cash At Hand/**********/

Route::get('/ManageTelecomCash', 'TelecomCashController@ManageTelecomCash')->name('ManageTelecomCash')->middleware('auth');

/****Telecom commission****/

Route::get('/TelecomCommission', 'TelecomCommissionController@TelecomCommission')->name('TelecomCommission')->middleware('auth');

/****Bank Commission*********/
Route::get('/BankCommission', 'BankCommissionController@BankCommission')->name('BankCommission')->middleware('auth');

/****FilterDatesFromTransactions***/

Route::post('/FilterDatesFromTransactions', 'AccountController@FilterDatesFromTransactions')->name('FilterDatesFromTransactions')->middleware('auth');

Route::post('/BankFilterDatesFromTransactions', 'AccountController@BankFilterDatesFromTransactions')->name('BankFilterDatesFromTransactions')->middleware('auth');

/*************Agent Profile********************/
Route::get('/ViewProfile', 'ProfileController@ViewProfile')->name('ViewProfile')->middleware('auth');
